from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

admin.site.site_header = "PAR"
admin.site.site_title = "PAR Admin Portal"
admin.site.index_title = "Welcome to PAR Admin Portal"

from django.contrib.auth.models import Group
from django_summernote.models import Attachment

from django.conf.urls.i18n import i18n_patterns

admin.site.unregister(Group)
admin.site.unregister(Attachment)

urlpatterns = i18n_patterns (
    path('', include('par.frontend.urls', namespace='frontend')),
    path('par-admin/', admin.site.urls),
    path('summernote/', include('django_summernote.urls')), # using include('django_summernote.urls')
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# di server saat settings debug true
if settings.DEBUG:
    urlpatterns + static(settings.STATIC_URL,
                         document_root=settings.STATIC_ROOT)
