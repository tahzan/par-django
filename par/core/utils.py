import os
from django.conf import settings
from slugify import slugify
from django.utils import timezone
from time import time

class FilenameGenerator(object):
    def __init__(self, prefix):
        self.prefix = prefix

    def __call__(self, instance, filename):
        today = timezone.localtime(timezone.now()).date()
        filepath = os.path.basename(filename)
        filename, extension = os.path.splitext(filepath)
        filename = slugify(filename)

        path = "/".join([
            'static_files',
            self.prefix,
            str(today.year),
            str(today.month),
            # str(today.day),
            filename + extension
        ])
        return path

try:
    from django.utils.deconstruct import deconstructible
    FilenameGenerator = deconstructible(FilenameGenerator)
except ImportError:
    pass


def custom_slugify(string):
    string = "%s-%s" % (string, str(time())[11:])
    return slugify(string)

def remove_prefix(text, prefix):
    return text[text.startswith(prefix) and len(prefix):]

def getJSONData(data, lang):
    _response = {}
    lang = lang.lower()
    for dt in data:
        if dt.name.startswith(lang) or dt.name.startswith('all_'):
            key = remove_prefix(dt.name, f'{lang}_')
            _response[key] = {
                "val":  dt.text_value or dt.img_value 
            }
    return _response