import json
from django.http import JsonResponse
from django.template.response import TemplateResponse
from django.views.decorators.csrf import csrf_exempt
from par.apps.settings.models import *
from par.apps.content.models import *
from .forms import ContactForm, RedeemForm
from django.db.models import Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
# from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
# from django.template import loader

# 1
def index(request):
    common_settings = CommonSettings().getSettings(request.LANGUAGE_CODE)
    footer = FooterSettings().getSettings(request.LANGUAGE_CODE)
    settings = HomeSettings().getSettings(request.LANGUAGE_CODE)
    clients = Client.objects.filter(is_active=True).filter(is_active=True).all()
    news = News.objects.filter(is_active=True).filter(is_active=True).all()
    business = OurBusiness.objects.filter(is_active=True).filter(is_active=True).all()
    banners = Banner.objects.filter(is_active=True).filter(is_active=True).all()
    context = {
        "footer": footer,
        "clients": clients,
        "news": news[0:3],
        "business": business,
        "banners": banners,
        "settings": settings,
        "common_settings": common_settings,
        "title": "PAR (Prima Armada Raya)"
    }
    return TemplateResponse(request, "frontend/index.html", context)
# 2
def aboutus(request):
    settings = AboutUsSettings().getSettings(request.LANGUAGE_CODE)
    awards = Award.objects.filter(is_active=True).all()
    certificates = Certicate.objects.filter(is_active=True).all()
    yearly_reports = YearlyReport.objects.filter(is_active=True).all()
    tata_nilai = TataNilai.objects.filter(is_active=True).all()
    history = History.objects.filter(is_active=True).all()
    footer = FooterSettings().getSettings(request.LANGUAGE_CODE)
    common_settings = CommonSettings().getSettings(request.LANGUAGE_CODE)
    context = {
        "settings": settings,
        "awards" : awards,
        "certificates" : certificates,
        "yearly_reports" : yearly_reports,
        "tata_nilai": tata_nilai,
        "history": history,
        "footer": footer,
        "common_settings": common_settings,
        "title": "About US (PAR Prima Armada Raya)"
    }
    return TemplateResponse(request, "frontend/aboutus.html", context)

# 3
def management(request):
    settings = AboutUsSettings().getSettings(request.LANGUAGE_CODE)
    common_settings = CommonSettings().getSettings(request.LANGUAGE_CODE)
    footer = FooterSettings().getSettings(request.LANGUAGE_CODE)
    direksi = DirectorProfile.objects.filter(type='direksi').all().order_by('-id')
    komisaris = DirectorProfile.objects.filter(type='komisaris').all().order_by('-id')

    context = {
        "common_settings": common_settings,
        "settings": settings,
        "footer": footer,
        "direksi": direksi,
        "komisaris": komisaris,
        "title": "Our Management (PAR Prima Armada Raya)"
    }
    return TemplateResponse(request, "frontend/direksi.html", context)

# 4
def our_business(request):
    common_settings = CommonSettings().getSettings(request.LANGUAGE_CODE)
    settings = OurBusinessSettings().getSettings(request.LANGUAGE_CODE)
    footer = FooterSettings().getSettings(request.LANGUAGE_CODE)
    business = OurBusiness.objects.filter(is_active=True).all()
    context = {
        "common_settings": common_settings,
        "settings": settings,
        "footer": footer,
        "business": business,
        "title": "Our Business (PAR Prima Armada Raya)"
    }
    return TemplateResponse(request, "frontend/our_business.html", context)

# 5
def investor(request):
    common_settings = CommonSettings().getSettings(request.LANGUAGE_CODE)
    settings = InvestorPageSettings().getSettings(request.LANGUAGE_CODE)
    footer = FooterSettings().getSettings(request.LANGUAGE_CODE)
    yearly_reports = YearlyReport.objects.filter(is_active=True).all()
    context = {
        "common_settings": common_settings,
        "settings": settings,
        "footer": footer,
        "yearly_reports": yearly_reports,
        "title": "Our Investor (PAR Prima Armada Raya)"
    }
    return TemplateResponse(request, "frontend/investor.html", context)

# 6
def news(request):
    common_settings = CommonSettings().getSettings(request.LANGUAGE_CODE)
    settings = NewsPageSettings().getSettings(request.LANGUAGE_CODE)
    footer = FooterSettings().getSettings(request.LANGUAGE_CODE)
    news = News.objects.filter(type='news').filter(is_active=True)
    articles = News.objects.filter(type='artikel').filter(is_active=True)

    page = request.GET.get('page')
    paginator = Paginator(articles, 6)
    try:
        articles = paginator.page(page)
    except PageNotAnInteger:
        articles = paginator.page(1)
    except EmptyPage:
        articles = []

    context = {
        "common_settings": common_settings,
        "settings": settings,
        "footer": footer,
        "second_news": news[1:3],
        "news": news[3:7],
        "articles": articles,
        "first_news": news.first(),
        "title": "Article & News (PAR Prima Armada Raya)"
    }
    return TemplateResponse(request, "frontend/news.html", context)

# 7
def contactus(request):
    common_settings = CommonSettings().getSettings(request.LANGUAGE_CODE)
    footer = FooterSettings().getSettings(request.LANGUAGE_CODE)
    settings = ContactUseSettings().getSettings(request.LANGUAGE_CODE)
    portals = Portal.objects.filter(is_active=True).all()
    form = ContactForm()
    message = ""
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            message= "Thank you. Your message send successfully" if request.LANGUAGE_CODE == 'en' else "Terima kasih. Pesan anda berhasil dikirim"
            form.save()
    context = {
        "settings": settings,
        "common_settings": common_settings,
        "footer": footer,
        "portals": portals,
        "form": form,
        "message": message,
        "title": "Contact US (PAR Prima Armada Raya)"
    }
    return TemplateResponse(request, "frontend/contact-us.html", context)

# 8
def portal(request):
    common_settings = CommonSettings().getSettings(request.LANGUAGE_CODE)
    footer = FooterSettings().getSettings(request.LANGUAGE_CODE)
    portals = Portal.objects.filter(is_active=True).all()
    context = {
        "common_settings": common_settings,
        "footer": footer,
        "portals": portals,
        "hide_footer": True,
        "title": "Portal (PAR Prima Armada Raya)"
    }
    if portals.count() <= 3:
        return TemplateResponse(request, "frontend/portal.html", context)
    else:
        return TemplateResponse(request, "frontend/portal_splide.html", context)

# 9
def news_detail(request, slug):
    common_settings = CommonSettings().getSettings(request.LANGUAGE_CODE)
    settings = NewsPageSettings().getSettings(request.LANGUAGE_CODE)
    footer = FooterSettings().getSettings(request.LANGUAGE_CODE)
    news = News.objects.filter(slug=slug).first()
    context = {
        "common_settings": common_settings,
        "footer": footer,
        "news": news,
        "title": news.en_title
    }
    return TemplateResponse(request, "frontend/news_detail.html", context)

# 10
def article_detail(request, slug):
    common_settings = CommonSettings().getSettings(request.LANGUAGE_CODE)
    settings = NewsPageSettings().getSettings(request.LANGUAGE_CODE)
    footer = FooterSettings().getSettings(request.LANGUAGE_CODE)
    news = News.objects.filter(slug=slug).first()
    context = {
        "common_settings": common_settings,
        "footer": footer,
        "news": news,
        "title": news.en_title
    }
    return TemplateResponse(request, "frontend/article_detail.html", context)

# 11
def tender(request):
    common_settings = CommonSettings().getSettings(request.LANGUAGE_CODE)
    settings = TenderPageSettings().getSettings(request.LANGUAGE_CODE)
    footer = FooterSettings().getSettings(request.LANGUAGE_CODE)
    tenders = Tender.objects.filter(is_active=True).all()
    context = {
        "common_settings": common_settings,
        "footer": footer,
        "settings": settings,
        "tenders": tenders,
        "title": "Tender (PAR Prima Armada Raya)"
    }
    return TemplateResponse(request, "frontend/tender.html", context)

# 12
def career(request):
    common_settings = CommonSettings().getSettings(request.LANGUAGE_CODE)
    settings = CarierPageSettings().getSettings(request.LANGUAGE_CODE)
    footer = FooterSettings().getSettings(request.LANGUAGE_CODE)
    career_categories = CarierCategory.objects.all()
    number_of_career = Carier.objects.filter(is_active=True).count()
    context = {
        "common_settings": common_settings,
        "footer": footer,
        "settings": settings,
        "career_categories": career_categories,
        "number_of_career" : number_of_career,
        "title": "Career (PAR Prima Armada Raya)"
    }
    return TemplateResponse(request, "frontend/career.html", context)

# 13
def career_detail(request, slug):
    career = Carier.objects.filter(slug=slug).first()
    careers = Carier.objects.filter(category=career.category)
    common_settings = CommonSettings().getSettings(request.LANGUAGE_CODE)
    settings = CarierDetailPageSettings().getSettings(request.LANGUAGE_CODE)
    footer = FooterSettings().getSettings(request.LANGUAGE_CODE)
    context = {
        "common_settings": common_settings,
        "footer": footer,
        "settings": settings,
        "careers": careers,
        "career": career,
        "title": "Career Detail (PAR Prima Armada Raya)"
    }
    return TemplateResponse(request, "frontend/career_detail.html", context)


# 14
def gallery(request):
    common_settings = CommonSettings().getSettings(request.LANGUAGE_CODE)
    settings = GalleryPageSettings().getSettings(request.LANGUAGE_CODE)
    footer = FooterSettings().getSettings(request.LANGUAGE_CODE)
    albums = Album.objects.filter(is_active=True).all().exclude(id_title__icontains="tjsl")

    page = request.GET.get('page')
    paginator = Paginator(albums, 6)
    try:
        albums = paginator.page(page)
    except PageNotAnInteger:
        albums = paginator.page(1)
    except EmptyPage:
        albums = []

    context = {
        "common_settings": common_settings,
        "footer": footer,
        "settings": settings,
        "albums": albums,
        "title": "Gallery (PAR Prima Armada Raya)"
    }
    return TemplateResponse(request, "frontend/gallery.html", context)

# 15
def gallery_detail(request, slug):
    album = Album.objects.filter(slug=slug).first()
    photos = Gallery.objects.filter(album=album)
    common_settings = CommonSettings().getSettings(request.LANGUAGE_CODE)
    settings = CarierDetailPageSettings().getSettings(request.LANGUAGE_CODE)
    footer = FooterSettings().getSettings(request.LANGUAGE_CODE)
    context = {
        "common_settings": common_settings,
        "footer": footer,
        "settings": settings,
        "album": album,
        "photos": photos,
        "title": album.id_title
    }
    return TemplateResponse(request, "frontend/gallery-detail.html", context)


# 16
def tjsl(request):
    common_settings = CommonSettings().getSettings(request.LANGUAGE_CODE)
    settings = TjslPageSettings().getSettings(request.LANGUAGE_CODE)
    footer = FooterSettings().getSettings(request.LANGUAGE_CODE)
    album = Album.objects.filter(id_title__icontains="tjsl").first()
    photos = Gallery.objects.filter(album=album)
    context = {
        "common_settings": common_settings,
        "footer": footer,
        "settings": settings,
        "photos": photos,
        "title": "TJSL (PAR Prima Armada Raya)"
    }
    return TemplateResponse(request, "frontend/tjsl.html", context)


# 17
def tender_detail(request, slug):
    tender = Tender.objects.filter(slug=slug).first()
    common_settings = CommonSettings().getSettings(request.LANGUAGE_CODE)
    settings = CarierDetailPageSettings().getSettings(request.LANGUAGE_CODE)
    footer = FooterSettings().getSettings(request.LANGUAGE_CODE)
    context = {
        "common_settings": common_settings,
        "footer": footer,
        "settings": settings,
        "tender": tender,
        "title": "Tender Detail (PAR Prima Armada Raya)"
    }
    return TemplateResponse(request, "frontend/tender_detail.html", context)


# 1
def search(request):
    query = request.GET.get("query")
    
    newss = News.objects.filter(
        Q(id_title__icontains=query)|
        Q(en_title__icontains=query)|
        Q(id_long_desc__icontains=query)|
        Q(en_long_desc__icontains=query)).filter(is_active=True).all()

    tenders = Tender.objects.filter(
        Q(title__icontains=query)
    ).filter(is_active=True).all()

    careers = Carier.objects.filter(
        Q(id_title__icontains=query)|
        Q(en_title__icontains=query)
    ).filter(is_active=True).all()


    common_settings = CommonSettings().getSettings(request.LANGUAGE_CODE)
    total = newss.count() + tenders.count() + careers.count()
    settings = SearchPageSettings().getSettings(request.LANGUAGE_CODE)
    footer = FooterSettings().getSettings(request.LANGUAGE_CODE)

    return TemplateResponse(request, 'frontend/search.html', 
        {
            "newss": newss,
            "tenders": tenders,
            "careers": careers,
            "common_settings": common_settings,
            "query": query,
            "total": total,
            "settings": settings,
            "footer": footer,
             "title": "Search (PAR Prima Armada Raya)"
        }
    )


def is_ajax(request):
    return request.META.get('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest'

@csrf_exempt
def subscribe(request):
    if is_ajax(request=request) and request.method == "POST":
        form = RedeemForm(request.POST)
        if form.is_valid():
            redeem = form.save()
        return JsonResponse({
            "message": "Transaksi berhasil",
        }, status=200)
    else:
        return JsonResponse(form.errors, status=400)
    return JsonResponse({"error": "Need Request Post and AJAX"}, status=400)
