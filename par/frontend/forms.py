from django import forms
from par.apps.content.models import Contact, EmailSubscribe


class ContactForm(forms.Form):
    first_name = forms.CharField()
    last_name = forms.CharField()
    email = forms.EmailField()
    phone_number = forms.CharField()
    message = forms.CharField()
    def save(self):
        data = self.cleaned_data
        contact, _ = Contact.objects.update_or_create(
            phone_number = data['phone_number'],
            email = data['email'],
            defaults = {
                "email" : data['email'],
                "phone_number" : data['phone_number'], 
                "first_name" : data['first_name'], 
                "last_name" : data['last_name'], 
                "message" : data['message'], 
            }
        )
        return contact


class RedeemForm(forms.Form):
    email = forms.CharField()
    def save(self):
        data = self.cleaned_data
        subs, _ = EmailSubscribe.objects.update_or_create(
            email = data['email'],
        )
        return subs