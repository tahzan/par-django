from django.urls import path
from .views import *

app_name = "frontend"
urlpatterns = [
    path('', index, name='index'),
    path('about-us/', aboutus, name='aboutus'),
    path('management/', management, name='management'),
    path('our-business/', our_business, name='our_business'),
    
    path('investor/', investor, name='investor'),
    path('contact-us/', contactus, name='contactus'),
    path('portal/', portal, name='portal'),

    path('news/', news, name='news'),
    path("news/<str:slug>", news_detail, name="news_detail"),
    path("article/<str:slug>", article_detail, name="article_detail"),

    path('tender/', tender, name='tender'),
    path("tender/<str:slug>", tender_detail, name="tender_detail"),
    path('career/', career, name='career'),
    path("career/<str:slug>", career_detail, name="career_detail"),

    path('gallery/', gallery, name='gallery'),
    path("gallery/<str:slug>", gallery_detail, name="gallery_detail"),
    path("tjsl", tjsl, name="tjsl"),
    path("search", search, name="search"),
    path('api-subscribe/', subscribe, name='subscribe'),
]
