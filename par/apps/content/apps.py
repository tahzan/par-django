from django.apps import AppConfig


class ContentConfig(AppConfig):
    name = 'par.apps.content'
