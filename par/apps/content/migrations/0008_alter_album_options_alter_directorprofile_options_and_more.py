# Generated by Django 4.1 on 2022-10-24 13:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0007_tatanilai_is_active'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='album',
            options={'verbose_name': 'Gallery & TJSL', 'verbose_name_plural': 'Gallery & TJSL'},
        ),
        migrations.AlterModelOptions(
            name='directorprofile',
            options={'verbose_name': 'AboutUs - Profile', 'verbose_name_plural': 'AboutUs - Profile'},
        ),
        migrations.AddField(
            model_name='history',
            name='en_title',
            field=models.CharField(max_length=254, null=True),
        ),
        migrations.AddField(
            model_name='history',
            name='id_title',
            field=models.CharField(max_length=254, null=True),
        ),
    ]
