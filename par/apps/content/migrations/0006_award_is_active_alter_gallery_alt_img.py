# Generated by Django 4.1 on 2022-10-09 21:51

from django.db import migrations, models
import par.core.utils


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0005_remove_gallery_img_gallery_alt_img_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='award',
            name='is_active',
            field=models.BooleanField(default=True, verbose_name='Published'),
        ),
        migrations.AlterField(
            model_name='gallery',
            name='alt_img',
            field=models.FileField(null=True, upload_to=par.core.utils.FilenameGenerator(prefix='alt_img'), verbose_name='thumbnail'),
        ),
    ]
