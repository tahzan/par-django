from par.core.utils import FilenameGenerator, custom_slugify
from django.db import models
from taggit.managers import TaggableManager


class Portal(models.Model):
    class Meta:
        verbose_name = 'Portal'
        verbose_name_plural = 'Portal'

    img =  models.ImageField(upload_to=FilenameGenerator(
        prefix='banner'))

    id_big_text = models.TextField(null=True)
    en_big_text = models.TextField(null=True)
    
    id_small_text = models.TextField(null=True)
    en_small_text = models.TextField(null=True)

    link = models.CharField(max_length=254, null=True)
    is_active = models.BooleanField(default=True, null=True)

    def __str__(self):
        return f"{self.id_big_text}"


class Menu(models.Model):
    class Meta:
        verbose_name = 'Top Menu'
        verbose_name_plural = 'Top Menu'
    id_title = models.CharField(max_length=254, null=True)
    en_title = models.CharField(max_length=254, null=True)
    order = models.PositiveSmallIntegerField(default=1)
   
    def __str__(self):
        return f"{self.id}"


class Banner(models.Model):
    class Meta:
        verbose_name = 'Home Banner'
        verbose_name_plural = 'Home Banner'

    img =  models.ImageField(upload_to=FilenameGenerator(
        prefix='banner'))

    id_big_text = models.TextField(null=True)
    en_big_text = models.TextField(null=True)
    
    id_small_text = models.TextField(null=True)
    en_small_text = models.TextField(null=True)
    
    is_active = models.BooleanField(default=True, null=True)

    def __str__(self):
        return f"{self.id_big_text}"


class OurBusiness(models.Model):
    class Meta:
        verbose_name = 'Our Business'
        verbose_name_plural = 'Our Business'
    img =  models.ImageField("image", upload_to=FilenameGenerator(
        prefix='banner'))
    logo =  models.ImageField("logo", upload_to=FilenameGenerator(
            prefix='logo'))
    
    id_title = models.CharField(max_length=254)
    id_short_desc = models.CharField(max_length=254)
    id_long_desc = models.TextField()

    en_title = models.CharField(max_length=254)
    en_short_desc = models.CharField(max_length=254)
    en_long_desc = models.TextField()

    order = models.PositiveIntegerField(default=0)

    is_active = models.BooleanField("Published", default=True)
    slug = models.CharField(unique=True, max_length=254, blank=True)
    
    def __str__(self):
        return f"{self.id_title}"

    # def preview(self):
    #     return mark_safe('<a href="/news/%s" target="_blank"/> preview </a>' % self.slug)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = custom_slugify(self.id_title)
        super(OurBusiness, self).save()


class NewsTag(models.Model):
    class Meta:
        verbose_name = 'News & Article Category'
        verbose_name_plural = 'News & Article Category'
    id_title = models.CharField(max_length=254, null=True)
    en_title = models.CharField(max_length=254, null=True)
    color = models.CharField(max_length=10, null=True)
    bg_color = models.CharField(max_length=10, null=True)
    
    def __str__(self):
        return self.id_title


# registered
class News(models.Model):
    class Meta:
        verbose_name = 'News & Article'
        verbose_name_plural = 'News & Article'
    img =  models.ImageField("image", upload_to=FilenameGenerator(
        prefix='banner'))
    
    id_title = models.CharField(max_length=254)
    id_short_desc = models.CharField(max_length=254)
    id_long_desc = models.TextField()

    en_title = models.CharField(max_length=254)
    en_short_desc = models.CharField(max_length=254)
    en_long_desc = models.TextField()

    date = models.DateField(blank=True)
    is_active = models.BooleanField("Published", default=True)
    categories = models.ManyToManyField(NewsTag)
    slug = models.CharField(unique=True, max_length=254, blank=True)
    TYPE = (
        ('artikel', 'artikel'),
        ('news', 'news'),
    )
    type = models.CharField(max_length=254, choices=TYPE, null=True)
    
    def __str__(self):
        return f"{self.id_title}"

    # def preview(self):
    #     return mark_safe('<a href="/news/%s" target="_blank"/> preview </a>' % self.slug)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = custom_slugify(self.id_title)
        super(News, self).save()


# registered
class YearlyReport(models.Model):
    class Meta:
        verbose_name = 'About Us - Yearly Report'
        verbose_name_plural = 'About Us - Yearly Report'
    img =  models.ImageField("image", upload_to=FilenameGenerator(
        prefix='banner'))
    
    id_title = models.CharField(max_length=254)
    en_title = models.CharField(max_length=254)

    is_active = models.BooleanField("Published", default=True)
    slug = models.CharField(unique=True, max_length=254, blank=True)
    file =  models.FileField("file", upload_to=FilenameGenerator(
        prefix='file'), null=True)
    
    def __str__(self):
        return f"{self.id} {self.id_title}"

    # def preview(self):
    #     return mark_safe('<a href="/news/%s" target="_blank"/> preview </a>' % self.slug)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = custom_slugify(self.id_title)
        super(YearlyReport, self).save()


#registered
class History(models.Model):
    class Meta:
        verbose_name = 'About Us - Milestone'
        verbose_name_plural = 'About Us - Milestone'

    id_title = models.CharField(max_length=254, null=True)
    en_title = models.CharField(max_length=254, null=True)
    
    id_long_desc = models.TextField()
    en_long_desc = models.TextField()
    year = models.IntegerField()
    is_active = models.BooleanField("Published", default=True)
    
    def __str__(self):
        return f"{self.id} {self.id_title}"

    # def preview(self):
    #     return mark_safe('<a href="/news/%s" target="_blank"/> preview </a>' % self.slug)

    # def save(self, *args, **kwargs):
    #     if not self.slug:
    #         self.slug = custom_slugify(self.title)
    #     super(History, self).save()
    

# registered
class DirectorProfile(models.Model):
    class Meta:
        verbose_name = 'AboutUs - Profile'
        verbose_name_plural = 'AboutUs - Profile'
    name = models.CharField(max_length=254)
    id_position = models.CharField(max_length=254, null=True)
    en_position = models.CharField(max_length=254, null=True)
    is_active = models.BooleanField("Published", default=True)
    id_long_desc = models.TextField()
    en_long_desc = models.TextField()
    img =  models.ImageField("image", upload_to=FilenameGenerator(
        prefix='profile_direktur'), null=True)
    TYPE = (
        ('direksi', 'direksi'),
        ('komisaris', 'komisaris'),
    )
    type = models.CharField(max_length=254, choices=TYPE)
    
    def __str__(self):
        return self.name

    # def preview(self):
    #     return mark_safe('<a href="/news/%s" target="_blank"/> preview </a>' % self.slug)


# registered
class Award(models.Model):
    id_title = models.CharField(null=True ,max_length=254)
    en_title = models.CharField(null=True, max_length=254)
    class Meta:
        verbose_name = 'About Us - Award'
        verbose_name_plural = 'About Us - Award'
    img =  models.ImageField("image", upload_to=FilenameGenerator(
        prefix='banner'))
    id_description = models.TextField()
    en_description = models.TextField()
    is_active = models.BooleanField("Published", default=True)
    month_year = models.CharField(max_length=254, null=True)
    
    def __str__(self):
        return f"{self.id} {self.id_title}"

    # def preview(self):
    #     return mark_safe('<a href="/news/%s" target="_blank"/> preview </a>' % self.slug)

    # def save(self, *args, **kwargs):
    #     if not self.slug:
    #         self.slug = custom_slugify(self.title)
    #     super(OurBusiness, self).save()


# registered
class Certicate(models.Model):
    id_title = models.CharField(null=True ,max_length=254)
    en_title = models.CharField(null=True, max_length=254)
    class Meta:
        verbose_name = 'About Us - Certicate'
        verbose_name_plural = 'About Us - Certicate'
    img =  models.ImageField("image", upload_to=FilenameGenerator(
        prefix='banner'))
    id_description = models.TextField()
    en_description = models.TextField()
    year = models.CharField(max_length=254)
    is_active = models.BooleanField("Published", default=True)
    slug = models.CharField(unique=True, max_length=254, blank=True)
    
    def __str__(self):
        return f"{self.id} {self.id_title}"

    # def preview(self):
    #     return mark_safe('<a href="/news/%s" target="_blank"/> preview </a>' % self.slug)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = custom_slugify(self.id_description)
        super(Certicate, self).save()


# registered
class Album(models.Model):
    class Meta:
        verbose_name = 'Gallery & TJSL'
        verbose_name_plural = 'Gallery & TJSL'
    img =  models.ImageField("image", upload_to=FilenameGenerator(
        prefix='banner'))
    id_title = models.TextField(null=True)
    en_title = models.TextField(null=True)
    date = models.DateField()
    is_active = models.BooleanField("Published", default=True)
    slug = models.CharField(unique=True, max_length=254, blank=True)
    total_photo = models.IntegerField(default=0, null=True)
    total_video = models.IntegerField(default=0, null=True)

    def __str__(self):
        return self.id_title

    # def preview(self):
    #     return mark_safe('<a href="/news/%s" target="_blank"/> preview </a>' % self.slug)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = custom_slugify(self.id_title)
        super(Album, self).save()


class Gallery(models.Model):
    class Meta:
        verbose_name = 'Gallery'
        verbose_name_plural = 'Gallery'
    alt_img =  models.FileField("thumbnail", upload_to=FilenameGenerator(
        prefix='alt_img'), null=True)
    img_or_video =  models.FileField("img_or_video", upload_to=FilenameGenerator(
        prefix='img_or_video'), null=True)
    is_active = models.BooleanField("Published", default=True)
    album = models.ForeignKey(Album, on_delete=models.CASCADE)
    id_title = models.CharField(max_length=254, blank=True, null=True)
    en_title = models.CharField(max_length=254, blank=True, null=True)
    TYPE = (
        ('foto', 'foto'),
        ('video', 'video'),
    )
    type = models.CharField(max_length=254, choices=TYPE, null=True)
    
    def save(self, *args, **kwargs):
        super(Gallery, self).save()
        fotos = Gallery.objects.filter(album=self.album)
        self.album.total_photo = fotos.filter(type='foto').count()
        self.album.total_video = fotos.filter(type='video').count()
        self.album.save()
   


class Client(models.Model):
    class Meta:
        verbose_name = 'Our Client'
        verbose_name_plural = 'Our Client'
    img =  models.ImageField("image", upload_to=FilenameGenerator(
        prefix='banner'))
    title = models.CharField(max_length=254)
    is_active = models.BooleanField("Published", default=True)
    
    def __str__(self):
        return self.title


class CarierCategory(models.Model):
    class Meta:
        verbose_name = 'Career Category'
        verbose_name_plural = 'Career Category'
    id_title = models.CharField(max_length=254, null=True)
    en_title = models.CharField(max_length=254, null=True)
    
    def __str__(self):
        return self.id_title


#KARIR
class Carier(models.Model):
    class Meta:
        verbose_name = 'Career'
        verbose_name_plural = 'Career'
    is_active = models.BooleanField("Published", default=True)
    id_title = models.CharField(max_length=254)
    en_title = models.CharField(max_length=254)
    id_description = models.TextField(null=True)
    en_description = models.TextField(null=True)
    city = models.CharField(max_length=254, blank=True, null=True)
    country = models.CharField(max_length=254, blank=True, null=True, default="Indonesia")
    category = models.ForeignKey(CarierCategory, on_delete=models.CASCADE, null=True)
    slug = models.CharField(max_length=254, blank=True)
    
    TYPE = (
        ('half_time', 'Half Time'),
        ('full_time', 'Full Time'),
    )
    type = models.CharField(max_length=254, choices=TYPE, null=True)
    
    def __str__(self):
        return self.id_title

    # def preview(self):
    #     return mark_safe('<a href="/news/%s" target="_blank"/> preview </a>' % self.slug)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = custom_slugify(self.id_title)
        super(Carier, self).save()
    


#TENDER
class Tender(models.Model):
    class Meta:
        verbose_name = 'Tender'
        verbose_name_plural = 'Tender'
    is_active = models.BooleanField("Published", default=True)
    id_title = models.CharField(max_length=254, null=True)
    en_title = models.CharField(max_length=254, null=True)
    start_date = models.DateField()
    end_date = models.DateField()
    city = models.CharField(max_length=254)
    country = models.CharField(max_length=254, default="Indonesia")
    slug = models.CharField(max_length=254, blank=True)
    id_description = models.TextField(null=True)
    en_description = models.TextField(null=True)
    
    def __str__(self):
        return self.id_title

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = custom_slugify(self.id_title)
        super(Tender, self).save()


#CONTACT
class Contact(models.Model):
    class Meta:
        verbose_name = 'Contact'
        verbose_name_plural = 'Contact'
    first_name = models.CharField(max_length=254)
    last_name = models.CharField(max_length=254)
    email = models.CharField(max_length=254, null=True)
    phone_number = models.CharField(max_length=254)
    message = models.TextField("Message")
    
    def __str__(self):
        return self.first_name


class EmailSubscribe(models.Model):
    class Meta:
        verbose_name = 'Email Subscribe'
        verbose_name_plural = 'Email Subscribe'
    email = models.CharField(max_length=254, null=True)

    def __str__(self):
        return self.email


class TataNilai(models.Model):
    class Meta:
        verbose_name = 'About Us - Tata Nilai [PRIMA]'
        verbose_name_plural = 'About Us - Tata Nilai [PRIMA]'
    img =  models.ImageField("image", upload_to=FilenameGenerator(
        prefix='banner'))
    id_title = models.TextField()
    id_description = models.TextField()
    en_description = models.TextField(null=True)
    order = models.PositiveIntegerField()
    is_active = models.BooleanField("Published", default=True)
    