from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin

from .models import *


class SettingsModelAdmin(admin.ModelAdmin):  # instead of ModelAdmin
    ordering = ('name',)
    list_display = ('name', 'text_value', 'img_value')


class SummernoteAdmin(SummernoteModelAdmin):    
    ordering = ('name',)
    summernote_fields = ['text_value']
    list_display = ('name', 'text_value', 'img_value')

admin.site.register(HomeSettings, SummernoteAdmin)
admin.site.register(AboutUsSettings, SummernoteAdmin)

admin.site.register(OurBusinessSettings, SettingsModelAdmin)
admin.site.register(NewsPageSettings, SettingsModelAdmin)
admin.site.register(ContactUseSettings, SettingsModelAdmin)
admin.site.register(TenderPageSettings, SettingsModelAdmin)
admin.site.register(CarierPageSettings, SettingsModelAdmin)
admin.site.register(CarierDetailPageSettings, SettingsModelAdmin)
admin.site.register(InvestorPageSettings, SettingsModelAdmin)
admin.site.register(GalleryPageSettings, SettingsModelAdmin)
admin.site.register(FooterSettings, SettingsModelAdmin)
admin.site.register(CommonSettings, SettingsModelAdmin)
admin.site.register(TjslPageSettings, SettingsModelAdmin)
admin.site.register(SearchPageSettings, SettingsModelAdmin)

# admin.site.register(Settings, SettingsModelAdmin)