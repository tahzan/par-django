from par.core.utils import FilenameGenerator, getJSONData
from django.db import models


class AbstractCompany(models.Model):
    text_value = models.TextField(blank=True, null=True)
    img_value = models.ImageField(upload_to=FilenameGenerator(
        prefix='settings'), blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        abstract = True


class HomeSettings(AbstractCompany):
    class Meta:
        verbose_name = 'Home Settings'
        verbose_name_plural = 'Home Settings'
    TYPE = (
        ('all_img_about_1', 'all_img_about_1'),
        ('all_total_kendaraan', 'all_total_kendaraan'),
        ('all_total_pengemudi', 'all_total_pengemudi'),

        ('id_about_title', 'id_about_title'),
        ('id_about_text', 'id_about_text'),
        ('id_about_description', 'id_about_description'),

        ('id_btn_selengkapnya', 'id_btn_selengkapnya'),

        ('id_client_title', 'id_client_title'),
        ('id_client_sub_title', 'id_client_sub_title'),

        ('id_map_title', 'id_map_title'),
        ('id_map_description', 'id_map_description'),
        ('id_map_kendaraan', 'id_map_kendaraan'),
        ('id_map_pengemudi', 'id_map_pengemudi'),
        
        ('id_news_title', 'id_news_title'),
        ('id_news_sub_title', 'id_news_sub_title'),

        ('id_our_business_title', 'id_our_business_title'),
        ('id_our_business_sub_title', 'id_our_business_sub_title'),

        ('en_about_title', 'en_about_title'),
        ('en_about_text', 'en_about_text'),
        ('en_about_description', 'en_about_description'),

        ('en_btn_selengkapnya', 'en_btn_selengkapnya'),

        ('en_client_title', 'en_client_title'),
        ('en_client_sub_title', 'en_client_sub_title'),

        ('en_map_title', 'en_map_title'),
        ('en_map_description', 'en_map_description'),
        ('en_map_kendaraan', 'en_map_kendaraan'),
        ('en_map_pengemudi', 'en_map_pengemudi'),

        ('en_news_title', 'en_news_title'),
        ('en_news_sub_title', 'en_news_sub_title'),

        ('en_our_business_title', 'en_our_business_title'),
        ('en_our_business_sub_title', 'en_our_business_sub_title'),

    )
    name = models.CharField(max_length=254, choices=TYPE, unique=True)

    def __str__(self):
        return self.name

    def getSettings(self, lang):
        data = HomeSettings.objects.all()
        return getJSONData(data, lang)


class AboutUsSettings(AbstractCompany):
    class Meta:
        verbose_name = 'About Us Settings'
        verbose_name_plural = 'About Us Settings'
    TYPE = (
        ('all_banner_image', 'all_banner_image'),
        ('all_visi_misi_image', 'all_visi_misi_image'),
        ('all_ahlak_bg', 'all_ahlak_bg'),

        ('id_about_us_title', 'id_about_us_title'),
        ('id_about_us_short_description', 'id_about_us_short_description'),
        ('id_about_us_long_description', 'id_about_us_long_description'),
        
        ('id_ahlak_text', 'id_ahlak_text'),

        ('id_award_title', 'id_award_title'),
        ('id_award_sub_title', 'id_award_sub_title'),

        ('id_certificate_title', 'id_certificate_title'),
        ('id_certificate_sub_title', 'id_certificate_sub_title'),

        ('id_jejak_langkah_title', 'id_jejak_langkah_title'),
        ('id_jejak_langkah_sub_title', 'id_jejak_langkah_sub_title'),

        ('id_misi_1_text', 'id_misi_1_text'),
        ('id_misi_2_text', 'id_misi_2_text'),
        ('id_misi_3_text', 'id_misi_3_text'),
        ('id_misi_4_text', 'id_misi_4_text'),
        
        ('id_profile_direksi_title', 'id_profile_direksi_title'),
        ('id_profile_komisaris_title', 'id_profile_komisaris_title'),

        ('id_tata_nilai_title', 'id_tata_nilai_title'),
        ('id_tata_nilai_sub_title', 'id_tata_nilai_sub_title'),

        ('id_visi_misi_title', 'id_visi_misi_title'),
        ('id_visi_misi_sub_title', 'id_visi_misi_sub_title'),

        ('id_visi_title', 'id_visi_title'),
        ('id_misi_title', 'id_misi_title'),

        ('id_visi_text', 'id_visi_text'),

        ('id_yearly_report_title', 'id_yearly_report_title'),
        ('id_yearly_report_sub_title', 'id_yearly_report_sub_title'),

      
        ('en_about_us_title', 'en_about_us_title'),
        ('en_about_us_short_description', 'en_about_us_short_description'),
        ('en_about_us_long_description', 'en_about_us_long_description'),

        ('en_ahlak_text', 'en_ahlak_text'),

        ('en_award_title', 'en_award_title'),
        ('en_award_sub_title', 'en_award_sub_title'),

        ('en_certificate_title', 'en_certificate_title'),
        ('en_certificate_sub_title', 'en_certificate_sub_title'),

        ('en_jejak_langkah_title', 'en_jejak_langkah_title'),
        ('en_jejak_langkah_sub_title', 'en_jejak_langkah_sub_title'),

        ('en_misi_1_text', 'en_misi_1_text'),
        ('en_misi_2_text', 'en_misi_2_text'),
        ('en_misi_3_text', 'en_misi_3_text'),
        ('en_misi_4_text', 'en_misi_4_text'),

        ('en_profile_direksi_title', 'en_profile_direksi_title'),
        ('en_profile_komisaris_title', 'en_profile_komisaris_title'),

        ('en_tata_nilai_title', 'en_tata_nilai_title'),
        ('en_tata_nilai_sub_title', 'en_tata_nilai_sub_title'),

        ('en_visi_misi_title', 'en_visi_misi_title'),
        ('en_visi_misi_sub_title', 'en_visi_misi_sub_title'),
        ('en_visi_title', 'en_visi_title'),
        ('en_misi_title', 'en_misi_title'),
        ('en_visi_text', 'en_visi_text'),

        ('en_yearly_report_title', 'en_yearly_report_title'),
        ('en_yearly_report_sub_title', 'en_yearly_report_sub_title'),       

    )
    name = models.CharField(max_length=254, choices=TYPE, unique=True)

    def __str__(self):
        return self.name

    def getSettings(self, lang):
        data = AboutUsSettings.objects.all()
        return getJSONData(data, lang)


class OurBusinessSettings(AbstractCompany):
    class Meta:
        verbose_name = 'Our Business Settings'
        verbose_name_plural = 'Our Business Settings'
    TYPE = (
        ('id_title', 'id_title'),
        ('id_banner_text', 'id_banner_text'),

        ('en_title', 'en_title'),
        ('en_banner_text', 'en_banner_text'),

        ('all_banner_image', 'banner_image'),
    )
    name = models.CharField(max_length=254, choices=TYPE, unique=True)

    def __str__(self):
        return self.name
    
    def getSettings(self, lang):
        data = OurBusinessSettings.objects.all()
        return getJSONData(data, lang)


class NewsPageSettings(AbstractCompany):
    class Meta:
        verbose_name = 'News Settings'
        verbose_name_plural = 'News Settings'
    TYPE = (
        ('all_banner_image', 'banner_image'),

        ('id_title', 'id_title'),
        ('id_banner_text', 'id_banner_text'),
        ('id_banner_sub_text', 'id_banner_sub_text'),
        ('id_latest_artikel_title', 'id_latest_artikel_title'),
        ('id_latest_news_title', 'id_latest_news_title'),

        ('en_title', 'en_title'),
        ('en_banner_text', 'en_banner_text'),
        ('en_banner_sub_text', 'en_banner_sub_text'),
        ('en_latest_artikel_title', 'en_latest_artikel_title'),
        ('en_latest_news_title', 'en_latest_news_title'),

    )
    name = models.CharField(max_length=254, choices=TYPE, unique=True)

    def __str__(self):
        return self.name

    def getSettings(self, lang):
        data = NewsPageSettings.objects.all()
        return getJSONData(data, lang)


class CarierPageSettings(AbstractCompany):
    class Meta:
        verbose_name = 'Career Settings'
        verbose_name_plural = 'Career Settings'
    TYPE = (
        ('all_banner_image', 'banner_image'),
        ('id_title', 'id_title'),
        ('id_banner_text', 'id_banner_text'),
        ('id_no_career', 'id_no_career'),
        ('id_lowongan', 'id_lowongan'),


        ('en_title', 'en_title'),
        ('en_banner_text', 'en_banner_text'),
        ('en_no_career', 'en_no_career'),
        ('en_lowongan', 'en_lowongan'),

    )
    name = models.CharField(max_length=254, choices=TYPE, unique=True)

    def __str__(self):
        return self.name

    def getSettings(self, lang):
        data = CarierPageSettings.objects.all()
        return getJSONData(data, lang)


class CarierDetailPageSettings(AbstractCompany):
    class Meta:
        verbose_name = 'Career Detail Settings'
        verbose_name_plural = 'Career Detail Settings'
    TYPE = (
        ('all_banner_image', 'banner_image'),

        ('id_lamar_pekerjaan', 'id_lamar_pekerjaan'),
        ('id_bagikan_pekerjaan', 'id_bagikan_pekerjaan'),
        ('id_pekerjaan_serupa', 'id_pekerjaan_serupa'),

        ('en_lamar_pekerjaan', 'en_lamar_pekerjaan'),
        ('en_bagikan_pekerjaan', 'en_bagikan_pekerjaan'),
        ('en_pekerjaan_serupa', 'en_pekerjaan_serupa'),
    )
    name = models.CharField(max_length=254, choices=TYPE, unique=True)

    def __str__(self):
        return self.name

    def getSettings(self, lang):
        data = CarierDetailPageSettings.objects.all()
        return getJSONData(data, lang)


class TenderPageSettings(AbstractCompany):
    class Meta:
        verbose_name = 'Tender Settings'
        verbose_name_plural = 'Tender Settings'
    TYPE = (
        ('all_banner_image', 'all_banner_image'),

        ('id_title', 'id_title'),
        ('id_banner_text', 'id_banner_text'),
        ('id_pengumuman_text', 'id_pengumuman_text'),
        ('id_penutupan_text', 'id_penutupan_text'),
        ('id_no_tender', 'id_no_tender'),

        ('en_title', 'en_title'),
        ('en_banner_text', 'en_banner_text'),
        ('en_pengumuman_text', 'en_pengumuman_text'),
        ('en_penutupan_text', 'en_penutupan_text'),
        ('en_no_tender', 'en_no_tender')
    )
    name = models.CharField(max_length=254, choices=TYPE, unique=True)

    def __str__(self):
        return self.name
    
    def getSettings(self, lang):
        data = TenderPageSettings.objects.all()
        return getJSONData(data, lang)


class ContactUseSettings(AbstractCompany):
    class Meta:
        verbose_name = 'Contact Us Settings'
        verbose_name_plural = 'Contact Us Settings'
    TYPE = (
        ('all_email_address', 'email_address'),
        ('all_phone_number', 'phone_number'),
        ('all_address', 'address'),
        ('all_map_link', 'all_map_link'),

        ('id_title', 'id_title'),
        ('id_sub_title', 'id_sub_title'),
        ('id_telephone_title', 'id_telephone_title'),
        ('id_email_title', 'id_email_title'),
        ('id_address_title', 'id_address_title'),
        ('id_telphone_subtext', 'id_telphone_subtext'),
        ('id_email_sub_text', 'id_email_sub_text'),
        ('id_address_sub_text', 'id_address_sub_text'),
        ('id_contact_form_title', 'id_contact_form_title'),
        ('id_contact_form_sub_title', 'id_contact_form_sub_title'),
        ('id_nama_depan_label', 'id_nama_depan_label'),
        ('id_nama_belakang_label', 'id_nama_belakang_label'),
        ('id_email_label', 'id_email_label'),
        ('id_phone_label', 'id_phone_label'),
        ('id_message_label', 'id_message_label'),
        ('id_button_text', 'id_button_text'),


        ('en_title', 'en_title'),
        ('en_sub_title', 'en_sub_title'),
        ('en_telephone_title', 'en_telephone_title'),
        ('en_email_title', 'en_email_title'),
        ('en_address_title', 'en_address_title'),
        ('en_telphone_subtext', 'en_telphone_subtext'),
        ('en_email_sub_text', 'en_email_sub_text'),
        ('en_address_sub_text', 'en_address_sub_text'),
        ('en_contact_form_title', 'en_contact_form_title'),
        ('en_contact_form_sub_title', 'en_contact_form_sub_title'),
        ('en_nama_depan_label', 'en_nama_depan_label'),
        ('en_nama_belakang_label', 'en_nama_belakang_label'),
        ('en_email_label', 'en_email_label'),
        ('en_phone_label', 'en_phone_label'),
        ('en_message_label', 'en_message_label'),
        ('en_button_text', 'en_button_text'),

    )
    name = models.CharField(max_length=254, choices=TYPE, unique=True)

    def __str__(self):
        return self.name

    def getSettings(self, lang):
        data = ContactUseSettings.objects.all()
        return getJSONData(data, lang)

class InvestorPageSettings(AbstractCompany):
    class Meta:
        verbose_name = 'Investor Settings'
        verbose_name_plural = 'Investor Settings'
    TYPE = (

        ('all_banner_image', 'banner_image'),
        ('all_komitmen_image', 'komitmen_image'),

        ('id_title', 'id_title'),
        ('id_banner_text', 'id_banner_text'),
        ('id_banner_sub_text', 'id_banner_sub_text'),
        ('id_komitmen_text', 'id_komitmen_text'),
        ('id_komitmen_description', 'id_komitmen_description'),
        ('id_yearly_report_title', 'id_yearly_report_title'),
        ('id_yearly_report_sub_title', 'id_yearly_report_sub_title'),

        ('en_title', 'en_title'),
        ('en_banner_text', 'en_banner_text'),
        ('en_banner_sub_text', 'en_banner_sub_text'),
        ('en_komitmen_text', 'en_komitmen_text'),
        ('en_komitmen_description', 'en_komitmen_description'),
        ('en_yearly_report_title', 'en_yearly_report_title'),
        ('en_yearly_report_sub_title', 'en_yearly_report_sub_title')
    )
    name = models.CharField(max_length=254, choices=TYPE, unique=True)

    def __str__(self):
        return self.name

    def getSettings(self, lang):
        data = InvestorPageSettings.objects.all()
        return getJSONData(data, lang)
    

class GalleryPageSettings(AbstractCompany):
    class Meta:
        verbose_name = 'Gallery Settings'
        verbose_name_plural = 'Gallery Settings'
    TYPE = (
        ('all_banner_image', 'all_banner_image'),

        ('id_title', 'id_title'),
        ('id_banner_text', 'id_banner_text'),
        ('id_banner_sub_text', 'id_banner_sub_text'),
        ('id_album_title', 'id_album_title'),

        ('en_title', 'en_title'),
        ('en_banner_text', 'en_banner_text'),
        ('en_banner_sub_text', 'en_banner_sub_text'),
        ('en_album_title', 'en_album_title'),
    )
    name = models.CharField(max_length=254, choices=TYPE, unique=True)

    def __str__(self):
        return self.name

    def getSettings(self, lang):
        data = GalleryPageSettings.objects.all()
        return getJSONData(data, lang)



class TjslPageSettings(AbstractCompany):
    class Meta:
        verbose_name = 'TJSL Settings'
        verbose_name_plural = 'TJSL Settings'
    TYPE = (
        ('all_banner_image', 'all_banner_image'),

        ('id_banner_text', 'id_banner_text'),
        ('id_banner_sub_text', 'id_banner_sub_text'),
        ('id_album_title', 'id_album_title'),

        ('en_banner_text', 'en_banner_text'),
        ('en_banner_sub_text', 'en_banner_sub_text'),
        ('en_album_title', 'en_album_title'),
    )
    name = models.CharField(max_length=254, choices=TYPE, unique=True)

    def __str__(self):
        return self.name

    def getSettings(self, lang):
        data = TjslPageSettings.objects.all()
        return getJSONData(data, lang)



class FooterSettings(AbstractCompany):
    class Meta:
        verbose_name = 'Footer Settings'
        verbose_name_plural = 'Footer Settings'
    TYPE = (
        ('id_ada_pertanyaan', 'id_ada_pertanyaan'),
        ('id_ada_pertanyaan_desc', 'id_ada_pertanyaan_desc'),

        ('id_bisnis_kami', 'id_bisnis_kami'),
        ('id_bisnis_kami_1', 'id_bisnis_kami_1'),
        ('id_bisnis_kami_2', 'id_bisnis_kami_2'),
        ('id_bisnis_kami_3', 'id_bisnis_kami_3'),

        ('id_tentang_kami', 'id_tentang_kami'),
        ('id_quick_links', 'id_quick_links'),
        ('id_info', 'id_info'),
        ('id_langganan_berita', 'id_langganan_berita'),
        ('id_email_placeholder', 'id_email_placeholder'),
        ('id_subscribe_description', 'id_subscribe_description'),
        ('id_sitemap', 'id_sitemap'),
        ('id_kebijakan_privasi', 'id_kebijakan_privasi'),

        # menu
        ('id_profile_perusahaan', 'id_profile_perusahaan'),
        ('id_manajement', 'id_manajement'),
        ('id_penghargaan', 'id_penghargaan'),
        ('id_kontak', 'id_kontak'),
        ('id_beranda', 'id_beranda'),
        ('id_patra_jasa', 'id_patra_jasa'),
        ('id_berita', 'id_berita'),
        ('id_karir', 'id_karir'),
        ('id_tender', 'id_tender'),
        ('id_news', 'id_news'),

        ('en_ada_pertanyaan', 'en_ada_pertanyaan'),
        ('en_ada_pertanyaan_desc', 'en_ada_pertanyaan_desc'),
        ('en_bisnis_kami', 'en_bisnis_kami'),
        ('en_bisnis_kami_1', 'en_bisnis_kami_1'),
        ('en_bisnis_kami_2', 'en_bisnis_kami_2'),
        ('en_bisnis_kami_3', 'en_bisnis_kami_3'),

        ('en_tentang_kami', 'en_tentang_kami'),
        ('en_quick_links', 'en_quick_links'),
        ('en_info', 'en_info'),
        ('en_langganan_berita', 'en_langganan_berita'),
        ('en_email_placeholder', 'en_email_placeholder'),
        ('en_subscribe_description', 'en_subscribe_description'),
        ('en_sitemap', 'en_sitemap'),
        ('en_kebijakan_privasi', 'en_kebijakan_privasi'),
        # menu
        ('en_profile_perusahaan', 'en_profile_perusahaan'),
        ('en_manajement', 'en_manajement'),
        ('en_penghargaan', 'en_penghargaan'),
        ('en_kontak', 'en_kontak'),
        ('en_beranda', 'en_beranda'),
        ('en_patra_jasa', 'en_patra_jasa'),
        ('en_berita', 'en_berita'),
        ('en_karir', 'en_karir'),
        ('en_tender', 'en_tender'),
        ('en_news', 'en_news'),
    )
    name = models.CharField(max_length=254, choices=TYPE, unique=True)

    def __str__(self):
        return self.name

    def getSettings(self, lang):
        data = FooterSettings.objects.all()
        return getJSONData(data, lang)


class CommonSettings(AbstractCompany):
    class Meta:
        verbose_name = 'Common Settings'
        verbose_name_plural = 'Common Settings'
    TYPE = (
        ('id_beranda', 'id_beranda'),
        ('id_bisnis_kami', 'id_bisnis_kami'),
        ('id_selengkapnya', 'id_selengkapnya'),
        ('id_lihat_laporan', 'id_lihat_laporan'),
        ('id_kunjungi_website', 'id_kunjungi_website'),
        ('id_media_informasi', 'id_media_informasi'),
        ('id_hubungi_kami', 'id_hubungi_kami'),
        ('id_karir', 'id_karir'),
        ('id_gallery', 'id_gallery'),
        ('id_tentang_kami', 'id_tentang_kami'),
        ('id_investor', 'id_investor'),
        ('id_profil_direksi', 'id_profil_direksi'),
        ('id_hubungan_investor', 'id_hubungan_investor'),
        ('id_artikel_dan_berita', 'id_artikel_dan_berita'),
        ('id_artikel', 'id_artikel'),
        ('id_berita', 'id_berita'),
        ('id_tender', 'id_tender'),
        ('id_menampilkan', 'id_menampilkan'),
        ('id_tjsl', 'id_tjsl'),
        ('id_portal', 'id_portal'),
        ('id_bagikan', 'id_bagikan'),

        ('en_beranda', 'en_beranda'),
        ('en_bisnis_kami', 'en_bisnis_kami'),
        ('en_selengkapnya', 'en_selengkapnya'),
        ('en_lihat_laporan', 'en_lihat_laporan'),
        ('en_kunjungi_website', 'en_kunjungi_website'),
        ('en_media_informasi', 'en_media_informasi'),
        ('en_hubungi_kami', 'en_hubungi_kami'),
        ('en_karir', 'en_karir'),
        ('en_gallery', 'en_gallery'),
        ('en_tentang_kami', 'en_tentang_kami'),
        ('en_investor', 'en_investor'),
        ('en_profil_direksi', 'en_profil_direksi'),
        ('en_hubungan_investor', 'en_hubungan_investor'),
        ('en_artikel_dan_berita', 'en_artikel_dan_berita'),
        ('en_artikel', 'en_artikel'),
        ('en_berita', 'en_berita'),
        ('en_tender', 'en_tender'),
        ('en_menampilkan', 'en_menampilkan'),
        ('en_tjsl', 'en_tjsl'),
        ('en_portal', 'en_portal'),
        ('en_bagikan', 'en_bagikan'),

        ('all_facebook_url', 'all_facebook_url'),
        ('all_linkedin_url', 'all_linkedin_url'),
        ('all_youtube_url', 'all_youtube_url'),
        ('all_instagram_url', 'all_instagram_url'),

        ('all_seo_title', 'all_seo_title'),
        ('all_seo_description', 'all_seo_description'),
        ('all_seo_fb_id', 'all_seo_fb_id'),
        ('all_website_url', 'all_website_url'),
        ('all_seo_image', 'all_seo_image'),
        ('all_seo_keywords', 'all_seo_keywords'),
        ('all_seo_domain', 'all_seo_domain'),
    )
    name = models.CharField(max_length=254, choices=TYPE, unique=True)

    def __str__(self):
        return self.name
    
    def getSettings(self, lang):
        data = CommonSettings.objects.all()
        return getJSONData(data, lang)


class SearchPageSettings(AbstractCompany):
    class Meta:
        verbose_name = 'Search Settings'
        verbose_name_plural = 'Search Settings'
    TYPE = (
        ('id_not_found_search', 'id_not_found_search'),
        ('id_title', 'id_title'),

        ('en_title', 'en_title'),
        ('en_not_found_search', 'en_not_found_search'),
    )
    name = models.CharField(max_length=254, choices=TYPE, unique=True)

    def __str__(self):
        return self.name

    def getSettings(self, lang):
        data = SearchPageSettings.objects.all()
        return getJSONData(data, lang)

