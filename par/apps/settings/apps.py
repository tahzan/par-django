from django.apps import AppConfig


class SettingsConfig(AppConfig):
    name = 'par.apps.settings'
