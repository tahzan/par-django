from django import template

register = template.Library()

@register.filter
def index(List, i):
    return List[int(i)]

@register.simple_tag
def relative_url(value, field_name, urlencode=None):
    url = '?{}={}'.format(field_name, value)
    if urlencode:
        querystring = urlencode.split('&')
        filtered_querystring = filter(lambda p: p.split('=')[0] != field_name, querystring)
        encoded_querystring = '&'.join(filtered_querystring)
        url = '{}&{}'.format(url, encoded_querystring)
    return url


@register.filter
def replace_lang(value):
    print("VALUE", value)
    if '/en/' in value:
        return f"/id/{value[4:]}"
    else:
        return f"/en/{value[4:]}"


@register.filter
def is_active(things):
    return things.filter(is_active=True)