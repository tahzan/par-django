$(function () {
  
  var x = window.matchMedia("(max-width: 768px)")

  try {
    var perPage = 2;
    if (x.matches) perPage = 1;
    var splide1 = new Splide('#timeline', {
      perPage: perPage,
      perMove: 2,
      pagination: false,
    });
    if (splide1) splide1.mount();
  } catch (error) {
    // console.log(error)
  }

  try {
    var AwperPage = 3;
    if (x.matches) AwperPage = 1;
    var splide2 = new Splide('#awards', {
      perPage: AwperPage,
      perMove: 1,
      pagination: false,
      loop: true,
      padding: '5rem',
      focus: 'center',
    });
    if (splide2) splide2.mount();
  } catch (error) {
    console.log(error)
  }
  
  try {
    var certPerpage = 3;
    if (x.matches) certPerpage = 1;
    var splide3 = new Splide('#cert', {
      perPage: AwperPage,
      perMove: 1,
      pagination: false,
      loop: true,
      padding: '5rem',
      focus: 'center',
    });
    if (splide3) splide3.mount();
  } catch (error) {
    // console.log(error)
  }
  
  try {
    var yearlyPerpage = 3;
    if (x.matches) yearlyPerpage = 1;
    var splide4 = new Splide('#yeearliReport', {
      perPage: AwperPage,
      perMove: 1,
      pagination: false,
      loop: true,
      padding: '5rem',
      focus: 'center',
    });
    if (splide4) splide4.mount();
  } catch (error) {
    // console.log(error)
  }

  try {
    var yearlyPerpage = 3;
    if (x.matches) yearlyPerpage = 1;
    var splide4 = new Splide('#portals', {
      perPage: yearlyPerpage,
      perMove: 1,
      loop: true,
      focus: 'center',
      pagination: false,
      height   : '100vh',
    });
    if (splide4) splide4.mount();
  } catch (error) {
    // console.log(error)
  }

});