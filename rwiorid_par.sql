-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 06, 2022 at 10:47 PM
-- Server version: 10.3.35-MariaDB-cll-lve
-- PHP Version: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rwiorid_par`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add site', 1, 'add_site'),
(2, 'Can change site', 1, 'change_site'),
(3, 'Can delete site', 1, 'delete_site'),
(4, 'Can view site', 1, 'view_site'),
(5, 'Can add log entry', 2, 'add_logentry'),
(6, 'Can change log entry', 2, 'change_logentry'),
(7, 'Can delete log entry', 2, 'delete_logentry'),
(8, 'Can view log entry', 2, 'view_logentry'),
(9, 'Can add permission', 3, 'add_permission'),
(10, 'Can change permission', 3, 'change_permission'),
(11, 'Can delete permission', 3, 'delete_permission'),
(12, 'Can view permission', 3, 'view_permission'),
(13, 'Can add group', 4, 'add_group'),
(14, 'Can change group', 4, 'change_group'),
(15, 'Can delete group', 4, 'delete_group'),
(16, 'Can view group', 4, 'view_group'),
(17, 'Can add content type', 5, 'add_contenttype'),
(18, 'Can change content type', 5, 'change_contenttype'),
(19, 'Can delete content type', 5, 'delete_contenttype'),
(20, 'Can view content type', 5, 'view_contenttype'),
(21, 'Can add session', 6, 'add_session'),
(22, 'Can change session', 6, 'change_session'),
(23, 'Can delete session', 6, 'delete_session'),
(24, 'Can view session', 6, 'view_session'),
(25, 'Can add attachment', 7, 'add_attachment'),
(26, 'Can change attachment', 7, 'change_attachment'),
(27, 'Can delete attachment', 7, 'delete_attachment'),
(28, 'Can view attachment', 7, 'view_attachment'),
(29, 'Can add tag', 8, 'add_tag'),
(30, 'Can change tag', 8, 'change_tag'),
(31, 'Can delete tag', 8, 'delete_tag'),
(32, 'Can view tag', 8, 'view_tag'),
(33, 'Can add tagged item', 9, 'add_taggeditem'),
(34, 'Can change tagged item', 9, 'change_taggeditem'),
(35, 'Can delete tagged item', 9, 'delete_taggeditem'),
(36, 'Can view tagged item', 9, 'view_taggeditem'),
(37, 'Can add user', 10, 'add_user'),
(38, 'Can change user', 10, 'change_user'),
(39, 'Can delete user', 10, 'delete_user'),
(40, 'Can view user', 10, 'view_user'),
(41, 'Can add Album', 11, 'add_album'),
(42, 'Can change Album', 11, 'change_album'),
(43, 'Can delete Album', 11, 'delete_album'),
(44, 'Can view Album', 11, 'view_album'),
(45, 'Can add Award', 12, 'add_award'),
(46, 'Can change Award', 12, 'change_award'),
(47, 'Can delete Award', 12, 'delete_award'),
(48, 'Can view Award', 12, 'view_award'),
(49, 'Can add Home Banner', 13, 'add_banner'),
(50, 'Can change Home Banner', 13, 'change_banner'),
(51, 'Can delete Home Banner', 13, 'delete_banner'),
(52, 'Can view Home Banner', 13, 'view_banner'),
(53, 'Can add Certicate', 14, 'add_certicate'),
(54, 'Can change Certicate', 14, 'change_certicate'),
(55, 'Can delete Certicate', 14, 'delete_certicate'),
(56, 'Can view Certicate', 14, 'view_certicate'),
(57, 'Can add Our Client', 15, 'add_client'),
(58, 'Can change Our Client', 15, 'change_client'),
(59, 'Can delete Our Client', 15, 'delete_client'),
(60, 'Can view Our Client', 15, 'view_client'),
(61, 'Can add Contact', 16, 'add_contact'),
(62, 'Can change Contact', 16, 'change_contact'),
(63, 'Can delete Contact', 16, 'delete_contact'),
(64, 'Can view Contact', 16, 'view_contact'),
(65, 'Can add Director Profile', 17, 'add_directorprofile'),
(66, 'Can change Director Profile', 17, 'change_directorprofile'),
(67, 'Can delete Director Profile', 17, 'delete_directorprofile'),
(68, 'Can view Director Profile', 17, 'view_directorprofile'),
(69, 'Can add History', 18, 'add_history'),
(70, 'Can change History', 18, 'change_history'),
(71, 'Can delete History', 18, 'delete_history'),
(72, 'Can view History', 18, 'view_history'),
(73, 'Can add Top Menu', 19, 'add_menu'),
(74, 'Can change Top Menu', 19, 'change_menu'),
(75, 'Can delete Top Menu', 19, 'delete_menu'),
(76, 'Can view Top Menu', 19, 'view_menu'),
(77, 'Can add Our Business', 20, 'add_ourbusiness'),
(78, 'Can change Our Business', 20, 'change_ourbusiness'),
(79, 'Can delete Our Business', 20, 'delete_ourbusiness'),
(80, 'Can view Our Business', 20, 'view_ourbusiness'),
(81, 'Can add Portal', 21, 'add_portal'),
(82, 'Can change Portal', 21, 'change_portal'),
(83, 'Can delete Portal', 21, 'delete_portal'),
(84, 'Can view Portal', 21, 'view_portal'),
(85, 'Can add TataNilai', 22, 'add_tatanilai'),
(86, 'Can change TataNilai', 22, 'change_tatanilai'),
(87, 'Can delete TataNilai', 22, 'delete_tatanilai'),
(88, 'Can view TataNilai', 22, 'view_tatanilai'),
(89, 'Can add Tender', 23, 'add_tender'),
(90, 'Can change Tender', 23, 'change_tender'),
(91, 'Can delete Tender', 23, 'delete_tender'),
(92, 'Can view Tender', 23, 'view_tender'),
(93, 'Can add Yearly Report', 24, 'add_yearlyreport'),
(94, 'Can change Yearly Report', 24, 'change_yearlyreport'),
(95, 'Can delete Yearly Report', 24, 'delete_yearlyreport'),
(96, 'Can view Yearly Report', 24, 'view_yearlyreport'),
(97, 'Can add News', 25, 'add_news'),
(98, 'Can change News', 25, 'change_news'),
(99, 'Can delete News', 25, 'delete_news'),
(100, 'Can view News', 25, 'view_news'),
(101, 'Can add Gallery', 26, 'add_gallery'),
(102, 'Can change Gallery', 26, 'change_gallery'),
(103, 'Can delete Gallery', 26, 'delete_gallery'),
(104, 'Can view Gallery', 26, 'view_gallery'),
(105, 'Can add Career', 27, 'add_carier'),
(106, 'Can change Career', 27, 'change_carier'),
(107, 'Can delete Career', 27, 'delete_carier'),
(108, 'Can view Career', 27, 'view_carier'),
(109, 'Can add Career Category', 28, 'add_cariercategory'),
(110, 'Can change Career Category', 28, 'change_cariercategory'),
(111, 'Can delete Career Category', 28, 'delete_cariercategory'),
(112, 'Can view Career Category', 28, 'view_cariercategory'),
(113, 'Can add News Category', 29, 'add_newstag'),
(114, 'Can change News Category', 29, 'change_newstag'),
(115, 'Can delete News Category', 29, 'delete_newstag'),
(116, 'Can view News Category', 29, 'view_newstag'),
(117, 'Can add AboutUs Settings', 30, 'add_aboutussettings'),
(118, 'Can change AboutUs Settings', 30, 'change_aboutussettings'),
(119, 'Can delete AboutUs Settings', 30, 'delete_aboutussettings'),
(120, 'Can view AboutUs Settings', 30, 'view_aboutussettings'),
(121, 'Can add Home Settings', 31, 'add_homesettings'),
(122, 'Can change Home Settings', 31, 'change_homesettings'),
(123, 'Can delete Home Settings', 31, 'delete_homesettings'),
(124, 'Can view Home Settings', 31, 'view_homesettings'),
(125, 'Can add Our Business Settings', 32, 'add_ourbusinesssettings'),
(126, 'Can change Our Business Settings', 32, 'change_ourbusinesssettings'),
(127, 'Can delete Our Business Settings', 32, 'delete_ourbusinesssettings'),
(128, 'Can view Our Business Settings', 32, 'view_ourbusinesssettings'),
(129, 'Can add Career Settings', 33, 'add_carierpagesettings'),
(130, 'Can change Career Settings', 33, 'change_carierpagesettings'),
(131, 'Can delete Career Settings', 33, 'delete_carierpagesettings'),
(132, 'Can view Career Settings', 33, 'view_carierpagesettings'),
(133, 'Can add ContactUs Settings', 34, 'add_contactusesettings'),
(134, 'Can change ContactUs Settings', 34, 'change_contactusesettings'),
(135, 'Can delete ContactUs Settings', 34, 'delete_contactusesettings'),
(136, 'Can view ContactUs Settings', 34, 'view_contactusesettings'),
(137, 'Can add News Settings', 35, 'add_newspagesettings'),
(138, 'Can change News Settings', 35, 'change_newspagesettings'),
(139, 'Can delete News Settings', 35, 'delete_newspagesettings'),
(140, 'Can view News Settings', 35, 'view_newspagesettings'),
(141, 'Can add Tender Settings', 36, 'add_tenderpagesettings'),
(142, 'Can change Tender Settings', 36, 'change_tenderpagesettings'),
(143, 'Can delete Tender Settings', 36, 'delete_tenderpagesettings'),
(144, 'Can view Tender Settings', 36, 'view_tenderpagesettings'),
(145, 'Can add Investor Settings', 37, 'add_investorpagesettings'),
(146, 'Can change Investor Settings', 37, 'change_investorpagesettings'),
(147, 'Can delete Investor Settings', 37, 'delete_investorpagesettings'),
(148, 'Can view Investor Settings', 37, 'view_investorpagesettings'),
(149, 'Can add Gallery Settings', 38, 'add_gallerypagesettings'),
(150, 'Can change Gallery Settings', 38, 'change_gallerypagesettings'),
(151, 'Can delete Gallery Settings', 38, 'delete_gallerypagesettings'),
(152, 'Can view Gallery Settings', 38, 'view_gallerypagesettings'),
(153, 'Can add Footer Settings', 39, 'add_footersettings'),
(154, 'Can change Footer Settings', 39, 'change_footersettings'),
(155, 'Can delete Footer Settings', 39, 'delete_footersettings'),
(156, 'Can view Footer Settings', 39, 'view_footersettings'),
(157, 'Can add Common Settings', 40, 'add_commonsettings'),
(158, 'Can change Common Settings', 40, 'change_commonsettings'),
(159, 'Can delete Common Settings', 40, 'delete_commonsettings'),
(160, 'Can view Common Settings', 40, 'view_commonsettings'),
(161, 'Can add Career Detail Settings', 41, 'add_carierdetailpagesettings'),
(162, 'Can change Career Detail Settings', 41, 'change_carierdetailpagesettings'),
(163, 'Can delete Career Detail Settings', 41, 'delete_carierdetailpagesettings'),
(164, 'Can view Career Detail Settings', 41, 'view_carierdetailpagesettings'),
(165, 'Can add TJSL Settings', 42, 'add_tjslpagesettings'),
(166, 'Can change TJSL Settings', 42, 'change_tjslpagesettings'),
(167, 'Can delete TJSL Settings', 42, 'delete_tjslpagesettings'),
(168, 'Can view TJSL Settings', 42, 'view_tjslpagesettings'),
(169, 'Can add Search Settings', 43, 'add_searchpagesettings'),
(170, 'Can change Search Settings', 43, 'change_searchpagesettings'),
(171, 'Can delete Search Settings', 43, 'delete_searchpagesettings'),
(172, 'Can view Search Settings', 43, 'view_searchpagesettings');

-- --------------------------------------------------------

--
-- Table structure for table `content_album`
--

CREATE TABLE `content_album` (
  `id` int(11) NOT NULL,
  `img` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `slug` varchar(254) NOT NULL,
  `en_title` longtext DEFAULT NULL,
  `id_title` longtext DEFAULT NULL,
  `total_photo` int(11) DEFAULT NULL,
  `total_video` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `content_album`
--

INSERT INTO `content_album` (`id`, `img`, `date`, `is_active`, `slug`, `en_title`, `id_title`, `total_photo`, `total_video`) VALUES
(1, 'static_files/banner/2022/8/image.png', '2022-08-26', 1, 'acara-kebersamaan-2019458', 'En Acara Kebersamaan', 'Acara Kebersamaan', 0, 0),
(2, 'static_files/banner/2022/8/lomba-kemerdekaan.png', '2022-08-26', 1, 'b', 'Lomba Kemerdekaan', 'Lomba Kemerdekaan', 0, 0),
(3, 'static_files/banner/2022/8/acara-ulang-tahun.png', '2022-08-26', 1, 'c', 'Acara Ulang Tahun', 'Acara Ulang Tahun', 0, 0),
(4, 'static_files/banner/2022/8/pelatihan-anggota.png', '2022-08-26', 1, 'pelatihan-anggota-4575393', 'Pelatihan Anggota', 'Pelatihan Anggota', 0, 0),
(5, 'static_files/banner/2022/8/pelantikan-pimpinan-baru.png', '2022-08-26', 1, 'e', 'Pelantikan Pimpinan Baru', 'Pelantikan Pimpinan Baru', 0, 0),
(6, 'static_files/banner/2022/8/peresmian-kantor-surabaya.png', '2022-08-26', 1, 'peresmian-kantor-surabaya-236562', 'Peresmian Kantor Surabaya', 'Peresmian Kantor Surabaya', 2, 1),
(7, 'static_files/banner/2022/9/auto.png', '2022-09-05', 1, 'tjsl-4368439', 'TJSL', 'TJSL', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `content_award`
--

CREATE TABLE `content_award` (
  `id` int(11) NOT NULL,
  `img` varchar(100) NOT NULL,
  `id_description` longtext NOT NULL,
  `en_description` longtext NOT NULL,
  `month_year` varchar(254) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `content_award`
--

INSERT INTO `content_award` (`id`, `img`, `id_description`, `en_description`, `month_year`) VALUES
(1, 'static_files/banner/2022/8/award-1.png', 'Fleet Appreciation\r\nToyota Astra Motor', 'Fleet Appreciation\r\nToyota Astra Motor', 'APRIL 2018'),
(2, 'static_files/banner/2022/8/award-2.png', 'Fleet Appreciation\r\nAgung Auto Toyota', 'Fleet Appreciation\r\nAgung Auto Toyota', 'JULI 2018'),
(3, 'static_files/banner/2022/8/award-3.png', 'Penghargaan Nusantara Berlian Motor', 'Penghargaan Nusantara Berlian Motor', 'OKTOBER 2018');

-- --------------------------------------------------------

--
-- Table structure for table `content_banner`
--

CREATE TABLE `content_banner` (
  `id` int(11) NOT NULL,
  `img` varchar(100) NOT NULL,
  `id_big_text` longtext DEFAULT NULL,
  `en_big_text` longtext DEFAULT NULL,
  `id_small_text` longtext DEFAULT NULL,
  `en_small_text` longtext DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `content_banner`
--

INSERT INTO `content_banner` (`id`, `img`, `id_big_text`, `en_big_text`, `id_small_text`, `en_small_text`, `is_active`) VALUES
(1, 'static_files/banner/2022/8/banner.png', 'Layanan Penyewaan <br /> dan Jasa Pengemudi Kendaraan <br /> Profesional dan Tersertifikasi', 'RENT SERVICES <br /> dan Jasa Pengemudi Kendaraan <br /> Profesional dan Tersertifikasi', 'Layanan Penyewaan <br /> dan Jasa Pengemudi Kendaraan <br /> Profesional dan Tersertifikasi', 'Layanan Penyewaan <br /> dan Jasa Pengemudi Kendaraan <br /> Profesional dan Tersertifikasi', 1);

-- --------------------------------------------------------

--
-- Table structure for table `content_carier`
--

CREATE TABLE `content_carier` (
  `id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `id_title` varchar(254) NOT NULL,
  `en_title` varchar(254) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `city` varchar(254) DEFAULT NULL,
  `country` varchar(254) DEFAULT NULL,
  `type` varchar(254) DEFAULT NULL,
  `en_description` longtext DEFAULT NULL,
  `id_description` longtext DEFAULT NULL,
  `slug` varchar(254) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `content_carier`
--

INSERT INTO `content_carier` (`id`, `is_active`, `id_title`, `en_title`, `category_id`, `city`, `country`, `type`, `en_description`, `id_description`, `slug`) VALUES
(2, 1, 'Lowongan Kerja 1', 'en Lowongan Kerja 1', 1, 'Jakarta', 'Indonesia', 'full_time', '<p>en Lowongan Kerja 1<br></p>', '<p>Lowongan Kerja 1<br></p>', 'lowongan-kerja-1-3023033');

-- --------------------------------------------------------

--
-- Table structure for table `content_cariercategory`
--

CREATE TABLE `content_cariercategory` (
  `id` int(11) NOT NULL,
  `en_title` varchar(254) DEFAULT NULL,
  `id_title` varchar(254) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `content_cariercategory`
--

INSERT INTO `content_cariercategory` (`id`, `en_title`, `id_title`) VALUES
(1, 'Business', 'Bisnis'),
(2, 'Technician', 'Teknisi');

-- --------------------------------------------------------

--
-- Table structure for table `content_certicate`
--

CREATE TABLE `content_certicate` (
  `id` int(11) NOT NULL,
  `img` varchar(100) NOT NULL,
  `id_description` longtext NOT NULL,
  `en_description` longtext NOT NULL,
  `year` varchar(254) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `slug` varchar(254) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `content_certicate`
--

INSERT INTO `content_certicate` (`id`, `img`, `id_description`, `en_description`, `year`, `is_active`, `slug`) VALUES
(1, 'static_files/banner/2022/8/sertif-1.png', 'Certificate ISO 37001:2016 <br /> dari TUV Rheinland untuk penerapan <br /> <br /> System Manajemen Anti Penyuapan <br /> Tanggal 20 Desember 2021', 'Certificate ISO 37001:2016 <br /> dari TUV Rheinland untuk penerapan <br /> <br /> System Manajemen Anti Penyuapan <br /> Tanggal 20 Desember 2021', 'Tanggal 20 Desember 2021', 1, ''),
(2, 'static_files/banner/2022/8/sertif-2.png', 'Certificate ISO 37001:2016 <br /> dari TUV Rheinland untuk penerapan <br />  <br /> System Manajemen Anti Penyuapan <br /> Tanggal 20 Desember 2021', 'Certificate ISO 37001:2016 <br /> dari TUV Rheinland untuk penerapan <br />  <br /> System Manajemen Anti Penyuapan <br /> Tanggal 20 Desember 2021', 'Tanggal 20 Desember 2021', 1, 'tes'),
(3, 'static_files/banner/2022/8/sertif-3.png', 'Certificate ISO 37001:2016 <br /> dari TUV Rheinland untuk penerapan <br /> <br /> System Manajemen Anti Penyuapan <br /> Tanggal 20 Desember 2021', 'Certificate ISO 37001:2016 <br /> dari TUV Rheinland untuk penerapan <br /> <br /> System Manajemen Anti Penyuapan <br /> Tanggal 20 Desember 2021', 'Tanggal 20 Desember 2021', 1, 'test');

-- --------------------------------------------------------

--
-- Table structure for table `content_client`
--

CREATE TABLE `content_client` (
  `id` int(11) NOT NULL,
  `img` varchar(100) NOT NULL,
  `title` varchar(254) NOT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `content_client`
--

INSERT INTO `content_client` (`id`, `img`, `title`, `is_active`) VALUES
(1, 'static_files/banner/2022/8/pertamina-1.png', 'pertamina 1', 1),
(2, 'static_files/banner/2022/8/pertamina-2.png', 'pertamina 2', 1),
(3, 'static_files/banner/2022/8/pertamina-3.png', 'pertamina 3', 1),
(4, 'static_files/banner/2022/8/pertamina-4.png', 'pertamina 4', 1),
(5, 'static_files/banner/2022/8/klien-1.png', 'klien 1', 1),
(6, 'static_files/banner/2022/8/klien-2.png', 'klien 2', 1),
(7, 'static_files/banner/2022/8/klien-3.png', 'klien 3', 1),
(8, 'static_files/banner/2022/8/klien-4.png', 'klien 4', 1);

-- --------------------------------------------------------

--
-- Table structure for table `content_contact`
--

CREATE TABLE `content_contact` (
  `id` int(11) NOT NULL,
  `first_name` varchar(254) NOT NULL,
  `last_name` varchar(254) NOT NULL,
  `phone_number` varchar(254) NOT NULL,
  `message` longtext NOT NULL,
  `email` varchar(254) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `content_contact`
--

INSERT INTO `content_contact` (`id`, `first_name`, `last_name`, `phone_number`, `message`, `email`) VALUES
(1, 'sdfsdfsd', 'sdfsdfsd', '08008808', 'sdfsdfsdfsdfsdfsd', 'sfsdfds@gmail.com'),
(2, 'kjkjkj', 'sdadasd', '021312312', 'dsfsdfsdfsd', 'ssss@gmcil.com');

-- --------------------------------------------------------

--
-- Table structure for table `content_directorprofile`
--

CREATE TABLE `content_directorprofile` (
  `id` int(11) NOT NULL,
  `name` varchar(254) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `id_long_desc` longtext NOT NULL,
  `en_long_desc` longtext NOT NULL,
  `type` varchar(254) NOT NULL,
  `img` varchar(100) DEFAULT NULL,
  `en_position` varchar(254) DEFAULT NULL,
  `id_position` varchar(254) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `content_directorprofile`
--

INSERT INTO `content_directorprofile` (`id`, `name`, `is_active`, `id_long_desc`, `en_long_desc`, `type`, `img`, `en_position`, `id_position`) VALUES
(1, 'Harnawan Santoso', 1, 'Warga Negara Indonesia, lahir di Jakarta, 12 November 1969. Ia merupakan Sarjana Teknik, Fakultas Teknik Jurusan Sipil Universitas Indonesia (UI) dan memiliki pengalaman lebih dari 23 tahun', 'en Warga Negara Indonesia, lahir di Jakarta, 12 November 1969. Ia merupakan Sarjana Teknik, Fakultas Teknik Jurusan Sipil Universitas Indonesia (UI) dan memiliki pengalaman lebih dari 23 tahun', 'direksi', 'static_files/profile_direktur/2022/9/direksi3.png', 'en Direkur Operasi', 'Direkur Operasi'),
(7, 'Mabroer MS', 1, 'Warga Negara Indonesia, lahir di Jakarta, 12 November 1969. Ia merupakan Sarjana Teknik, Fakultas Teknik Jurusan Sipil Universitas Indonesia (UI) dan memiliki pengalaman lebih dari 23 tahun', 'en Warga Negara Indonesia, lahir di Jakarta, 12 November 1969. Ia merupakan Sarjana Teknik, Fakultas Teknik Jurusan Sipil Universitas Indonesia (UI) dan memiliki pengalaman lebih dari 23 tahun', 'komisaris', 'static_files/profile_direktur/2022/9/direksi4.png', 'en Direktur Utama', 'Direktur Utama'),
(6, 'Wahyu Witjaksono', 1, 'Warga Negara Indonesia, lahir di Jakarta, 12 November 1969. Ia merupakan Sarjana Teknik, Fakultas Teknik Jurusan Sipil Universitas Indonesia (UI) dan memiliki pengalaman lebih dari 23 tahun', 'en Warga Negara Indonesia, lahir di Jakarta, 12 November 1969. Ia merupakan Sarjana Teknik, Fakultas Teknik Jurusan Sipil Universitas Indonesia (UI) dan memiliki pengalaman lebih dari 23 tahun', 'direksi', 'static_files/profile_direktur/2022/9/direksi1.png', 'en Direktur Utama', 'Direktur Utama'),
(5, 'Anjas Jati Kesuma', 1, 'Warga Negara Indonesia, lahir di Jakarta, 12 November 1969. Ia merupakan Sarjana Teknik, Fakultas Teknik Jurusan Sipil Universitas Indonesia (UI) dan memiliki pengalaman lebih dari 23 tahun', 'en Warga Negara Indonesia, lahir di Jakarta, 12 November 1969. Ia merupakan Sarjana Teknik, Fakultas Teknik Jurusan Sipil Universitas Indonesia (UI) dan memiliki pengalaman lebih dari 23 tahun', 'direksi', 'static_files/profile_direktur/2022/9/direksi2.png', 'en Direktur Keuangan & SDM', 'Direktur Keuangan & SDM');

-- --------------------------------------------------------

--
-- Table structure for table `content_gallery`
--

CREATE TABLE `content_gallery` (
  `id` int(11) NOT NULL,
  `img` varchar(100) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `album_id` int(11) NOT NULL,
  `type` varchar(254) DEFAULT NULL,
  `en_title` varchar(254) DEFAULT NULL,
  `id_title` varchar(254) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `content_gallery`
--

INSERT INTO `content_gallery` (`id`, `img`, `is_active`, `album_id`, `type`, `en_title`, `id_title`) VALUES
(1, 'static_files/banner/2022/9/driver_ivByY56.png', 1, 7, 'foto', 'en Title Foto', 'Title Foto'),
(2, 'static_files/banner/2022/9/file-example-mp4-480-1-5mg.mp4', 1, 6, 'video', NULL, NULL),
(3, 'static_files/banner/2022/9/driver_s6TUWsj.png', 1, 6, 'foto', NULL, NULL),
(4, 'static_files/banner/2022/9/banner.png', 1, 6, 'foto', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `content_history`
--

CREATE TABLE `content_history` (
  `id` int(11) NOT NULL,
  `id_long_desc` longtext NOT NULL,
  `en_long_desc` longtext NOT NULL,
  `year` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `content_history`
--

INSERT INTO `content_history` (`id`, `id_long_desc`, `en_long_desc`, `year`, `is_active`) VALUES
(1, '<p><span class=\"my-text-blue\">Tahun 2021<span> - Total kendaraan tersewa sebanyak The total number of rented vehicles was 3.355 units. 3,355 unit.</p>\r\n<p><span class=\"my-text-blue\">2 November 2021<span> - Perseroan bersinergi dengan PT Pertamina Lubricants untuk kerja sama selama 3 tahun.</p>\r\n<p><span class=\"my-text-blue\">Desember 2021<span> - Perseroan meraih sertifikasi ISO 37001:2016 untuk Sistem Manajemen Anti-Suap</p>', '<p><span class=\"my-text-blue\">Tahun 2021<span> - Total kendaraan tersewa sebanyak The total number of rented vehicles was 3.355 units. 3,355 unit.</p>\r\n<p><span class=\"my-text-blue\">2 November 2021<span> - Perseroan bersinergi dengan PT Pertamina Lubricants untuk kerja sama selama 3 tahun.</p>\r\n<p><span class=\"my-text-blue\">Desember 2021<span> - Perseroan meraih sertifikasi ISO 37001:2016 untuk Sistem Manajemen Anti-Suap</p>', 2022, 1),
(2, '<p><span class=\"my-text-blue\">Tahun 2021<span> - Total kendaraan tersewa sebanyak The total number of rented vehicles was 3.355 units. 3,355 unit.</p>\r\n<p><span class=\"my-text-blue\">2 November 2021<span> - Perseroan bersinergi dengan PT Pertamina Lubricants untuk kerja sama selama 3 tahun.</p>\r\n<p><span class=\"my-text-blue\">Desember 2021<span> - Perseroan meraih sertifikasi ISO 37001:2016 untuk Sistem Manajemen Anti-Suap</p>', '<p><span class=\"my-text-blue\">Tahun 2021<span> - Total kendaraan tersewa sebanyak The total number of rented vehicles was 3.355 units. 3,355 unit.</p>\r\n<p><span class=\"my-text-blue\">2 November 2021<span> - Perseroan bersinergi dengan PT Pertamina Lubricants untuk kerja sama selama 3 tahun.</p>\r\n<p><span class=\"my-text-blue\">Desember 2021<span> - Perseroan meraih sertifikasi ISO 37001:2016 untuk Sistem Manajemen Anti-Suap</p>', 2021, 1);

-- --------------------------------------------------------

--
-- Table structure for table `content_menu`
--

CREATE TABLE `content_menu` (
  `id` int(11) NOT NULL,
  `id_title` varchar(254) DEFAULT NULL,
  `en_title` varchar(254) DEFAULT NULL,
  `order` smallint(5) UNSIGNED NOT NULL CHECK (`order` >= 0)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `content_menu`
--

INSERT INTO `content_menu` (`id`, `id_title`, `en_title`, `order`) VALUES
(1, 'Beranda', 'Home', 1);

-- --------------------------------------------------------

--
-- Table structure for table `content_news`
--

CREATE TABLE `content_news` (
  `id` int(11) NOT NULL,
  `img` varchar(100) NOT NULL,
  `id_title` varchar(254) NOT NULL,
  `id_short_desc` varchar(254) NOT NULL,
  `id_long_desc` longtext NOT NULL,
  `en_title` varchar(254) NOT NULL,
  `en_short_desc` varchar(254) NOT NULL,
  `en_long_desc` longtext NOT NULL,
  `date` date NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `slug` varchar(254) NOT NULL,
  `type` varchar(254) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `content_news`
--

INSERT INTO `content_news` (`id`, `img`, `id_title`, `id_short_desc`, `id_long_desc`, `en_title`, `en_short_desc`, `en_long_desc`, `date`, `is_active`, `slug`, `type`) VALUES
(1, 'static_files/banner/2022/9/tentang3.png', 'Peresemian Kantor Baru di Surabaya Sebagai Langkah Pengembagan Bisnis', 'Peresemian Kantor Baru di Surabaya Sebagai Langkah Pengembagan Bisnis', '<p>Sample News sss<br></p>', 'en Peresemian eng Kantor Baru di Surabaya Sebagai Langkah Pengembagan Bisnis', 'en Peresemian Kantor Baru di Surabaya Sebagai Langkah Pengembagan Bisnis', '<p>en Sample News 123<br></p>', '2022-09-04', 1, 'sample-news-3794622', 'news'),
(2, 'static_files/banner/2022/9/tentang2_MpfVBR2.png', 'Penghargaan ke-6 PT Prima Armada Raya di Top Company Awards 2022', 'Sample Article', '<p>Sample Article<br></p>', 'en Penghargaan ke-6 en PT Prima Armada Raya di Top Company Awards 2022', 'en Sample Article', '<p>en Sample Article<br></p>', '2022-09-04', 1, 'sample-article-2848155', 'artikel');

-- --------------------------------------------------------

--
-- Table structure for table `content_newstag`
--

CREATE TABLE `content_newstag` (
  `id` int(11) NOT NULL,
  `en_title` varchar(254) DEFAULT NULL,
  `id_title` varchar(254) DEFAULT NULL,
  `bg_color` varchar(10) DEFAULT NULL,
  `color` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `content_newstag`
--

INSERT INTO `content_newstag` (`id`, `en_title`, `id_title`, `bg_color`, `color`) VALUES
(1, 'Company', 'Perusahaan', '#F9F5FF', '#7ABF50'),
(2, 'Leadership', 'Kepemimpinan', '#FFF1F3', '#C01048'),
(3, 'Vehicle', 'Kendaraan', '#ECFDF3', '#027A48'),
(4, 'Car', 'Mobil', '#FDF2FA', '#C11574');

-- --------------------------------------------------------

--
-- Table structure for table `content_news_categories`
--

CREATE TABLE `content_news_categories` (
  `id` int(11) NOT NULL,
  `news_id` int(11) NOT NULL,
  `newstag_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `content_news_categories`
--

INSERT INTO `content_news_categories` (`id`, `news_id`, `newstag_id`) VALUES
(1, 1, 2),
(2, 1, 3),
(4, 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `content_ourbusiness`
--

CREATE TABLE `content_ourbusiness` (
  `id` int(11) NOT NULL,
  `img` varchar(100) NOT NULL,
  `logo` varchar(100) NOT NULL,
  `id_title` varchar(254) NOT NULL,
  `id_short_desc` varchar(254) NOT NULL,
  `id_long_desc` longtext NOT NULL,
  `en_title` varchar(254) NOT NULL,
  `en_short_desc` varchar(254) NOT NULL,
  `en_long_desc` longtext NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `slug` varchar(254) NOT NULL,
  `order` int(10) UNSIGNED NOT NULL CHECK (`order` >= 0)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `content_ourbusiness`
--

INSERT INTO `content_ourbusiness` (`id`, `img`, `logo`, `id_title`, `id_short_desc`, `id_long_desc`, `en_title`, `en_short_desc`, `en_long_desc`, `is_active`, `slug`, `order`) VALUES
(1, 'static_files/banner/2022/8/bisnis-1.png', 'static_files/logo/2022/8/logo-prima.png', 'Prima Fleet Services', 'Penyediaan Jasa Sewa Kendaraan Profesional', '<p>Penyediaan jasa sewa transportasi kendaraan dengan skema :</p>\r\n<ul class=\"mb-5\">\r\n    <li><b>KONTRAK PANJANG</b> <br/>\r\nMasa sewa ? 1 tahun (time charter / long term)</li>\r\n    <li><b>KONTRAK PENDEK</b> <br/>\r\nmasa sewa < 1 tahun (spot charter/ spot term/ daily/ retail)</li>\r\n</ul>', 'en Prima Fleet Services', 'en Penyediaan Jasa Sewa Kendaraan Profesional', '<p>en Penyediaan jasa sewa transportasi kendaraan dengan skema :</p>\r\n<ul class=\"mb-5\">\r\n    <li><b>KONTRAK PANJANG</b> <br/>\r\nMasa sewa ? 1 tahun (time charter / long term)</li>\r\n    <li><b>KONTRAK PENDEK</b> <br/>\r\nmasa sewa < 1 tahun (spot charter/ spot term/ daily/ retail)</li>\r\n</ul>', 1, 'news', 0),
(2, 'static_files/banner/2022/8/bisnis-2_q0QL2h8.png', 'static_files/logo/2022/8/drivermanagement.png', 'Prima Driver Management', 'Pengemudi berpengalaman dan profesional 24 jam', '<p>Penyediaan jasa layanan pengemudi dengan skema kontrak panjang (long term) dan pendek (short term) </p>\r\n<ul >\r\n    <li>Jasa layanan pengemudi dengan kendaraan</li>\r\n    <li>Jasa layanan pengemudi tanpa kendaraan\r\n</li>\r\n</ul>\r\n<p class=\"mb-5\"><b>Layanan pengemudi profesional untuk keperluan:</b><br/>\r\nFull-dedicated, pool services, dan on-call service\r\n</p>', 'en Prima Driver Management', 'en Pengemudi berpengalaman dan profesional 24 jam', '<p>en Penyediaan jasa layanan pengemudi dengan skema kontrak panjang (long term) dan pendek (short term) </p>\r\n<ul >\r\n    <li>Jasa layanan pengemudi dengan kendaraan</li>\r\n    <li>Jasa layanan pengemudi tanpa kendaraan\r\n</li>\r\n</ul>\r\n<p class=\"mb-5\"><b>Layanan pengemudi profesional untuk keperluan:</b><br/>\r\nFull-dedicated, pool services, dan on-call service\r\n</p>', 1, 'bisnis-kami-2-6554027', 0),
(3, 'static_files/banner/2022/8/bisnis-3_dWJxz2A.png', 'static_files/logo/2022/8/autocare.png', 'Prima Auto Care & Sales', 'Layanan perawatan dapat datang ke rumah atau kantor anda', '<p>Penyediaan jasa perawatan dan/atau perbaikan kendaraan dengan  dukungan jaringan bengkel dan suku cadang asli ex-ATPM (OEM)</p>\r\n<p>Penjualan kendaraan pasca kontrak sewa berakhir dengan skema  penjualan secara:\r\n</p>\r\n<ul class=\"mb-5\">\r\n<li>Langsung (direct sales)</li>\r\n<li>Lelang (bekerjasama dengan Balai Lelang)</li>\r\n<li>Kolektif (bulk)</li>\r\n</ul>', 'en Prima Auto Care & Sales', 'en Layanan perawatan dapat datang ke rumah atau kantor anda', '<p>en Penyediaan jasa perawatan dan/atau perbaikan kendaraan dengan  dukungan jaringan bengkel dan suku cadang asli ex-ATPM (OEM)</p>\r\n<p>Penjualan kendaraan pasca kontrak sewa berakhir dengan skema  penjualan secara:\r\n</p>\r\n<ul class=\"mb-5\">\r\n<li>Langsung (direct sales)</li>\r\n<li>Lelang (bekerjasama dengan Balai Lelang)</li>\r\n<li>Kolektif (bulk)</li>\r\n</ul>', 1, 'bisnis-kami-3-0488956', 0);

-- --------------------------------------------------------

--
-- Table structure for table `content_portal`
--

CREATE TABLE `content_portal` (
  `id` int(11) NOT NULL,
  `img` varchar(100) NOT NULL,
  `id_big_text` longtext DEFAULT NULL,
  `en_big_text` longtext DEFAULT NULL,
  `id_small_text` longtext DEFAULT NULL,
  `en_small_text` longtext DEFAULT NULL,
  `link` varchar(254) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `content_portal`
--

INSERT INTO `content_portal` (`id`, `img`, `id_big_text`, `en_big_text`, `id_small_text`, `en_small_text`, `link`, `is_active`) VALUES
(1, 'static_files/banner/2022/9/jual-mobil.png', 'Jual Mobil', 'Selling Cars', 'Anak usaha Prima Armada Raya yang melakukan penjualan mobil bekas berkualitas', 'Subsidiary of Prima Armada Raya that sells quality used cars', 'https://youtube.com', 1),
(2, 'static_files/banner/2022/9/partra-jasa.png', 'Patra Jasa', 'Patra Jasa', 'Perusahaan yang bergerak di bisnis Property & Development, <br />  Hotels & Resorts dan Services', 'Companies engaged in the business of Property & Development, <br /> Hotels & Resorts and Services', 'https://facebook.com', 1),
(3, 'static_files/banner/2022/9/rectangle-84-1.png', 'Pertamina', 'Pertamina', 'Perusahaan BUMN yang bertugas mengelola penambangan minyak dan gas bumi di Indonesia', 'BUMN company in charge of managing oil and natural gas mining in Indonesia', 'https://facebook.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `content_tatanilai`
--

CREATE TABLE `content_tatanilai` (
  `id` int(11) NOT NULL,
  `img` varchar(100) NOT NULL,
  `id_title` longtext NOT NULL,
  `id_description` longtext NOT NULL,
  `order` int(10) UNSIGNED NOT NULL CHECK (`order` >= 0),
  `en_description` longtext DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `content_tatanilai`
--

INSERT INTO `content_tatanilai` (`id`, `img`, `id_title`, `id_description`, `order`, `en_description`) VALUES
(1, 'static_files/banner/2022/8/prima-perfoma.png', 'Performance', 'Mengoptimalkan <br /> sumberdaya dan sinergi <br /> untuk menghasilkan kinerja <br /> terbaik', 1, 'en Mengoptimalkan <br /> sumberdaya dan sinergi <br /> untuk menghasilkan kinerja <br /> terbaik'),
(2, 'static_files/banner/2022/8/prima-reliable.png', 'Reliable', 'Memiliki kapabilitas dan <br /> kualitas yang selalu dapat <br /> diandalkan', 2, 'en Memiliki kapabilitas dan <br /> kualitas yang selalu dapat <br /> diandalkan'),
(3, 'static_files/banner/2022/8/prima-integrity.png', 'Integrity', 'Mengedepankan intergritas <br /> dan dedikasi tertinggi <br /> untuk memajukan <br /> perusahaan', 3, 'en Mengedepankan intergritas <br /> dan dedikasi tertinggi <br /> untuk memajukan <br /> perusahaan'),
(4, 'static_files/banner/2022/8/prima-modern.png', 'Modern', 'Selalu bertumbuh dengan <br /> berinovasi menciptakan <br /> nilai tambah', 4, 'en Selalu bertumbuh dengan <br /> berinovasi menciptakan <br /> nilai tambah'),
(5, 'static_files/banner/2022/8/prima-agile.png', 'Agile', 'Gesit beradaptasi dalam <br /> menyikapi setiap <br /> perubahan', 5, 'en Gesit beradaptasi dalam <br /> menyikapi setiap <br /> perubahan');

-- --------------------------------------------------------

--
-- Table structure for table `content_tender`
--

CREATE TABLE `content_tender` (
  `id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `title` varchar(254) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `city` varchar(254) NOT NULL,
  `country` varchar(254) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `content_tender`
--

INSERT INTO `content_tender` (`id`, `is_active`, `title`, `start_date`, `end_date`, `city`, `country`) VALUES
(1, 1, 'Pengadaan peralatan otomotif untuk keperluar perbaikan mobil dan motor untuk provinsi Jawab Barat', '2022-07-20', '2022-11-08', 'Jakarta', 'Indonesia'),
(2, 1, 'peralatan otomotif untuk keperluar perbaikan mobil dan motor untuk provinsi Jawab Barat', '2022-07-20', '2022-11-08', 'Jakarta', 'Indonesia');

-- --------------------------------------------------------

--
-- Table structure for table `content_yearlyreport`
--

CREATE TABLE `content_yearlyreport` (
  `id` int(11) NOT NULL,
  `img` varchar(100) NOT NULL,
  `id_title` varchar(254) NOT NULL,
  `en_title` varchar(254) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `slug` varchar(254) NOT NULL,
  `file` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `content_yearlyreport`
--

INSERT INTO `content_yearlyreport` (`id`, `img`, `id_title`, `en_title`, `is_active`, `slug`, `file`) VALUES
(1, 'static_files/banner/2022/8/laporan-th-1.png', 'Laporan Tahunan 2021', 'en Yearly Report 2021', 1, 'tes', 'static_files/file/2022/9/samplepdf.pdf'),
(2, 'static_files/banner/2022/8/laporan-th-2.png', 'Laporan Tahunan 2020', 'Yearly Report 2020', 1, 'test', 'static_files/file/2022/9/sample-questions-1.csv'),
(3, 'static_files/banner/2022/8/laporan-th-3.png', 'Laporan Tahunan 2019', 'Yearly Report 2019', 1, 'test1', 'static_files/file/2022/9/klien2.png'),
(4, 'static_files/banner/2022/8/laporan-th-4.png', 'Laporan Tahunan 2018', 'Yearly Report 2018', 1, 't', 'static_files/file/2022/9/bg-tender.png');

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext DEFAULT NULL,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL CHECK (`action_flag` >= 0),
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_admin_log`
--

INSERT INTO `django_admin_log` (`id`, `action_time`, `object_id`, `object_repr`, `action_flag`, `change_message`, `content_type_id`, `user_id`) VALUES
(1, '2022-08-16 05:38:32.337783', '1', 'Layanan Penyewaan <br /> dan Jasa Pengemudi Kendaraan <br /> Profesional dan Tersertifikasi', 1, '[{\"added\": {}}]', 13, 1),
(2, '2022-08-23 09:42:01.706651', '1', 'all_img_about_1', 1, '[{\"added\": {}}]', 31, 1),
(3, '2022-08-23 09:43:21.320840', '1', 'all_img_about_2', 2, '[{\"changed\": {\"fields\": [\"Name\"]}}]', 31, 1),
(4, '2022-08-23 09:49:22.085734', '2', 'all_total_kendaraan', 1, '[{\"added\": {}}]', 31, 1),
(5, '2022-08-23 09:49:42.861293', '2', 'all_total_kendaraan', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 31, 1),
(6, '2022-08-23 09:50:44.633445', '3', 'id_about_text', 1, '[{\"added\": {}}]', 31, 1),
(7, '2022-08-23 09:51:02.759535', '3', 'id_about_text', 2, '[{\"changed\": {\"fields\": [\"Img value\"]}}]', 31, 1),
(8, '2022-08-23 09:51:23.090319', '3', 'id_about_description', 2, '[{\"changed\": {\"fields\": [\"Name\"]}}]', 31, 1),
(9, '2022-08-23 09:52:30.823210', '4', 'id_map_title', 1, '[{\"added\": {}}]', 31, 1),
(10, '2022-08-23 09:53:02.457498', '1', 'all_img_about_2', 2, '[{\"changed\": {\"fields\": [\"Img value\"]}}]', 31, 1),
(11, '2022-08-23 09:55:00.164495', '1', 'Layanan Penyewaan <br /> dan Jasa Pengemudi Kendaraan <br /> Profesional dan Tersertifikasi', 2, '[{\"changed\": {\"fields\": [\"En big text\"]}}]', 13, 1),
(12, '2022-08-23 10:17:59.817397', '1', 'judul', 1, '[{\"added\": {}}]', 20, 1),
(13, '2022-08-23 10:19:02.074841', '1', 'judul', 2, '[{\"changed\": {\"fields\": [\"Slug\"]}}]', 20, 1),
(14, '2022-08-24 04:48:58.368178', '1', 'all_img_about_1', 2, '[{\"changed\": {\"fields\": [\"Name\"]}}]', 31, 1),
(15, '2022-08-24 04:50:31.950527', '5', 'id_btn_selengkapnya', 1, '[{\"added\": {}}]', 31, 1),
(16, '2022-08-24 04:51:44.401199', '1', 'id_selengkapnya', 1, '[{\"added\": {}}]', 40, 1),
(17, '2022-08-24 04:52:03.698187', '1', 'id_selengkapnya', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 40, 1),
(18, '2022-08-25 07:11:27.633882', '4', 'id_map_title', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 31, 1),
(19, '2022-08-25 07:13:25.072922', '6', 'id_map_description', 1, '[{\"added\": {}}]', 31, 1),
(20, '2022-08-25 07:13:58.031077', '2', 'all_total_kendaraan', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 31, 1),
(21, '2022-08-25 07:16:59.483629', '7', 'id_map_pengemudi', 1, '[{\"added\": {}}]', 31, 1),
(22, '2022-08-25 07:20:10.096716', '1', 'all_img_about_1', 2, '[{\"changed\": {\"fields\": [\"Text value\", \"Img value\"]}}]', 31, 1),
(23, '2022-08-25 07:22:50.214191', '8', 'all_img_about_2', 1, '[{\"added\": {}}]', 31, 1),
(24, '2022-08-25 07:23:29.154324', '9', 'all_img_about_3', 1, '[{\"added\": {}}]', 31, 1),
(25, '2022-08-25 07:26:34.870066', '2', 'all_total_kendaraan', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 31, 1),
(26, '2022-08-25 07:26:41.482037', '7', 'id_map_pengemudi', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 31, 1),
(27, '2022-08-25 07:41:08.756970', '3', 'id_about_description', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 31, 1),
(28, '2022-08-25 07:41:59.647315', '3', 'id_about_description', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 31, 1),
(29, '2022-08-25 07:43:32.654235', '6', 'id_map_description', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 31, 1),
(30, '2022-08-25 07:44:44.606152', '6', 'id_map_description', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 31, 1),
(31, '2022-08-25 07:45:31.360490', '4', 'id_map_title', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 31, 1),
(32, '2022-08-25 07:47:22.708595', '10', 'id_about_text', 1, '[{\"added\": {}}]', 31, 1),
(33, '2022-08-25 07:49:15.279338', '11', 'id_our_business_title', 1, '[{\"added\": {}}]', 31, 1),
(34, '2022-08-25 07:50:28.903172', '12', 'id_our_business_sub_title', 1, '[{\"added\": {}}]', 31, 1),
(35, '2022-08-25 08:03:04.630701', '1', 'bisnis kami 1', 2, '[{\"changed\": {\"fields\": [\"Id title\", \"Id short desc\"]}}]', 20, 1),
(36, '2022-08-25 08:04:00.303323', '1', 'bisnis kami 1', 2, '[{\"changed\": {\"fields\": [\"Image\", \"Logo\"]}}]', 20, 1),
(37, '2022-08-25 08:04:50.140069', '5', 'id_btn_selengkapnya', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 31, 1),
(38, '2022-08-25 08:06:33.657379', '2', 'bisnis kami 2', 1, '[{\"added\": {}}]', 20, 1),
(39, '2022-08-25 08:07:45.050762', '3', 'bisnis kami 3', 1, '[{\"added\": {}}]', 20, 1),
(40, '2022-08-25 08:14:27.371674', '13', 'id_news_title', 1, '[{\"added\": {}}]', 31, 1),
(41, '2022-08-25 08:15:52.550461', '14', 'id_news_sub_title', 1, '[{\"added\": {}}]', 31, 1),
(42, '2022-08-25 08:16:46.105193', '15', 'id_client_title', 1, '[{\"added\": {}}]', 31, 1),
(43, '2022-08-25 08:18:28.321487', '16', 'id_client_sub_title', 1, '[{\"added\": {}}]', 31, 1),
(44, '2022-08-25 08:32:32.600740', '1', 'pertamina 1', 1, '[{\"added\": {}}]', 15, 1),
(45, '2022-08-25 08:32:51.911531', '2', 'pertamina2', 1, '[{\"added\": {}}]', 15, 1),
(46, '2022-08-25 08:33:12.168830', '3', 'pertamina3', 1, '[{\"added\": {}}]', 15, 1),
(47, '2022-08-25 08:33:23.415147', '4', 'pertamina 4', 1, '[{\"added\": {}}]', 15, 1),
(48, '2022-08-25 08:33:29.178126', '3', 'pertamina 3', 2, '[{\"changed\": {\"fields\": [\"Title\"]}}]', 15, 1),
(49, '2022-08-25 08:33:34.143583', '2', 'pertamina 2', 2, '[{\"changed\": {\"fields\": [\"Title\"]}}]', 15, 1),
(50, '2022-08-25 08:34:41.990487', '5', 'klien 1', 1, '[{\"added\": {}}]', 15, 1),
(51, '2022-08-25 08:34:54.492332', '6', 'klien 2', 1, '[{\"added\": {}}]', 15, 1),
(52, '2022-08-25 08:35:05.249324', '7', 'klien 3', 1, '[{\"added\": {}}]', 15, 1),
(53, '2022-08-25 08:35:16.544638', '8', 'klien 4', 1, '[{\"added\": {}}]', 15, 1),
(54, '2022-08-25 08:40:08.388261', '1', 'id_about_us_title', 1, '[{\"added\": {}}]', 30, 1),
(55, '2022-08-25 08:40:36.609567', '2', 'all_banner_image', 1, '[{\"added\": {}}]', 30, 1),
(56, '2022-08-25 08:41:12.668484', '3', 'id_about_us_short_description', 1, '[{\"added\": {}}]', 30, 1),
(57, '2022-08-25 08:43:06.511582', '4', 'id_about_us_long_description', 1, '[{\"added\": {}}]', 30, 1),
(58, '2022-08-25 08:43:49.067281', '4', 'id_about_us_long_description', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 30, 1),
(59, '2022-08-25 08:56:21.725647', '5', 'id_jejak_langkah_title', 1, '[{\"added\": {}}]', 30, 1),
(60, '2022-08-25 08:58:20.585345', '6', 'id_jejak_langkah_sub_title', 1, '[{\"added\": {}}]', 30, 1),
(61, '2022-08-25 09:00:30.348301', '7', 'all_visi_misi_image', 1, '[{\"added\": {}}]', 30, 1),
(62, '2022-08-25 09:01:04.054686', '8', 'id_visi_misi_title', 1, '[{\"added\": {}}]', 30, 1),
(63, '2022-08-25 09:01:26.248750', '9', 'id_visi_misi_sub_title', 1, '[{\"added\": {}}]', 30, 1),
(64, '2022-08-25 09:02:18.080799', '10', 'id_visi_title', 1, '[{\"added\": {}}]', 30, 1),
(65, '2022-08-25 09:03:26.227873', '11', 'id_visi_text', 1, '[{\"added\": {}}]', 30, 1),
(66, '2022-08-25 09:04:05.354910', '12', 'id_misi_title', 1, '[{\"added\": {}}]', 30, 1),
(67, '2022-08-25 09:05:10.521150', '13', 'id_misi_text', 1, '[{\"added\": {}}]', 30, 1),
(68, '2022-08-25 09:06:37.996978', '14', 'id_tata_nilai_title', 1, '[{\"added\": {}}]', 30, 1),
(69, '2022-08-25 09:07:11.068716', '15', 'id_tata_nilai_sub_title', 1, '[{\"added\": {}}]', 30, 1),
(70, '2022-08-25 09:09:16.502551', '16', 'id_award_title', 1, '[{\"added\": {}}]', 30, 1),
(71, '2022-08-25 09:13:48.437557', '17', 'id_award_sub_title', 1, '[{\"added\": {}}]', 30, 1),
(72, '2022-08-25 09:17:27.609434', '1', 'Fleet Appreciation\r\nToyota Astra Motor', 1, '[{\"added\": {}}]', 12, 1),
(73, '2022-08-25 09:19:33.583015', '2', 'Fleet Appreciation\r\nAgung Auto Toyota', 1, '[{\"added\": {}}]', 12, 1),
(74, '2022-08-25 09:20:03.134715', '3', 'Penghargaan Nusantara Berlian Motor', 1, '[{\"added\": {}}]', 12, 1),
(75, '2022-08-25 09:21:48.800864', '18', 'id_certificate_title', 1, '[{\"added\": {}}]', 30, 1),
(76, '2022-08-25 09:21:58.865967', '19', 'id_certificate_sub_title', 1, '[{\"added\": {}}]', 30, 1),
(77, '2022-08-25 09:24:12.172028', '20', 'id_yearly_report_title', 1, '[{\"added\": {}}]', 30, 1),
(78, '2022-08-25 09:24:24.223380', '21', 'en_yearly_report_sub_title', 1, '[{\"added\": {}}]', 30, 1),
(79, '2022-08-26 02:54:34.705268', '1', 'id_subscribe_description', 1, '[{\"added\": {}}]', 39, 1),
(80, '2022-08-26 02:56:18.997152', '1', 'id_subscribe_description', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 39, 1),
(81, '2022-08-26 02:56:49.635584', '2', 'id_email_placeholder', 1, '[{\"added\": {}}]', 39, 1),
(82, '2022-08-26 02:58:37.463844', '3', 'id_langganan_berita', 1, '[{\"added\": {}}]', 39, 1),
(83, '2022-08-26 03:02:43.505327', '4', 'id_info', 1, '[{\"added\": {}}]', 39, 1),
(84, '2022-08-26 03:06:42.818615', '5', 'id_quick_links', 1, '[{\"added\": {}}]', 39, 1),
(85, '2022-08-26 03:07:15.189939', '6', 'id_tentang_kami', 1, '[{\"added\": {}}]', 39, 1),
(86, '2022-08-26 03:07:34.464261', '7', 'id_bisnis_kami', 1, '[{\"added\": {}}]', 39, 1),
(87, '2022-08-26 03:08:37.767352', '8', 'id_ada_pertanyaan', 1, '[{\"added\": {}}]', 39, 1),
(88, '2022-08-26 03:08:55.527480', '9', 'id_ada_pertanyaan_desc', 1, '[{\"added\": {}}]', 39, 1),
(89, '2022-08-26 03:09:50.525320', '10', 'id_beranda', 1, '[{\"added\": {}}]', 39, 1),
(90, '2022-08-26 03:11:18.732907', '11', 'id_berita', 1, '[{\"added\": {}}]', 39, 1),
(91, '2022-08-26 03:11:37.776247', '12', 'id_karir', 1, '[{\"added\": {}}]', 39, 1),
(92, '2022-08-26 03:11:49.818157', '13', 'id_tender', 1, '[{\"added\": {}}]', 39, 1),
(93, '2022-08-26 03:12:50.388042', '9', 'id_ada_pertanyaan_desc', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 39, 1),
(94, '2022-08-26 04:39:14.466146', '1', 'Certicate object (1)', 1, '[{\"added\": {}}]', 14, 1),
(95, '2022-08-26 04:44:38.972959', '2', 'Certicate object (2)', 1, '[{\"added\": {}}]', 14, 1),
(96, '2022-08-26 04:46:59.126386', '3', 'Certicate object (3)', 1, '[{\"added\": {}}]', 14, 1),
(97, '2022-08-26 04:48:19.811311', '1', 'Certicate object (1)', 2, '[{\"changed\": {\"fields\": [\"Id description\", \"En description\"]}}]', 14, 1),
(98, '2022-08-26 04:48:48.422326', '1', 'Certicate object (1)', 2, '[{\"changed\": {\"fields\": [\"Id description\", \"En description\"]}}]', 14, 1),
(99, '2022-08-26 04:49:06.580683', '1', 'Certicate object (1)', 2, '[{\"changed\": {\"fields\": [\"Id description\"]}}]', 14, 1),
(100, '2022-08-26 04:51:13.767220', '1', 'History object (1)', 1, '[{\"added\": {}}]', 18, 1),
(101, '2022-08-26 04:52:54.793834', '2', 'History object (2)', 1, '[{\"added\": {}}]', 18, 1),
(102, '2022-08-26 07:26:51.063890', '1', 'TataNilai object (1)', 1, '[{\"added\": {}}]', 22, 1),
(103, '2022-08-26 07:28:02.659824', '2', 'TataNilai object (2)', 1, '[{\"added\": {}}]', 22, 1),
(104, '2022-08-26 07:30:09.336640', '3', 'TataNilai object (3)', 1, '[{\"added\": {}}]', 22, 1),
(105, '2022-08-26 07:35:13.006886', '4', 'TataNilai object (4)', 1, '[{\"added\": {}}]', 22, 1),
(106, '2022-08-26 07:36:22.828880', '5', 'TataNilai object (5)', 1, '[{\"added\": {}}]', 22, 1),
(107, '2022-08-26 07:49:49.233801', '1', 'Laporan Tahunan 2021', 1, '[{\"added\": {}}]', 24, 1),
(108, '2022-08-26 07:50:46.658918', '22', 'id_yearly_report_sub_title', 1, '[{\"added\": {}}]', 30, 1),
(109, '2022-08-26 07:53:14.161531', '2', 'Laporan Tahunan 2020', 1, '[{\"added\": {}}]', 24, 1),
(110, '2022-08-26 07:54:18.082671', '3', 'Laporan Tahunan 2019', 1, '[{\"added\": {}}]', 24, 1),
(111, '2022-08-26 07:54:47.496517', '4', 'Laporan Tahunan 2018', 1, '[{\"added\": {}}]', 24, 1),
(112, '2022-08-26 08:05:36.367456', '1', 'bisnis kami 1', 2, '[{\"changed\": {\"fields\": [\"Id long desc\", \"En short desc\", \"En long desc\"]}}]', 20, 1),
(113, '2022-08-26 08:07:39.769534', '1', 'all_banner_image', 1, '[{\"added\": {}}]', 32, 1),
(114, '2022-08-26 08:07:56.897840', '2', 'id_title', 1, '[{\"added\": {}}]', 32, 1),
(115, '2022-08-26 08:08:23.558964', '3', 'id_banner_text', 1, '[{\"added\": {}}]', 32, 1),
(116, '2022-08-26 08:08:43.051263', '1', 'all_banner_image', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 32, 1),
(117, '2022-08-26 08:09:12.552051', '1', 'all_banner_image', 2, '[{\"changed\": {\"fields\": [\"Img value\"]}}]', 32, 1),
(118, '2022-08-26 08:09:40.542706', '1', 'Prima Fleet Services', 2, '[{\"changed\": {\"fields\": [\"Id title\", \"En title\", \"En short desc\"]}}]', 20, 1),
(119, '2022-08-26 08:12:49.374899', '2', 'Prima Driver Management', 2, '[{\"changed\": {\"fields\": [\"Image\", \"Logo\", \"Id title\", \"Id long desc\", \"En title\", \"En short desc\", \"En long desc\"]}}]', 20, 1),
(120, '2022-08-26 08:14:14.906992', '1', 'Prima Fleet Services', 2, '[{\"changed\": {\"fields\": [\"Id long desc\"]}}]', 20, 1),
(121, '2022-08-26 08:16:36.094253', '3', 'Prima Auto Care & Sales', 2, '[{\"changed\": {\"fields\": [\"Id title\", \"Id long desc\", \"En title\", \"En short desc\", \"En long desc\"]}}]', 20, 1),
(122, '2022-08-26 08:22:26.494142', '3', 'Prima Auto Care & Sales', 2, '[{\"changed\": {\"fields\": [\"Image\", \"Logo\", \"Id long desc\", \"En long desc\"]}}]', 20, 1),
(123, '2022-08-26 08:27:14.525067', '2', 'Prima Driver Management', 2, '[{\"changed\": {\"fields\": [\"Logo\", \"Id long desc\", \"En long desc\"]}}]', 20, 1),
(124, '2022-08-26 08:29:12.479463', '3', 'id_banner_text', 2, '[{\"changed\": {\"fields\": [\"Img value\"]}}]', 32, 1),
(125, '2022-08-26 08:29:23.675455', '1', 'all_banner_image', 2, '[{\"changed\": {\"fields\": [\"Img value\"]}}]', 32, 1),
(126, '2022-08-26 08:29:54.287260', '4', 'en_title', 1, '[{\"added\": {}}]', 32, 1),
(127, '2022-08-26 08:30:37.270828', '5', 'en_banner_text', 1, '[{\"added\": {}}]', 32, 1),
(128, '2022-08-26 08:31:25.397786', '1', 'all_banner_image', 2, '[{\"changed\": {\"fields\": [\"Text value\", \"Img value\"]}}]', 32, 1),
(129, '2022-08-26 08:33:24.821096', '1', 'all_banner_image', 1, '[{\"added\": {}}]', 35, 1),
(130, '2022-08-26 08:33:42.263678', '2', 'id_title', 1, '[{\"added\": {}}]', 35, 1),
(131, '2022-08-26 08:34:10.982913', '3', 'id_banner_text', 1, '[{\"added\": {}}]', 35, 1),
(132, '2022-08-26 08:39:56.416548', '1', 'all_banner_image', 1, '[{\"added\": {}}]', 38, 1),
(133, '2022-08-26 08:40:33.588120', '2', 'id_title', 1, '[{\"added\": {}}]', 38, 1),
(134, '2022-08-26 08:40:44.792488', '2', 'id_title', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 38, 1),
(135, '2022-08-26 08:41:05.096158', '3', 'id_banner_text', 1, '[{\"added\": {}}]', 38, 1),
(136, '2022-08-26 08:41:26.974239', '4', 'id_banner_sub_text', 1, '[{\"added\": {}}]', 38, 1),
(137, '2022-08-26 08:41:55.857613', '5', 'id_album_title', 1, '[{\"added\": {}}]', 38, 1),
(138, '2022-08-26 08:43:58.062347', '1', 'Acara Kebersamaan', 1, '[{\"added\": {}}]', 11, 1),
(139, '2022-08-26 08:49:11.455453', '2', 'Lomba Kemerdekaan', 1, '[{\"added\": {}}]', 11, 1),
(140, '2022-08-26 08:49:35.098475', '3', 'Acara Ulang Tahun', 1, '[{\"added\": {}}]', 11, 1),
(141, '2022-08-26 08:50:00.948195', '4', 'Pelatihan Anggota', 1, '[{\"added\": {}}]', 11, 1),
(142, '2022-08-26 08:50:23.263612', '5', 'Pelantikan Pimpinan Baru', 1, '[{\"added\": {}}]', 11, 1),
(143, '2022-08-26 08:51:06.776975', '6', 'Peresmian Kantor Surabaya', 1, '[{\"added\": {}}]', 11, 1),
(144, '2022-08-26 08:53:44.143452', '1', 'all_banner_image', 1, '[{\"added\": {}}]', 33, 1),
(145, '2022-08-26 08:54:21.200435', '2', 'id_banner_text', 1, '[{\"added\": {}}]', 33, 1),
(146, '2022-08-26 08:54:30.046396', '3', 'id_title', 1, '[{\"added\": {}}]', 33, 1),
(147, '2022-08-26 08:54:50.245019', '4', 'id_banner_sub_text', 1, '[{\"added\": {}}]', 33, 1),
(148, '2022-09-01 02:58:31.379605', '8', 'all_img_about_2', 2, '[{\"changed\": {\"fields\": [\"Img value\"]}}]', 31, 1),
(149, '2022-09-01 02:58:40.150923', '9', 'all_img_about_3', 2, '[{\"changed\": {\"fields\": [\"Img value\"]}}]', 31, 1),
(150, '2022-09-01 03:09:07.655222', '7', 'id_map_pengemudi', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 31, 1),
(151, '2022-09-01 03:10:33.573474', '17', 'all_total_pengemudi', 1, '[{\"added\": {}}]', 31, 1),
(152, '2022-09-01 03:11:15.731660', '18', 'id_map_kendaraan', 1, '[{\"added\": {}}]', 31, 1),
(153, '2022-09-01 03:11:38.285868', '7', 'id_map_pengemudi', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 31, 1),
(154, '2022-09-01 03:23:54.939244', '1', 'History object (1)', 2, '[{\"changed\": {\"fields\": [\"Id long desc\", \"En long desc\"]}}]', 18, 1),
(155, '2022-09-01 03:24:25.652901', '2', 'History object (2)', 2, '[{\"changed\": {\"fields\": [\"Id long desc\", \"En long desc\", \"Year\"]}}]', 18, 1),
(156, '2022-09-01 03:27:14.149275', '1', 'History object (1)', 2, '[{\"changed\": {\"fields\": [\"Id long desc\", \"En long desc\"]}}]', 18, 1),
(157, '2022-09-01 03:28:12.221214', '2', 'History object (2)', 2, '[{\"changed\": {\"fields\": [\"Id long desc\", \"En long desc\", \"Year\"]}}]', 18, 1),
(158, '2022-09-01 03:52:21.476678', '23', 'id_profile_direksi_title', 1, '[{\"added\": {}}]', 30, 1),
(159, '2022-09-01 04:02:04.875597', '2', 'id_title', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 32, 1),
(160, '2022-09-01 04:13:31.361353', '1', 'all_banner_image', 2, '[{\"changed\": {\"fields\": [\"Img value\"]}}]', 33, 1),
(161, '2022-09-01 04:16:02.872882', '1', 'all_banner_image', 1, '[{\"added\": {}}]', 41, 1),
(162, '2022-09-01 04:18:03.715113', '2', 'id_lamar_pekerjaan', 1, '[{\"added\": {}}]', 41, 1),
(163, '2022-09-01 04:18:55.622948', '3', 'id_pekerjaan_serupa', 1, '[{\"added\": {}}]', 41, 1),
(164, '2022-09-01 04:27:27.521306', '1', 'Bisnis', 1, '[{\"added\": {}}]', 28, 1),
(165, '2022-09-01 04:27:56.344663', '2', 'Teknisi', 1, '[{\"added\": {}}]', 28, 1),
(166, '2022-09-01 04:55:18.477884', '1', 'Pengadaan peralatan otomotif untuk keperluar perbaikan mobil dan motor untuk provinsi Jawab Barat', 1, '[{\"added\": {}}]', 23, 1),
(167, '2022-09-01 04:56:32.115404', '1', 'all_banner_image', 1, '[{\"added\": {}}]', 36, 1),
(168, '2022-09-01 04:57:02.618142', '2', 'id_title', 1, '[{\"added\": {}}]', 36, 1),
(169, '2022-09-01 04:57:11.302861', '3', 'id_banner_text', 1, '[{\"added\": {}}]', 36, 1),
(170, '2022-09-01 04:57:26.305707', '2', 'id_title', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 36, 1),
(171, '2022-09-01 04:58:44.915852', '4', 'id_pengumuman_text', 1, '[{\"added\": {}}]', 36, 1),
(172, '2022-09-01 04:59:00.612122', '5', 'id_penutupan_text', 1, '[{\"added\": {}}]', 36, 1),
(173, '2022-09-01 05:00:21.524783', '2', 'Pengadaan peralatan otomotif untuk keperluar perbaikan mobil dan motor untuk provinsi Jawab Barat', 1, '[{\"added\": {}}]', 23, 1),
(174, '2022-09-01 05:03:56.968450', '1', '1', 1, '[{\"added\": {}}]', 19, 1),
(175, '2022-09-01 06:43:30.085305', '1', 'id_selengkapnya', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 40, 1),
(176, '2022-09-01 06:45:27.765205', '2', 'id_beranda', 1, '[{\"added\": {}}]', 40, 1),
(177, '2022-09-01 06:48:56.664528', '3', 'id_lihat_laporan', 1, '[{\"added\": {}}]', 40, 1),
(178, '2022-09-01 06:51:42.642477', '4', 'id_kunjungi_website', 1, '[{\"added\": {}}]', 40, 1),
(179, '2022-09-01 06:58:06.389059', '1', 'Certicate object (1)', 2, '[{\"changed\": {\"fields\": [\"Id description\"]}}]', 14, 1),
(180, '2022-09-01 06:58:37.297877', '1', 'Certicate object (1)', 2, '[{\"changed\": {\"fields\": [\"Id description\"]}}]', 14, 1),
(181, '2022-09-01 07:51:48.786662', '11', 'id_berita', 2, '[]', 39, 1),
(182, '2022-09-01 07:59:33.118086', '1', 'all_email_address', 1, '[{\"added\": {}}]', 34, 1),
(183, '2022-09-01 08:00:15.135346', '2', 'all_phone_number', 1, '[{\"added\": {}}]', 34, 1),
(184, '2022-09-01 08:01:05.445129', '3', 'all_address', 1, '[{\"added\": {}}]', 34, 1),
(185, '2022-09-01 08:01:24.011082', '4', 'id_title', 1, '[{\"added\": {}}]', 34, 1),
(186, '2022-09-01 08:01:47.542691', '5', 'id_sub_title', 1, '[{\"added\": {}}]', 34, 1),
(187, '2022-09-01 08:02:13.888322', '6', 'id_telephone_title', 1, '[{\"added\": {}}]', 34, 1),
(188, '2022-09-01 08:02:27.668137', '7', 'id_email_title', 1, '[{\"added\": {}}]', 34, 1),
(189, '2022-09-01 08:02:40.140576', '8', 'id_address_title', 1, '[{\"added\": {}}]', 34, 1),
(190, '2022-09-01 08:03:03.865743', '9', 'id_telphone_subtext', 1, '[{\"added\": {}}]', 34, 1),
(191, '2022-09-01 08:03:16.217412', '10', 'id_email_sub_text', 1, '[{\"added\": {}}]', 34, 1),
(192, '2022-09-01 08:03:32.252329', '11', 'id_address_sub_text', 1, '[{\"added\": {}}]', 34, 1),
(193, '2022-09-01 08:04:25.899387', '12', 'id_contact_form_title', 1, '[{\"added\": {}}]', 34, 1),
(194, '2022-09-01 08:04:39.556440', '13', 'id_contact_form_sub_title', 1, '[{\"added\": {}}]', 34, 1),
(195, '2022-09-01 08:05:22.787506', '14', 'id_nama_depan_label', 1, '[{\"added\": {}}]', 34, 1),
(196, '2022-09-01 08:05:35.238393', '15', 'id_nama_belakang_label', 1, '[{\"added\": {}}]', 34, 1),
(197, '2022-09-01 08:05:46.395771', '16', 'id_email_label', 1, '[{\"added\": {}}]', 34, 1),
(198, '2022-09-01 08:06:24.911131', '17', 'id_phone_label', 1, '[{\"added\": {}}]', 34, 1),
(199, '2022-09-01 08:06:37.352112', '18', 'id_message_label', 1, '[{\"added\": {}}]', 34, 1),
(200, '2022-09-01 08:06:55.635120', '19', 'id_button_text', 1, '[{\"added\": {}}]', 34, 1),
(201, '2022-09-01 08:10:45.646890', '1', 'all_banner_image', 1, '[{\"added\": {}}]', 37, 1),
(202, '2022-09-01 08:12:04.412654', '2', 'all_komitmen_image', 1, '[{\"added\": {}}]', 37, 1),
(203, '2022-09-01 08:12:23.994331', '3', 'id_title', 1, '[{\"added\": {}}]', 37, 1),
(204, '2022-09-01 08:12:50.579370', '4', 'id_banner_text', 1, '[{\"added\": {}}]', 37, 1),
(205, '2022-09-01 08:13:22.070531', '5', 'id_komitmen_text', 1, '[{\"added\": {}}]', 37, 1),
(206, '2022-09-01 08:14:33.832056', '6', 'id_komitmen_description', 1, '[{\"added\": {}}]', 37, 1),
(207, '2022-09-01 08:15:20.637145', '7', 'en_yearly_report_title', 1, '[{\"added\": {}}]', 37, 1),
(208, '2022-09-01 08:15:50.030006', '8', 'id_yearly_report_sub_title', 1, '[{\"added\": {}}]', 37, 1),
(209, '2022-09-01 08:16:05.454663', '9', 'id_yearly_report_title', 1, '[{\"added\": {}}]', 37, 1),
(210, '2022-09-01 08:20:22.677217', '2', 'id_title', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 32, 1),
(211, '2022-09-01 08:26:42.736426', '1', 'all_banner_image', 1, '[{\"added\": {}}]', 42, 1),
(212, '2022-09-01 08:27:00.047435', '2', 'id_banner_text', 1, '[{\"added\": {}}]', 42, 1),
(213, '2022-09-01 08:27:33.648060', '3', 'id_banner_sub_text', 1, '[{\"added\": {}}]', 42, 1),
(214, '2022-09-01 08:28:34.052801', '4', 'id_album_title', 1, '[{\"added\": {}}]', 42, 1),
(215, '2022-09-04 01:08:30.678378', '2', 'id_beranda', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 40, 1),
(216, '2022-09-04 01:08:40.726110', '1', 'id_selengkapnya', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 40, 1),
(217, '2022-09-04 01:09:10.704822', '5', 'id_hubungi_kami', 1, '[{\"added\": {}}]', 40, 1),
(218, '2022-09-04 01:09:21.066493', '6', 'id_media_informasi', 1, '[{\"added\": {}}]', 40, 1),
(219, '2022-09-04 01:09:28.694588', '7', 'id_karir', 1, '[{\"added\": {}}]', 40, 1),
(220, '2022-09-04 01:09:35.481669', '8', 'id_gallery', 1, '[{\"added\": {}}]', 40, 1),
(221, '2022-09-04 01:09:43.644591', '9', 'id_tentang_kami', 1, '[{\"added\": {}}]', 40, 1),
(222, '2022-09-04 01:09:56.658643', '10', 'id_hubungan_investor', 1, '[{\"added\": {}}]', 40, 1),
(223, '2022-09-04 01:10:05.528420', '11', 'id_artikel_dan_berita', 1, '[{\"added\": {}}]', 40, 1),
(224, '2022-09-04 01:10:12.778614', '12', 'id_artikel', 1, '[{\"added\": {}}]', 40, 1),
(225, '2022-09-04 01:10:19.149801', '13', 'id_berita', 1, '[{\"added\": {}}]', 40, 1),
(226, '2022-09-04 01:10:27.897304', '14', 'id_tender', 1, '[{\"added\": {}}]', 40, 1),
(227, '2022-09-04 01:10:36.239317', '15', 'id_menampilkan', 1, '[{\"added\": {}}]', 40, 1),
(228, '2022-09-04 01:10:45.301557', '16', 'id_tjsl', 1, '[{\"added\": {}}]', 40, 1),
(229, '2022-09-04 01:10:54.118795', '17', 'en_beranda', 1, '[{\"added\": {}}]', 40, 1),
(230, '2022-09-04 01:11:04.879548', '18', 'en_bisnis_kami', 1, '[{\"added\": {}}]', 40, 1),
(231, '2022-09-04 01:11:14.949663', '19', 'en_selengkapnya', 1, '[{\"added\": {}}]', 40, 1),
(232, '2022-09-04 01:11:25.796868', '20', 'en_lihat_laporan', 1, '[{\"added\": {}}]', 40, 1),
(233, '2022-09-04 01:11:35.788412', '21', 'en_kunjungi_website', 1, '[{\"added\": {}}]', 40, 1),
(234, '2022-09-04 01:11:52.796222', '22', 'en_media_informasi', 1, '[{\"added\": {}}]', 40, 1),
(235, '2022-09-04 01:12:00.542918', '23', 'en_hubungi_kami', 1, '[{\"added\": {}}]', 40, 1),
(236, '2022-09-04 01:12:13.613925', '24', 'en_karir', 1, '[{\"added\": {}}]', 40, 1),
(237, '2022-09-04 01:12:20.785480', '25', 'en_gallery', 1, '[{\"added\": {}}]', 40, 1),
(238, '2022-09-04 01:12:28.724166', '26', 'en_tentang_kami', 1, '[{\"added\": {}}]', 40, 1),
(239, '2022-09-04 01:12:41.675986', '27', 'en_investor', 1, '[{\"added\": {}}]', 40, 1),
(240, '2022-09-04 01:12:50.965431', '28', 'en_profil_direksi', 1, '[{\"added\": {}}]', 40, 1),
(241, '2022-09-04 01:13:01.758662', '29', 'id_profil_direksi', 1, '[{\"added\": {}}]', 40, 1),
(242, '2022-09-04 01:13:42.399202', '30', 'en_hubungan_investor', 1, '[{\"added\": {}}]', 40, 1),
(243, '2022-09-04 01:13:51.675971', '31', 'en_artikel_dan_berita', 1, '[{\"added\": {}}]', 40, 1),
(244, '2022-09-04 01:13:59.977634', '32', 'en_artikel', 1, '[{\"added\": {}}]', 40, 1),
(245, '2022-09-04 01:14:11.087639', '33', 'en_berita', 1, '[{\"added\": {}}]', 40, 1),
(246, '2022-09-04 01:14:19.315816', '34', 'en_tender', 1, '[{\"added\": {}}]', 40, 1),
(247, '2022-09-04 01:14:27.585466', '35', 'en_menampilkan', 1, '[{\"added\": {}}]', 40, 1),
(248, '2022-09-04 01:14:33.379591', '36', 'en_tjsl', 1, '[{\"added\": {}}]', 40, 1),
(249, '2022-09-04 02:05:28.933273', '14', 'id_news', 1, '[{\"added\": {}}]', 39, 1),
(250, '2022-09-04 02:08:49.700557', '15', 'id_sitemap', 1, '[{\"added\": {}}]', 39, 1),
(251, '2022-09-04 02:09:03.349837', '16', 'id_kebijakan_privasi', 1, '[{\"added\": {}}]', 39, 1),
(252, '2022-09-04 02:09:17.029369', '17', 'id_profile_perusahaan', 1, '[{\"added\": {}}]', 39, 1),
(253, '2022-09-04 02:09:27.049443', '18', 'id_manajement', 1, '[{\"added\": {}}]', 39, 1),
(254, '2022-09-04 02:09:35.032550', '19', 'id_penghargaan', 1, '[{\"added\": {}}]', 39, 1),
(255, '2022-09-04 02:09:42.584845', '20', 'id_kontak', 1, '[{\"added\": {}}]', 39, 1),
(256, '2022-09-04 02:09:56.516129', '21', 'id_patra_jasa', 1, '[{\"added\": {}}]', 39, 1),
(257, '2022-09-04 02:18:31.069238', '22', 'en_ada_pertanyaan', 1, '[{\"added\": {}}]', 39, 1),
(258, '2022-09-04 02:19:23.965130', '23', 'en_ada_pertanyaan_desc', 1, '[{\"added\": {}}]', 39, 1),
(259, '2022-09-04 02:19:33.617315', '24', 'en_bisnis_kami', 1, '[{\"added\": {}}]', 39, 1),
(260, '2022-09-04 02:19:41.301561', '25', 'en_tentang_kami', 1, '[{\"added\": {}}]', 39, 1),
(261, '2022-09-04 02:19:52.188927', '26', 'en_quick_links', 1, '[{\"added\": {}}]', 39, 1),
(262, '2022-09-04 02:20:00.257337', '27', 'en_info', 1, '[{\"added\": {}}]', 39, 1),
(263, '2022-09-04 02:20:21.871676', '28', 'en_langganan_berita', 1, '[{\"added\": {}}]', 39, 1),
(264, '2022-09-04 02:20:36.537263', '29', 'en_email_placeholder', 1, '[{\"added\": {}}]', 39, 1),
(265, '2022-09-04 02:21:16.918325', '30', 'en_subscribe_description', 1, '[{\"added\": {}}]', 39, 1),
(266, '2022-09-04 02:21:25.209267', '31', 'en_sitemap', 1, '[{\"added\": {}}]', 39, 1),
(267, '2022-09-04 02:21:44.229098', '32', 'en_kebijakan_privasi', 1, '[{\"added\": {}}]', 39, 1),
(268, '2022-09-04 02:22:00.805123', '33', 'en_profile_perusahaan', 1, '[{\"added\": {}}]', 39, 1),
(269, '2022-09-04 02:22:08.809868', '34', 'en_manajement', 1, '[{\"added\": {}}]', 39, 1),
(270, '2022-09-04 02:22:15.997271', '35', 'en_penghargaan', 1, '[{\"added\": {}}]', 39, 1),
(271, '2022-09-04 02:22:22.050182', '36', 'en_kontak', 1, '[{\"added\": {}}]', 39, 1),
(272, '2022-09-04 02:22:28.576934', '37', 'en_beranda', 1, '[{\"added\": {}}]', 39, 1),
(273, '2022-09-04 02:22:34.906945', '38', 'en_patra_jasa', 1, '[{\"added\": {}}]', 39, 1),
(274, '2022-09-04 02:22:41.678469', '39', 'en_berita', 1, '[{\"added\": {}}]', 39, 1),
(275, '2022-09-04 02:22:52.479051', '40', 'en_karir', 1, '[{\"added\": {}}]', 39, 1),
(276, '2022-09-04 02:22:58.976742', '41', 'en_tender', 1, '[{\"added\": {}}]', 39, 1),
(277, '2022-09-04 02:23:05.525009', '42', 'en_news', 1, '[{\"added\": {}}]', 39, 1),
(278, '2022-09-04 02:50:51.412913', '1', 'all_img_about_1', 2, '[{\"changed\": {\"fields\": [\"Img value\"]}}]', 31, 1),
(279, '2022-09-04 02:51:17.606022', '8', 'all_img_about_2', 3, '', 31, 1),
(280, '2022-09-04 02:51:17.606568', '9', 'all_img_about_3', 3, '', 31, 1),
(281, '2022-09-04 02:54:46.918011', '19', 'id_about_title', 1, '[{\"added\": {}}]', 31, 1),
(282, '2022-09-04 02:55:17.715589', '20', 'en_about_title', 1, '[{\"added\": {}}]', 31, 1),
(283, '2022-09-04 02:55:46.436298', '21', 'en_about_text', 1, '[{\"added\": {}}]', 31, 1),
(284, '2022-09-04 02:56:10.030315', '22', 'en_about_description', 1, '[{\"added\": {}}]', 31, 1),
(285, '2022-09-04 02:56:28.258322', '21', 'en_about_text', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 31, 1),
(286, '2022-09-04 02:57:22.737484', '23', 'en_map_title', 1, '[{\"added\": {}}]', 31, 1),
(287, '2022-09-04 02:57:56.095185', '24', 'en_map_description', 1, '[{\"added\": {}}]', 31, 1),
(288, '2022-09-04 02:58:36.261284', '25', 'en_map_kendaraan', 1, '[{\"added\": {}}]', 31, 1),
(289, '2022-09-04 02:58:43.554228', '26', 'en_map_pengemudi', 1, '[{\"added\": {}}]', 31, 1),
(290, '2022-09-04 02:58:54.405951', '27', 'en_our_business_title', 1, '[{\"added\": {}}]', 31, 1),
(291, '2022-09-04 02:59:28.733065', '28', 'en_our_business_sub_title', 1, '[{\"added\": {}}]', 31, 1),
(292, '2022-09-04 02:59:37.697310', '29', 'en_news_title', 1, '[{\"added\": {}}]', 31, 1),
(293, '2022-09-04 03:00:24.009105', '30', 'en_news_sub_title', 1, '[{\"added\": {}}]', 31, 1),
(294, '2022-09-04 03:00:40.470849', '31', 'en_client_title', 1, '[{\"added\": {}}]', 31, 1),
(295, '2022-09-04 03:01:07.538174', '32', 'en_client_sub_title', 1, '[{\"added\": {}}]', 31, 1),
(296, '2022-09-04 03:01:22.480342', '33', 'en_btn_selengkapnya', 1, '[{\"added\": {}}]', 31, 1),
(297, '2022-09-04 03:18:55.153167', '24', 'id_profile_komisaris_title', 1, '[{\"added\": {}}]', 30, 1),
(298, '2022-09-04 03:19:03.351317', '24', 'id_profile_komisaris_title', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 30, 1),
(299, '2022-09-04 03:23:04.095354', '21', 'en_yearly_report_sub_title', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 30, 1),
(300, '2022-09-04 03:23:12.726503', '25', 'en_about_us_title', 1, '[{\"added\": {}}]', 30, 1),
(301, '2022-09-04 03:23:30.158466', '26', 'en_about_us_short_description', 1, '[{\"added\": {}}]', 30, 1),
(302, '2022-09-04 03:23:51.707498', '27', 'en_about_us_long_description', 1, '[{\"added\": {}}]', 30, 1),
(303, '2022-09-04 03:24:20.675960', '28', 'en_jejak_langkah_title', 1, '[{\"added\": {}}]', 30, 1),
(304, '2022-09-04 03:24:38.669179', '29', 'en_jejak_langkah_sub_title', 1, '[{\"added\": {}}]', 30, 1),
(305, '2022-09-04 03:24:56.425677', '30', 'en_visi_misi_title', 1, '[{\"added\": {}}]', 30, 1),
(306, '2022-09-04 03:25:04.118155', '31', 'en_misi_title', 1, '[{\"added\": {}}]', 30, 1),
(307, '2022-09-04 03:25:10.337021', '32', 'en_visi_title', 1, '[{\"added\": {}}]', 30, 1),
(308, '2022-09-04 03:25:43.810482', '33', 'en_visi_text', 1, '[{\"added\": {}}]', 30, 1),
(309, '2022-09-04 03:26:48.029966', '34', 'en_tata_nilai_title', 1, '[{\"added\": {}}]', 30, 1),
(310, '2022-09-04 03:27:13.422210', '35', 'en_tata_nilai_sub_title', 1, '[{\"added\": {}}]', 30, 1),
(311, '2022-09-04 03:27:41.787806', '36', 'en_award_title', 1, '[{\"added\": {}}]', 30, 1),
(312, '2022-09-04 03:28:09.578452', '37', 'en_award_sub_title', 1, '[{\"added\": {}}]', 30, 1),
(313, '2022-09-04 03:28:26.694392', '38', 'en_certificate_title', 1, '[{\"added\": {}}]', 30, 1),
(314, '2022-09-04 03:28:59.875874', '39', 'en_certificate_sub_title', 1, '[{\"added\": {}}]', 30, 1),
(315, '2022-09-04 03:29:11.994318', '40', 'en_yearly_report_title', 1, '[{\"added\": {}}]', 30, 1),
(316, '2022-09-04 03:29:42.075733', '41', 'en_profile_direksi_title', 1, '[{\"added\": {}}]', 30, 1),
(317, '2022-09-04 03:30:03.974334', '42', 'en_profile_komisaris_title', 1, '[{\"added\": {}}]', 30, 1),
(318, '2022-09-04 04:10:28.143813', '37', 'id_bisnis_kami', 1, '[{\"added\": {}}]', 40, 1),
(319, '2022-09-04 04:11:06.943823', '5', 'en_banner_text', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 32, 1),
(320, '2022-09-04 04:11:12.921948', '4', 'en_title', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 32, 1),
(321, '2022-09-04 04:31:50.312936', '3', 'Prima Auto Care & Sales', 2, '[{\"changed\": {\"fields\": [\"Id long desc\"]}}]', 20, 1),
(322, '2022-09-04 04:32:38.532646', '3', 'Prima Auto Care & Sales', 2, '[{\"changed\": {\"fields\": [\"Id long desc\", \"En long desc\"]}}]', 20, 1),
(323, '2022-09-04 04:34:12.092176', '1', 'Prima Fleet Services', 2, '[{\"changed\": {\"fields\": [\"Id long desc\", \"En long desc\"]}}]', 20, 1),
(324, '2022-09-04 04:34:25.978845', '2', 'Prima Driver Management', 2, '[{\"changed\": {\"fields\": [\"Id long desc\", \"En long desc\"]}}]', 20, 1),
(325, '2022-09-04 04:34:46.871913', '3', 'Prima Auto Care & Sales', 2, '[]', 20, 1),
(326, '2022-09-04 04:36:53.927813', '3', 'Prima Auto Care & Sales', 2, '[{\"changed\": {\"fields\": [\"Id long desc\", \"En long desc\"]}}]', 20, 1),
(327, '2022-09-04 04:38:17.943829', '2', 'Prima Driver Management', 2, '[{\"changed\": {\"fields\": [\"Id long desc\", \"En long desc\"]}}]', 20, 1),
(328, '2022-09-04 04:38:33.196792', '2', 'Prima Driver Management', 2, '[{\"changed\": {\"fields\": [\"Id long desc\", \"En long desc\"]}}]', 20, 1),
(329, '2022-09-04 04:38:47.448751', '2', 'Prima Driver Management', 2, '[{\"changed\": {\"fields\": [\"Id long desc\", \"En long desc\"]}}]', 20, 1),
(330, '2022-09-04 04:39:10.871334', '2', 'Prima Driver Management', 2, '[{\"changed\": {\"fields\": [\"Id long desc\", \"En long desc\"]}}]', 20, 1),
(331, '2022-09-04 04:40:33.157881', '1', 'Prima Fleet Services', 2, '[{\"changed\": {\"fields\": [\"Id long desc\", \"En long desc\"]}}]', 20, 1),
(332, '2022-09-04 04:46:57.504226', '10', 'en_title', 1, '[{\"added\": {}}]', 37, 1),
(333, '2022-09-04 04:47:16.106458', '11', 'en_banner_sub_text', 1, '[{\"added\": {}}]', 37, 1),
(334, '2022-09-04 04:47:40.213974', '12', 'en_komitmen_description', 1, '[{\"added\": {}}]', 37, 1),
(335, '2022-09-04 04:48:33.081235', '13', 'en_komitmen_text', 1, '[{\"added\": {}}]', 37, 1),
(336, '2022-09-04 04:49:05.281376', '14', 'en_yearly_report_sub_title', 1, '[{\"added\": {}}]', 37, 1),
(337, '2022-09-04 04:49:29.823590', '15', 'id_banner_sub_text', 1, '[{\"added\": {}}]', 37, 1),
(338, '2022-09-04 04:50:15.478147', '16', 'en_banner_text', 1, '[{\"added\": {}}]', 37, 1),
(339, '2022-09-04 05:48:03.631087', '1', 'Jual Beli Mobil', 1, '[{\"added\": {}}]', 21, 1),
(340, '2022-09-04 05:49:36.924906', '2', 'Patra Jasa', 1, '[{\"added\": {}}]', 21, 1),
(341, '2022-09-04 05:50:31.814442', '1', 'Jual Mobil', 2, '[{\"changed\": {\"fields\": [\"Id big text\", \"En big text\", \"Id small text\", \"En small text\"]}}]', 21, 1),
(342, '2022-09-04 05:51:44.744057', '3', 'Pertamina', 1, '[{\"added\": {}}]', 21, 1),
(343, '2022-09-04 05:56:22.643406', '20', 'en_title', 1, '[{\"added\": {}}]', 34, 1),
(344, '2022-09-04 05:57:10.544171', '21', 'en_sub_title', 1, '[{\"added\": {}}]', 34, 1),
(345, '2022-09-04 05:57:39.860648', '22', 'en_telephone_title', 1, '[{\"added\": {}}]', 34, 1),
(346, '2022-09-04 05:57:47.232565', '23', 'en_email_title', 1, '[{\"added\": {}}]', 34, 1),
(347, '2022-09-04 05:57:54.881509', '24', 'en_address_title', 1, '[{\"added\": {}}]', 34, 1),
(348, '2022-09-04 05:58:26.075248', '25', 'en_telphone_subtext', 1, '[{\"added\": {}}]', 34, 1),
(349, '2022-09-04 05:58:53.881407', '26', 'en_email_sub_text', 1, '[{\"added\": {}}]', 34, 1),
(350, '2022-09-04 05:59:11.428139', '27', 'en_address_sub_text', 1, '[{\"added\": {}}]', 34, 1),
(351, '2022-09-04 05:59:34.093274', '28', 'en_contact_form_title', 1, '[{\"added\": {}}]', 34, 1),
(352, '2022-09-04 06:07:54.624715', '29', 'en_contact_form_sub_title', 1, '[{\"added\": {}}]', 34, 1),
(353, '2022-09-04 06:09:15.037211', '30', 'en_nama_depan_label', 1, '[{\"added\": {}}]', 34, 1),
(354, '2022-09-04 06:09:23.220428', '31', 'en_nama_belakang_label', 1, '[{\"added\": {}}]', 34, 1),
(355, '2022-09-04 06:09:33.087933', '32', 'en_email_label', 1, '[{\"added\": {}}]', 34, 1),
(356, '2022-09-04 06:09:40.664697', '33', 'en_phone_label', 1, '[{\"added\": {}}]', 34, 1),
(357, '2022-09-04 06:09:51.259967', '34', 'en_message_label', 1, '[{\"added\": {}}]', 34, 1),
(358, '2022-09-04 06:09:57.975574', '35', 'en_button_text', 1, '[{\"added\": {}}]', 34, 1),
(359, '2022-09-04 06:21:35.417046', '5', 'en_banner_text', 1, '[{\"added\": {}}]', 33, 1),
(360, '2022-09-04 06:21:53.441783', '5', 'en_banner_text', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 33, 1),
(361, '2022-09-04 06:22:11.621292', '3', 'id_title', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 33, 1),
(362, '2022-09-04 06:22:18.425501', '6', 'en_title', 1, '[{\"added\": {}}]', 33, 1),
(363, '2022-09-04 06:22:56.933922', '6', 'en_title', 1, '[{\"added\": {}}]', 36, 1),
(364, '2022-09-04 06:23:21.885576', '7', 'en_banner_text', 1, '[{\"added\": {}}]', 36, 1),
(365, '2022-09-04 06:23:26.615625', '6', 'en_title', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 36, 1),
(366, '2022-09-04 06:23:35.713469', '8', 'en_pengumuman_text', 1, '[{\"added\": {}}]', 36, 1),
(367, '2022-09-04 06:23:42.224481', '9', 'en_penutupan_text', 1, '[{\"added\": {}}]', 36, 1),
(368, '2022-09-04 06:23:50.307597', '8', 'en_pengumuman_text', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 36, 1),
(369, '2022-09-04 06:29:49.637662', '6', 'en_title', 1, '[{\"added\": {}}]', 38, 1),
(370, '2022-09-04 06:30:00.975465', '7', 'en_banner_text', 1, '[{\"added\": {}}]', 38, 1),
(371, '2022-09-04 06:30:22.527113', '8', 'en_banner_sub_text', 1, '[{\"added\": {}}]', 38, 1),
(372, '2022-09-04 06:30:28.930342', '9', 'en_album_title', 1, '[{\"added\": {}}]', 38, 1),
(373, '2022-09-04 06:30:49.880441', '9', 'en_album_title', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 38, 1),
(374, '2022-09-04 06:32:39.053628', '1', 'Sample Career', 1, '[{\"added\": {}}]', 27, 1),
(375, '2022-09-04 06:34:25.481535', '1', 'Sample Career', 3, '', 27, 1),
(376, '2022-09-04 06:36:23.698832', '7', 'id_no_career', 1, '[{\"added\": {}}]', 33, 1),
(377, '2022-09-04 06:36:39.887496', '8', 'en_no_career', 1, '[{\"added\": {}}]', 33, 1),
(378, '2022-09-04 06:40:40.267543', '8', 'en_no_career', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 33, 1),
(379, '2022-09-04 06:40:51.590211', '8', 'en_no_career', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 33, 1),
(380, '2022-09-04 06:43:51.339243', '10', 'id_no_tender', 1, '[{\"added\": {}}]', 36, 1),
(381, '2022-09-04 06:44:27.243020', '11', 'en_no_tender', 1, '[{\"added\": {}}]', 36, 1),
(382, '2022-09-04 06:51:40.823708', '1', 'Perusahaan', 1, '[{\"added\": {}}]', 29, 1),
(383, '2022-09-04 06:51:49.603500', '2', 'Kepemimpinan', 1, '[{\"added\": {}}]', 29, 1),
(384, '2022-09-04 06:52:12.160025', '3', 'Kendaraan', 1, '[{\"added\": {}}]', 29, 1),
(385, '2022-09-04 06:52:19.212639', '4', 'Mobil', 1, '[{\"added\": {}}]', 29, 1),
(386, '2022-09-04 06:52:20.397085', '5', 'Mobil', 1, '[{\"added\": {}}]', 29, 1),
(387, '2022-09-04 06:53:12.383878', '1', 'Sample News', 1, '[{\"added\": {}}]', 25, 1),
(388, '2022-09-04 06:53:50.706927', '4', 'en_banner_text', 1, '[{\"added\": {}}]', 35, 1),
(389, '2022-09-04 06:54:08.208020', '5', 'id_banner_sub_text', 1, '[{\"added\": {}}]', 35, 1),
(390, '2022-09-04 06:55:13.261555', '5', 'en_banner_sub_text', 2, '[{\"changed\": {\"fields\": [\"Name\"]}}]', 35, 1),
(391, '2022-09-04 06:55:24.930461', '6', 'en_latest_artikel_title', 1, '[{\"added\": {}}]', 35, 1),
(392, '2022-09-04 06:55:35.263374', '7', 'en_latest_news_title', 1, '[{\"added\": {}}]', 35, 1),
(393, '2022-09-04 06:55:41.535653', '6', 'en_latest_artikel_title', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 35, 1),
(394, '2022-09-04 06:57:22.288982', '2', 'Sample Article', 1, '[{\"added\": {}}]', 25, 1),
(395, '2022-09-04 13:12:27.864735', '38', 'id_portal', 1, '[{\"added\": {}}]', 40, 1),
(396, '2022-09-04 13:12:35.209050', '39', 'en_portal', 1, '[{\"added\": {}}]', 40, 1),
(397, '2022-09-04 13:58:39.525599', '43', 'all_ahlak_bg', 1, '[{\"added\": {}}]', 30, 1),
(398, '2022-09-04 13:59:19.316954', '44', 'id_ahlak_text', 1, '[{\"added\": {}}]', 30, 1),
(399, '2022-09-04 13:59:39.477914', '45', 'en_ahlak_text', 1, '[{\"added\": {}}]', 30, 1),
(400, '2022-09-04 14:21:40.487075', '1', 'History object (1)', 2, '[{\"changed\": {\"fields\": [\"Id long desc\", \"En long desc\"]}}]', 18, 1),
(401, '2022-09-04 14:21:49.363228', '2', 'History object (2)', 2, '[{\"changed\": {\"fields\": [\"Id long desc\", \"En long desc\"]}}]', 18, 1),
(402, '2022-09-04 23:07:20.510060', '2', 'Sample Article', 2, '[{\"changed\": {\"fields\": [\"Image\"]}}]', 25, 1),
(403, '2022-09-04 23:07:35.150480', '2', 'Sample Article', 2, '[{\"changed\": {\"fields\": [\"Image\"]}}]', 25, 1),
(404, '2022-09-04 23:07:43.110595', '1', 'Sample News', 2, '[{\"changed\": {\"fields\": [\"Image\"]}}]', 25, 1),
(405, '2022-09-04 23:08:32.873914', '2', 'Penghargaan ke-6 PT Prima Armada Raya di Top Company Awards 2022', 2, '[{\"changed\": {\"fields\": [\"Id title\", \"En title\"]}}]', 25, 1),
(406, '2022-09-04 23:08:52.036301', '1', 'Peresemian Kantor Baru di Surabaya Sebagai Langkah Pengembagan Bisnis', 2, '[{\"changed\": {\"fields\": [\"Id title\", \"Id short desc\", \"En title\", \"En short desc\"]}}]', 25, 1),
(407, '2022-09-05 00:07:37.832682', '5', 'Mobil', 3, '', 29, 1),
(408, '2022-09-05 00:08:00.960715', '4', 'Mobil', 2, '[{\"changed\": {\"fields\": [\"Color\", \"Bg color\"]}}]', 29, 1),
(409, '2022-09-05 00:08:33.515872', '3', 'Kendaraan', 2, '[{\"changed\": {\"fields\": [\"Color\", \"Bg color\"]}}]', 29, 1),
(410, '2022-09-05 00:08:53.712346', '2', 'Kepemimpinan', 2, '[{\"changed\": {\"fields\": [\"Color\", \"Bg color\"]}}]', 29, 1),
(411, '2022-09-05 00:09:18.912213', '1', 'Perusahaan', 2, '[{\"changed\": {\"fields\": [\"Color\", \"Bg color\"]}}]', 29, 1),
(412, '2022-09-05 03:20:46.593697', '15', 'id_client_title', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 31, 1),
(413, '2022-09-05 03:23:38.237267', '16', 'id_client_sub_title', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 31, 1),
(414, '2022-09-05 06:14:11.641967', '16', 'id_client_sub_title', 2, '[]', 31, 1),
(415, '2022-09-05 06:14:31.595387', '16', 'id_client_sub_title', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 31, 1),
(416, '2022-09-05 06:15:03.196850', '32', 'en_client_sub_title', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 31, 1),
(417, '2022-09-05 22:30:46.441644', '7', 'TJSL', 1, '[{\"added\": {}}]', 11, 1),
(418, '2022-09-05 22:31:07.259415', '1', 'Gallery object (1)', 1, '[{\"added\": {}}]', 26, 1),
(419, '2022-09-05 23:56:25.202822', '1', 'id_title', 1, '[{\"added\": {}}]', 43, 1),
(420, '2022-09-05 23:56:33.721237', '2', 'en_title', 1, '[{\"added\": {}}]', 43, 1),
(421, '2022-09-05 23:56:51.308798', '3', 'id_not_found_search', 1, '[{\"added\": {}}]', 43, 1),
(422, '2022-09-05 23:57:14.647880', '4', 'en_not_found_search', 1, '[{\"added\": {}}]', 43, 1),
(423, '2022-09-06 00:08:14.187067', '1', 'Laporan Tahunan 2021', 2, '[{\"changed\": {\"fields\": [\"File\"]}}]', 24, 1),
(424, '2022-09-06 00:08:44.362790', '2', 'Laporan Tahunan 2020', 2, '[{\"changed\": {\"fields\": [\"File\"]}}]', 24, 1),
(425, '2022-09-06 00:08:57.197962', '3', 'Laporan Tahunan 2019', 2, '[{\"changed\": {\"fields\": [\"File\"]}}]', 24, 1),
(426, '2022-09-06 00:09:10.628031', '4', 'Laporan Tahunan 2018', 2, '[{\"changed\": {\"fields\": [\"File\"]}}]', 24, 1),
(427, '2022-09-06 00:10:07.865592', '1', 'Laporan Tahunan 2021', 2, '[{\"changed\": {\"fields\": [\"File\"]}}]', 24, 1),
(428, '2022-09-06 05:05:36.470709', '19', 'id_about_title', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 31, 1),
(429, '2022-09-06 05:05:47.402902', '19', 'id_about_title', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 31, 1),
(430, '2022-09-06 05:10:00.597251', '2', 'id_title', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 32, 1),
(431, '2022-09-06 05:10:37.435444', '2', 'id_title', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 32, 1),
(432, '2022-09-06 05:30:50.613390', '1', 'Peresemian Kantor Baru di Surabaya Sebagai Langkah Pengembagan Bisnis', 2, '[{\"changed\": {\"fields\": [\"Id long desc\"]}}]', 25, 1),
(433, '2022-09-06 05:31:36.787435', '1', 'Peresemian Kantor Baru di Surabaya Sebagai Langkah Pengembagan Bisnis', 2, '[{\"changed\": {\"fields\": [\"En long desc\"]}}]', 25, 1),
(434, '2022-09-06 05:32:18.494870', '1', 'Peresemian Kantor Baru di Surabaya Sebagai Langkah Pengembagan Bisnis', 2, '[{\"changed\": {\"fields\": [\"En title\"]}}]', 25, 1),
(435, '2022-09-06 05:34:45.619410', '2', 'Penghargaan ke-6 PT Prima Armada Raya di Top Company Awards 2022', 2, '[{\"changed\": {\"fields\": [\"En title\"]}}]', 25, 1),
(436, '2022-09-06 05:58:31.464186', '2', 'peralatan otomotif untuk keperluar perbaikan mobil dan motor untuk provinsi Jawab Barat', 2, '[{\"changed\": {\"fields\": [\"Title\"]}}]', 23, 1),
(437, '2022-09-06 13:13:20.072499', '40', 'id_bagikan', 1, '[{\"added\": {}}]', 40, 1),
(438, '2022-09-06 13:13:28.398855', '41', 'en_bagikan', 1, '[{\"added\": {}}]', 40, 1),
(439, '2022-09-06 13:13:39.625527', '9', 'id_lowongan', 1, '[{\"added\": {}}]', 33, 1),
(440, '2022-09-06 13:14:08.086128', '10', 'en_lowongan', 1, '[{\"added\": {}}]', 33, 1),
(441, '2022-09-06 13:14:26.915049', '10', 'en_lowongan', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 33, 1),
(442, '2022-09-06 13:15:00.552884', '3', 'Prima Auto Care & Sales', 2, '[{\"changed\": {\"fields\": [\"En title\", \"En short desc\"]}}]', 20, 1),
(443, '2022-09-06 13:15:09.164018', '2', 'Prima Driver Management', 2, '[{\"changed\": {\"fields\": [\"En title\", \"En short desc\"]}}]', 20, 1),
(444, '2022-09-06 13:15:16.764832', '1', 'Prima Fleet Services', 2, '[{\"changed\": {\"fields\": [\"En title\", \"En short desc\"]}}]', 20, 1),
(445, '2022-09-06 13:15:39.122617', '2', 'Penghargaan ke-6 PT Prima Armada Raya di Top Company Awards 2022', 2, '[{\"changed\": {\"fields\": [\"En title\"]}}]', 25, 1),
(446, '2022-09-06 13:15:47.424014', '1', 'Peresemian Kantor Baru di Surabaya Sebagai Langkah Pengembagan Bisnis', 2, '[{\"changed\": {\"fields\": [\"En title\", \"En short desc\"]}}]', 25, 1),
(447, '2022-09-06 13:16:32.132165', '1', 'TataNilai object (1)', 2, '[{\"changed\": {\"fields\": [\"En description\"]}}]', 22, 1),
(448, '2022-09-06 13:16:37.962377', '2', 'TataNilai object (2)', 2, '[{\"changed\": {\"fields\": [\"En description\"]}}]', 22, 1),
(449, '2022-09-06 13:16:42.868165', '3', 'TataNilai object (3)', 2, '[{\"changed\": {\"fields\": [\"En description\"]}}]', 22, 1),
(450, '2022-09-06 13:16:48.337291', '4', 'TataNilai object (4)', 2, '[{\"changed\": {\"fields\": [\"En description\"]}}]', 22, 1),
(451, '2022-09-06 13:16:52.997103', '5', 'TataNilai object (5)', 2, '[{\"changed\": {\"fields\": [\"En description\"]}}]', 22, 1),
(452, '2022-09-06 13:20:40.434863', '2', 'Prima Driver Management', 2, '[{\"changed\": {\"fields\": [\"En long desc\"]}}]', 20, 1),
(453, '2022-09-06 13:20:48.264142', '3', 'Prima Auto Care & Sales', 2, '[{\"changed\": {\"fields\": [\"En long desc\"]}}]', 20, 1),
(454, '2022-09-06 13:20:53.664572', '1', 'Prima Fleet Services', 2, '[{\"changed\": {\"fields\": [\"En long desc\"]}}]', 20, 1),
(455, '2022-09-06 13:21:26.395473', '1', 'Laporan Tahunan 2021', 2, '[{\"changed\": {\"fields\": [\"En title\"]}}]', 24, 1),
(456, '2022-09-06 13:32:55.754185', '1', 'Acara Kebersamaan', 2, '[{\"changed\": {\"fields\": [\"En title\"]}}]', 11, 1),
(457, '2022-09-06 13:34:44.303620', '2', 'Lowongan Kerja 1', 1, '[{\"added\": {}}]', 27, 1),
(458, '2022-09-06 14:15:30.984862', '7', 'TJSL', 2, '[{\"changed\": {\"name\": \"Gallery\", \"object\": \"Gallery object (1)\", \"fields\": [\"Id title\", \"En title\", \"Type\"]}}]', 11, 1),
(459, '2022-09-06 14:15:50.459386', '4', 'Pelatihan Anggota', 2, '[{\"changed\": {\"fields\": [\"Slug\"]}}]', 11, 1),
(460, '2022-09-06 14:15:55.238566', '6', 'Peresmian Kantor Surabaya', 2, '[{\"changed\": {\"fields\": [\"Slug\"]}}]', 11, 1),
(461, '2022-09-06 14:16:05.203247', '1', 'Acara Kebersamaan', 2, '[{\"changed\": {\"fields\": [\"Slug\"]}}]', 11, 1),
(462, '2022-09-06 14:17:13.118790', '6', 'Peresmian Kantor Surabaya', 2, '[{\"added\": {\"name\": \"Gallery\", \"object\": \"Gallery object (2)\"}}, {\"added\": {\"name\": \"Gallery\", \"object\": \"Gallery object (3)\"}}, {\"added\": {\"name\": \"Gallery\", \"object\": \"Gallery object (4)\"}}]', 11, 1),
(463, '2022-09-06 14:17:20.146795', '6', 'Peresmian Kantor Surabaya', 2, '[{\"changed\": {\"name\": \"Gallery\", \"object\": \"Gallery object (4)\", \"fields\": [\"Type\"]}}]', 11, 1),
(464, '2022-09-06 15:20:23.509670', '4', 'Harnawan Santoso', 3, '', 17, 1),
(465, '2022-09-06 15:20:23.510834', '3', 'Harnawan Santoso', 3, '', 17, 1),
(466, '2022-09-06 15:20:23.511532', '2', 'Harnawan Santoso', 3, '', 17, 1),
(467, '2022-09-06 15:20:53.604652', '5', 'Anjas Jati Kesuma', 1, '[{\"added\": {}}]', 17, 1),
(468, '2022-09-06 15:21:22.666986', '6', 'Wahyu Witjaksono', 1, '[{\"added\": {}}]', 17, 1),
(469, '2022-09-06 15:21:58.834824', '7', 'Mabroer MS', 1, '[{\"added\": {}}]', 17, 1),
(470, '2022-09-06 15:27:16.708568', '23', 'id_profile_direksi_title', 2, '[{\"changed\": {\"fields\": [\"Text value\"]}}]', 30, 1),
(471, '2022-09-06 15:30:23.868451', '7', 'Mabroer MS', 2, '[{\"changed\": {\"fields\": [\"Id position\", \"En position\"]}}]', 17, 1),
(472, '2022-09-06 15:30:35.016100', '5', 'Anjas Jati Kesuma', 2, '[{\"changed\": {\"fields\": [\"Id position\", \"En position\"]}}]', 17, 1),
(473, '2022-09-06 15:30:49.142021', '6', 'Wahyu Witjaksono', 2, '[{\"changed\": {\"fields\": [\"Id position\", \"En position\"]}}]', 17, 1),
(474, '2022-09-06 15:31:04.160333', '1', 'Harnawan Santoso', 2, '[{\"changed\": {\"fields\": [\"Id position\", \"En position\"]}}]', 17, 1);

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1, 'sites', 'site'),
(2, 'admin', 'logentry'),
(3, 'auth', 'permission'),
(4, 'auth', 'group'),
(5, 'contenttypes', 'contenttype'),
(6, 'sessions', 'session'),
(7, 'django_summernote', 'attachment'),
(8, 'taggit', 'tag'),
(9, 'taggit', 'taggeditem'),
(10, 'user', 'user'),
(11, 'content', 'album'),
(12, 'content', 'award'),
(13, 'content', 'banner'),
(14, 'content', 'certicate'),
(15, 'content', 'client'),
(16, 'content', 'contact'),
(17, 'content', 'directorprofile'),
(18, 'content', 'history'),
(19, 'content', 'menu'),
(20, 'content', 'ourbusiness'),
(21, 'content', 'portal'),
(22, 'content', 'tatanilai'),
(23, 'content', 'tender'),
(24, 'content', 'yearlyreport'),
(25, 'content', 'news'),
(26, 'content', 'gallery'),
(27, 'content', 'carier'),
(28, 'content', 'cariercategory'),
(29, 'content', 'newstag'),
(30, 'settings', 'aboutussettings'),
(31, 'settings', 'homesettings'),
(32, 'settings', 'ourbusinesssettings'),
(33, 'settings', 'carierpagesettings'),
(34, 'settings', 'contactusesettings'),
(35, 'settings', 'newspagesettings'),
(36, 'settings', 'tenderpagesettings'),
(37, 'settings', 'investorpagesettings'),
(38, 'settings', 'gallerypagesettings'),
(39, 'settings', 'footersettings'),
(40, 'settings', 'commonsettings'),
(41, 'settings', 'carierdetailpagesettings'),
(42, 'settings', 'tjslpagesettings'),
(43, 'settings', 'searchpagesettings');

-- --------------------------------------------------------

--
-- Table structure for table `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2022-08-16 04:49:38.028622'),
(2, 'contenttypes', '0002_remove_content_type_name', '2022-08-16 04:49:38.038495'),
(3, 'auth', '0001_initial', '2022-08-16 04:49:38.066591'),
(4, 'auth', '0002_alter_permission_name_max_length', '2022-08-16 04:49:38.073292'),
(5, 'auth', '0003_alter_user_email_max_length', '2022-08-16 04:49:38.078099'),
(6, 'auth', '0004_alter_user_username_opts', '2022-08-16 04:49:38.082573'),
(7, 'auth', '0005_alter_user_last_login_null', '2022-08-16 04:49:38.087449'),
(8, 'auth', '0006_require_contenttypes_0002', '2022-08-16 04:49:38.088995'),
(9, 'auth', '0007_alter_validators_add_error_messages', '2022-08-16 04:49:38.094208'),
(10, 'auth', '0008_alter_user_username_max_length', '2022-08-16 04:49:38.098559'),
(11, 'auth', '0009_alter_user_last_name_max_length', '2022-08-16 04:49:38.103197'),
(12, 'auth', '0010_alter_group_name_max_length', '2022-08-16 04:49:38.109272'),
(13, 'auth', '0011_update_proxy_permissions', '2022-08-16 04:49:38.114879'),
(14, 'auth', '0012_alter_user_first_name_max_length', '2022-08-16 04:49:38.119439'),
(15, 'user', '0001_initial', '2022-08-16 04:49:38.146130'),
(16, 'admin', '0001_initial', '2022-08-16 04:49:38.161886'),
(17, 'admin', '0002_logentry_remove_auto_add', '2022-08-16 04:49:38.167716'),
(18, 'admin', '0003_logentry_add_action_flag_choices', '2022-08-16 04:49:38.173263'),
(19, 'taggit', '0001_initial', '2022-08-16 04:49:38.192785'),
(20, 'taggit', '0002_auto_20150616_2121', '2022-08-16 04:49:38.201961'),
(21, 'taggit', '0003_taggeditem_add_unique_index', '2022-08-16 04:49:38.209244'),
(22, 'taggit', '0004_alter_taggeditem_content_type_alter_taggeditem_tag', '2022-08-16 04:49:38.223356'),
(23, 'taggit', '0005_auto_20220424_2025', '2022-08-16 04:49:38.226289'),
(24, 'content', '0001_initial', '2022-08-16 04:49:38.287570'),
(25, 'content', '0002_news_type', '2022-08-16 04:49:38.295884'),
(26, 'content', '0003_remove_ourbusiness_contact', '2022-08-16 04:49:38.302683'),
(27, 'content', '0004_remove_news_contact_ourbusiness_contact_and_more', '2022-08-16 04:49:38.339064'),
(28, 'content', '0005_alter_news_date', '2022-08-16 04:49:38.347544'),
(29, 'content', '0006_remove_ourbusiness_contact_alter_ourbusiness_logo', '2022-08-16 04:49:38.355250'),
(30, 'content', '0007_alter_banner_en_big_text_alter_banner_en_small_text_and_more', '2022-08-16 04:49:38.370848'),
(31, 'content', '0008_award_month_year_alter_yearlyreport_file', '2022-08-16 04:49:38.381638'),
(32, 'content', '0009_rename_description_tatanilai_id_description_and_more', '2022-08-16 04:49:38.393255'),
(33, 'content', '0010_remove_history_en_title_remove_history_id_title_and_more', '2022-08-16 04:49:38.405192'),
(34, 'content', '0011_ourbusiness_order', '2022-08-16 04:49:38.411047'),
(35, 'content', '0012_alter_portal_en_big_text_alter_portal_en_small_text_and_more', '2022-08-16 04:49:38.427363'),
(36, 'content', '0013_cariercategory_newstag_remove_news_tags_and_more', '2022-08-16 04:49:38.476401'),
(37, 'content', '0014_alter_cariercategory_options_alter_newstag_options_and_more', '2022-08-16 04:49:38.508241'),
(38, 'content', '0015_rename_city_carier_en_title_and_more', '2022-08-16 04:49:38.536114'),
(39, 'content', '0016_carier_city_carier_country', '2022-08-16 04:49:38.547395'),
(40, 'content', '0017_carier_type', '2022-08-16 04:49:38.552669'),
(41, 'content', '0018_carier_en_description_carier_id_description', '2022-08-16 04:49:38.564305'),
(42, 'content', '0019_carier_slug', '2022-08-16 04:49:38.570584'),
(43, 'content', '0020_alter_carier_slug', '2022-08-16 04:49:38.575145'),
(44, 'content', '0021_alter_carier_options_alter_cariercategory_options', '2022-08-16 04:49:38.580533'),
(45, 'content', '0022_remove_album_title_album_en_title_album_id_title', '2022-08-16 04:49:38.593053'),
(46, 'content', '0023_alter_album_date', '2022-08-16 04:49:38.597180'),
(47, 'django_summernote', '0001_initial', '2022-08-16 04:49:38.600990'),
(48, 'django_summernote', '0002_update-help_text', '2022-08-16 04:49:38.603997'),
(49, 'sessions', '0001_initial', '2022-08-16 04:49:38.610059'),
(50, 'settings', '0001_initial', '2022-08-16 04:49:38.618891'),
(51, 'settings', '0002_carierpagesettings_contactusesettings_and_more', '2022-08-16 04:49:38.631433'),
(52, 'settings', '0003_investorpagesettings_alter_aboutussettings_options_and_more', '2022-08-16 04:49:38.645951'),
(53, 'settings', '0004_gallerypagesettings', '2022-08-16 04:49:38.650320'),
(54, 'settings', '0005_footersettings', '2022-08-16 04:49:38.654315'),
(55, 'settings', '0006_alter_footersettings_name', '2022-08-16 04:49:38.657969'),
(56, 'settings', '0007_alter_homesettings_name', '2022-08-16 04:49:38.661685'),
(57, 'settings', '0008_alter_aboutussettings_name_alter_homesettings_name', '2022-08-16 04:49:38.667590'),
(58, 'settings', '0009_commonsettings_alter_ourbusinesssettings_name', '2022-08-16 04:49:38.673819'),
(59, 'settings', '0010_alter_commonsettings_options', '2022-08-16 04:49:38.677089'),
(60, 'settings', '0011_alter_aboutussettings_name_alter_commonsettings_name_and_more', '2022-08-16 04:49:38.685490'),
(61, 'settings', '0012_alter_carierpagesettings_name_and_more', '2022-08-16 04:49:38.692487'),
(62, 'settings', '0013_carierdetailpagesettings_alter_commonsettings_name_and_more', '2022-08-16 04:49:38.701994'),
(63, 'settings', '0014_alter_carierdetailpagesettings_options_and_more', '2022-08-16 04:49:38.708101'),
(64, 'settings', '0015_alter_gallerypagesettings_name', '2022-08-16 04:49:38.712623'),
(65, 'settings', '0016_alter_commonsettings_name', '2022-08-16 04:49:38.716983'),
(66, 'sites', '0001_initial', '2022-08-16 04:49:38.721474'),
(67, 'sites', '0002_alter_domain_unique', '2022-08-16 04:49:38.729508'),
(68, 'user', '0002_auto_20210909_1641', '2022-08-16 04:49:38.747486'),
(69, 'settings', '0017_tjslpagesettings_alter_commonsettings_name', '2022-08-28 23:15:36.859672'),
(70, 'content', '0024_newstag_bg_color', '2022-09-05 00:07:19.468505'),
(71, 'content', '0025_newstag_color', '2022-09-05 00:07:19.474595'),
(72, 'settings', '0018_alter_aboutussettings_name_and_more', '2022-09-05 00:07:19.487251'),
(73, 'content', '0026_contact_email', '2022-09-05 01:13:26.534513'),
(74, 'content', '0027_alter_contact_last_name_alter_contact_phone_number', '2022-09-05 01:13:26.542781'),
(75, 'settings', '0019_searchpagesettings', '2022-09-05 23:56:09.594811'),
(76, 'settings', '0020_alter_searchpagesettings_options', '2022-09-05 23:56:09.598002'),
(77, 'content', '0028_alter_yearlyreport_file', '2022-09-06 00:07:54.890603'),
(78, 'content', '0029_rename_ed_title_cariercategory_en_title', '2022-09-06 13:37:49.227195'),
(79, 'settings', '0021_alter_carierpagesettings_name_and_more', '2022-09-06 13:37:49.232360'),
(80, 'content', '0030_album_total_photo_album_total_video_gallery_type', '2022-09-06 14:15:04.326522'),
(81, 'content', '0031_alter_gallery_type', '2022-09-06 14:15:04.333400'),
(82, 'content', '0032_alter_gallery_img', '2022-09-06 14:15:04.338705'),
(83, 'content', '0033_gallery_en_title_gallery_id_title', '2022-09-06 14:15:04.351544'),
(84, 'content', '0034_alter_gallery_en_title_alter_gallery_id_title', '2022-09-06 14:15:04.364833'),
(85, 'content', '0035_directorprofile_img', '2022-09-06 15:16:37.236654'),
(86, 'settings', '0022_alter_homesettings_name', '2022-09-06 15:16:37.240034'),
(87, 'content', '0036_remove_directorprofile_position_and_more', '2022-09-06 15:30:04.225526'),
(88, 'settings', '0023_alter_homesettings_name', '2022-09-06 15:30:04.230075');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('bfcfbtnxpzs6apquvxqu575bmbk6csu2', '.eJxVjEEKwyAQAP_iuYga0U2PvfcNsmZ3a9piICan0L8XIYf2OjPMoRLuW0l74zXNpK7Kqssvyzi9uHZBT6yPRU9L3dY5657o0zZ9X4jft7P9GxRspW89s2TOA6LLGDORGAoxiIAFEvFWgJwfRjcYQiEYbfAQwDCGyUFUny8lATjP:1oNoY2:I1ivl51fqade9PIFaKmttRAklN2bqsfdSmnQRufAPgQ', '2022-08-30 04:51:26.860477'),
('0of7fj1mr6dh44fug3v5d7usvm31ic3t', '.eJxVjEEKwyAQAP_iuYga0U2PvfcNsmZ3a9piICan0L8XIYf2OjPMoRLuW0l74zXNpK7Kqssvyzi9uHZBT6yPRU9L3dY5657o0zZ9X4jft7P9GxRspW89s2TOA6LLGDORGAoxiIAFEvFWgJwfRjcYQiEYbfAQwDCGyUFUny8lATjP:1oNobd:qTqCOLGE1DaCrEEcHzJz3EcJpHxP7HdgIeoUnG9At0g', '2022-08-30 04:55:09.701279'),
('osw50pf1vccee2ynxtq4php119f1qiq0', '.eJxVjEEKwyAQAP_iuYga0U2PvfcNsmZ3a9piICan0L8XIYf2OjPMoRLuW0l74zXNpK7Kqssvyzi9uHZBT6yPRU9L3dY5657o0zZ9X4jft7P9GxRspW89s2TOA6LLGDORGAoxiIAFEvFWgJwfRjcYQiEYbfAQwDCGyUFUny8lATjP:1oQQHD:wuhbzu723Ee0Uxqu4WCuJ-XyHxIYT1w4WC0sPYuVsiA', '2022-09-06 09:32:51.632639'),
('623nawug8tlltse1cz5k33y7kbhm8tf8', '.eJxVjEEKwyAQAP_iuYga0U2PvfcNsmZ3a9piICan0L8XIYf2OjPMoRLuW0l74zXNpK7Kqssvyzi9uHZBT6yPRU9L3dY5657o0zZ9X4jft7P9GxRspW89s2TOA6LLGDORGAoxiIAFEvFWgJwfRjcYQiEYbfAQwDCGyUFUny8lATjP:1oQQij:mKUURJUzvJPUJcNpWQDuX6VqWr_kvPk3iu0YByQYbJc', '2022-09-06 10:01:17.510291'),
('r07bcm39j4uzxw26svlmogk9k9qjcpxo', '.eJxVjEEKwyAQAP_iuYga0U2PvfcNsmZ3a9piICan0L8XIYf2OjPMoRLuW0l74zXNpK7Kqssvyzi9uHZBT6yPRU9L3dY5657o0zZ9X4jft7P9GxRspW89s2TOA6LLGDORGAoxiIAFEvFWgJwfRjcYQiEYbfAQwDCGyUFUny8lATjP:1oUcuT:GmMDzwppFrNgN_3lZ6Cry7WkK48-5fsnDoBDJMPXWzk', '2022-09-17 23:50:45.489011');

-- --------------------------------------------------------

--
-- Table structure for table `django_site`
--

CREATE TABLE `django_site` (
  `id` int(11) NOT NULL,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_site`
--

INSERT INTO `django_site` (`id`, `domain`, `name`) VALUES
(1, 'example.com', 'example.com');

-- --------------------------------------------------------

--
-- Table structure for table `django_summernote_attachment`
--

CREATE TABLE `django_summernote_attachment` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `file` varchar(100) NOT NULL,
  `uploaded` datetime(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settings_aboutussettings`
--

CREATE TABLE `settings_aboutussettings` (
  `id` int(11) NOT NULL,
  `text_value` longtext DEFAULT NULL,
  `img_value` varchar(100) DEFAULT NULL,
  `name` varchar(254) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_aboutussettings`
--

INSERT INTO `settings_aboutussettings` (`id`, `text_value`, `img_value`, `name`) VALUES
(1, 'Tentang kami', 'static_files/settings/2022/8/ttg-kami.png', 'id_about_us_title'),
(2, '', 'static_files/settings/2022/8/ttg-kami_UpGgndY.png', 'all_banner_image'),
(3, 'Kami sangat memperhatikan aspek <br /> keamanan bagi kendaraan yang disewakan', '', 'id_about_us_short_description'),
(4, 'PT Prima Armada Raya, salah satu anak usaha dari PT Patra Jasa,<br />adalah perusahaan yang didirikan<br />untuk memenuhi berbagai kebutuhan transportasi untuk bisnis termasuk penyewaan kendaraan, penyediaan pengemudi, penjualan<br />mobil bekas, perbengkelan, dan keagenan suku cadang pengganti mobil.<br /> <br />Kami berkomitmen memberikan pelayanan yang prima, memberikan jaminan keamanan dan kenyamanan yang memudahkan dalam semua kebutuhan transportasi pelanggan kami.', '', 'id_about_us_long_description'),
(5, 'Jejak Langkah', '', 'id_jejak_langkah_title'),
(6, 'Perseroan mencatat jejak langkah dari tahun ke tahun sebagai berikut:', '', 'id_jejak_langkah_sub_title'),
(7, '', 'static_files/settings/2022/8/visi-1.png', 'all_visi_misi_image'),
(8, 'Visi & Misi Perusahaan', '', 'id_visi_misi_title'),
(9, 'Tujuan dan langkah yang akan dilakukan perusahaan', '', 'id_visi_misi_sub_title'),
(10, 'Visi Perusahaan', '', 'id_visi_title'),
(11, 'Menjadi perusahaan jasa transportasi modern yang mengedepankan keamanan dalam pelayanan prima kepada pelanggan', '', 'id_visi_text'),
(12, 'Misi Perusahaan', '', 'id_misi_title'),
(13, '1. Memberikan produk dan pelayanan terbaik untuk mencapai customer experience.\r\n\r\n2. Mengembangkan kapabilitas dan kualitas sumber daya manusia.\r\n\r\n3. Menciptakan nilai tambah yang inovatif untuk kesinambungan pertumbuhan.\r\n\r\n4. Menjalankan kegiatan usaha dengan etika dan prinsip tata kelola perusahaan yang baik.', '', 'id_misi_text'),
(14, 'Tata Nilai Perusahaan', '', 'id_tata_nilai_title'),
(15, 'Poin-poin dari AKHLAK dari BUMN dan PRIMA sebagai tata nilai perusahaan', '', 'id_tata_nilai_sub_title'),
(16, 'Penghargaan', '', 'id_award_title'),
(17, 'Penghargaan nasional & internasional yang telah diraih perusahaan', '', 'id_award_sub_title'),
(18, 'Sertifikat', '', 'id_certificate_title'),
(19, 'Sertifikat yang telah dicapai dan diterima oleh perusahaan', '', 'id_certificate_sub_title'),
(20, 'Laporan Tahunan', '', 'id_yearly_report_title'),
(21, 'See our business progress in the annual report', '', 'en_yearly_report_sub_title'),
(22, 'Lihat perkembangan bisnis kami pada laporan tahunan', '', 'id_yearly_report_sub_title'),
(23, 'Profil Direksi', '', 'id_profile_direksi_title'),
(24, 'Profil Komisaris', '', 'id_profile_komisaris_title'),
(25, 'About Us', '', 'en_about_us_title'),
(26, 'We are very concerned about the <br /> security aspect for the rental vehicles', '', 'en_about_us_short_description'),
(27, 'PT Prima Armada Raya, a subsidiary of PT Patra Jasa,<br />is a company established<br />to meet various transportation needs for businesses including vehicle rental, provision of drivers, sales<br />used cars, repair shops, and agency for car replacement parts.<br /> <br />We are committed to providing excellent service, guaranteeing safety and comfort that makes it easy for all of our customers\' transportation needs.', '', 'en_about_us_long_description'),
(28, 'Milestones', '', 'en_jejak_langkah_title'),
(29, 'The Company recorded the following steps from year to year:', '', 'en_jejak_langkah_sub_title'),
(30, 'Visi & Misi', '', 'en_visi_misi_title'),
(31, 'Misi', '', 'en_misi_title'),
(32, 'Visi', '', 'en_visi_title'),
(33, 'To become a modern transportation service company that prioritizes security in excellent service to customers', '', 'en_visi_text'),
(34, 'Values', '', 'en_tata_nilai_title'),
(35, 'Points from AKHLAK from BUMN and PRIMA as corporate values', '', 'en_tata_nilai_sub_title'),
(36, 'Awards', '', 'en_award_title'),
(37, 'National & international awards the company has won', '', 'en_award_sub_title'),
(38, 'Certificate', '', 'en_certificate_title'),
(39, 'Certificates that have been achieved and accepted by the company', '', 'en_certificate_sub_title'),
(40, 'Yearly Report', '', 'en_yearly_report_title'),
(41, 'Director Profile', '', 'en_profile_direksi_title'),
(42, 'Commissioner', '', 'en_profile_komisaris_title'),
(43, '', 'static_files/settings/2022/9/bg-ahlak.png', 'all_ahlak_bg'),
(44, 'AKHLAK adalah nilai diterapkan oleh Kementerian BUMN dalam melayani negeri <br /> yang memiliki singkatan Amanah, Kompeten, Harmonis, Loyal, Adaptif, dan <br /> Kolaboratif.', '', 'id_ahlak_text'),
(45, 'AKHLAK is the value applied by the Ministry of SOEs in serving the country <br /> which has the abbreviation Amanah, Competent, Harmonious, Loyal, Adaptive, and Collaborative.', '', 'en_ahlak_text');

-- --------------------------------------------------------

--
-- Table structure for table `settings_carierdetailpagesettings`
--

CREATE TABLE `settings_carierdetailpagesettings` (
  `id` int(11) NOT NULL,
  `text_value` longtext DEFAULT NULL,
  `img_value` varchar(100) DEFAULT NULL,
  `name` varchar(254) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_carierdetailpagesettings`
--

INSERT INTO `settings_carierdetailpagesettings` (`id`, `text_value`, `img_value`, `name`) VALUES
(1, '', 'static_files/settings/2022/9/img-banner-karir_7CWBpC0.png', 'all_banner_image'),
(2, 'Semua Kategori', '', 'id_lamar_pekerjaan'),
(3, 'Semua Tipe', '', 'id_pekerjaan_serupa');

-- --------------------------------------------------------

--
-- Table structure for table `settings_carierpagesettings`
--

CREATE TABLE `settings_carierpagesettings` (
  `id` int(11) NOT NULL,
  `text_value` longtext DEFAULT NULL,
  `img_value` varchar(100) DEFAULT NULL,
  `name` varchar(254) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_carierpagesettings`
--

INSERT INTO `settings_carierpagesettings` (`id`, `text_value`, `img_value`, `name`) VALUES
(1, '', 'static_files/settings/2022/9/img-banner-karir.png', 'all_banner_image'),
(2, 'Temukan Jalanmu untuk <br /> Mewujudkan Mimpi', '', 'id_banner_text'),
(3, 'Karir', '', 'id_title'),
(4, 'Menampilkan 8 pekerjaan disemua lokasi dalam semua kategori dan semua tipe', '', 'id_banner_sub_text'),
(5, 'Find Your Way to <br /> Realize Your Dream', '', 'en_banner_text'),
(6, 'Career', '', 'en_title'),
(7, 'Belum ada Informasi <br /> Karir Saat Ini', '', 'id_no_career'),
(8, 'No Carrer Information', '', 'en_no_career'),
(9, 'Lowongan', '', 'id_lowongan'),
(10, 'Vacancies', '', 'en_lowongan');

-- --------------------------------------------------------

--
-- Table structure for table `settings_commonsettings`
--

CREATE TABLE `settings_commonsettings` (
  `id` int(11) NOT NULL,
  `text_value` longtext DEFAULT NULL,
  `img_value` varchar(100) DEFAULT NULL,
  `name` varchar(254) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_commonsettings`
--

INSERT INTO `settings_commonsettings` (`id`, `text_value`, `img_value`, `name`) VALUES
(1, 'Selengkapnya', '', 'id_selengkapnya'),
(2, 'Beranda', '', 'id_beranda'),
(3, 'Lihat Laporan', '', 'id_lihat_laporan'),
(4, 'Kunjungi Website', '', 'id_kunjungi_website'),
(5, 'Hubungi Kami', '', 'id_hubungi_kami'),
(6, 'Media Informasi', '', 'id_media_informasi'),
(7, 'Karir', '', 'id_karir'),
(8, 'Galeri', '', 'id_gallery'),
(9, 'Tentang Kami', '', 'id_tentang_kami'),
(10, 'Hubungan Investor', '', 'id_hubungan_investor'),
(11, 'Artikel & Berita', '', 'id_artikel_dan_berita'),
(12, 'Artikel', '', 'id_artikel'),
(13, 'Berita', '', 'id_berita'),
(14, 'Tender', '', 'id_tender'),
(15, 'Menampilkan', '', 'id_menampilkan'),
(16, 'TJSL', '', 'id_tjsl'),
(17, 'Home', '', 'en_beranda'),
(18, 'Our Business', '', 'en_bisnis_kami'),
(19, 'More Detail', '', 'en_selengkapnya'),
(20, 'Show Report', '', 'en_lihat_laporan'),
(21, 'Visit Website', '', 'en_kunjungi_website'),
(22, 'Media Information', '', 'en_media_informasi'),
(23, 'Contact Us', '', 'en_hubungi_kami'),
(24, 'Career', '', 'en_karir'),
(25, 'Gallery', '', 'en_gallery'),
(26, 'About Us', '', 'en_tentang_kami'),
(27, 'Investor', '', 'en_investor'),
(28, 'Director Profile', '', 'en_profil_direksi'),
(29, 'Profil Direksi', '', 'id_profil_direksi'),
(30, 'Investor Relations', '', 'en_hubungan_investor'),
(31, 'Article & News', '', 'en_artikel_dan_berita'),
(32, 'Article', '', 'en_artikel'),
(33, 'News', '', 'en_berita'),
(34, 'Tender', '', 'en_tender'),
(35, 'show', '', 'en_menampilkan'),
(36, 'TJSL', '', 'en_tjsl'),
(37, 'Bisnis Kami', '', 'id_bisnis_kami'),
(38, 'Portal', '', 'id_portal'),
(39, 'Portal', '', 'en_portal'),
(40, 'Bagikan:', '', 'id_bagikan'),
(41, 'Share to', '', 'en_bagikan');

-- --------------------------------------------------------

--
-- Table structure for table `settings_contactusesettings`
--

CREATE TABLE `settings_contactusesettings` (
  `id` int(11) NOT NULL,
  `text_value` longtext DEFAULT NULL,
  `img_value` varchar(100) DEFAULT NULL,
  `name` varchar(254) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_contactusesettings`
--

INSERT INTO `settings_contactusesettings` (`id`, `text_value`, `img_value`, `name`) VALUES
(1, 'customer.care@par.co.id', '', 'all_email_address'),
(2, '021-5290-0021', '', 'all_phone_number'),
(3, 'Patra Jasa Office Tower Lantai 1, Jl. <br /> Jend. Gatot Subroto Kav.32-34, <br /> Jakarta 12950', '', 'all_address'),
(4, 'Hubungi Kami', '', 'id_title'),
(5, 'Kami siap membantu dan menjawab segala pertanyaan terkait <br /> dengan layanan dan keluhan anda', '', 'id_sub_title'),
(6, 'Telephone', '', 'id_telephone_title'),
(7, 'Email', '', 'id_email_title'),
(8, 'Alamat', '', 'id_address_title'),
(9, 'Setiap Hari (09:00 - 18:00)', '', 'id_telphone_subtext'),
(10, 'Dibalas dalam 24 jam', '', 'id_email_sub_text'),
(11, 'Head Office', '', 'id_address_sub_text'),
(12, 'Kontak Form', '', 'id_contact_form_title'),
(13, 'Tanyakan pertanyaan anda melalui kontak form', '', 'id_contact_form_sub_title'),
(14, 'Nama Depan', '', 'id_nama_depan_label'),
(15, 'Nama Belakang', '', 'id_nama_belakang_label'),
(16, 'Email', '', 'id_email_label'),
(17, 'Nomor Telephone', '', 'id_phone_label'),
(18, 'Pesan', '', 'id_message_label'),
(19, 'Kirim Pesan', '', 'id_button_text'),
(20, 'Contact Us', '', 'en_title'),
(21, 'We are ready to help and answer any questions <br /> related to your service and complaints', '', 'en_sub_title'),
(22, 'Phone Number', '', 'en_telephone_title'),
(23, 'Email', '', 'en_email_title'),
(24, 'Address', '', 'en_address_title'),
(25, 'Everyday (09:00 - 18:00)', '', 'en_telphone_subtext'),
(26, 'Reply in 24 hours', '', 'en_email_sub_text'),
(27, 'Head Office', '', 'en_address_sub_text'),
(28, 'Contact Form', '', 'en_contact_form_title'),
(29, 'Ask your question via the contact form', '', 'en_contact_form_sub_title'),
(30, 'First Name', '', 'en_nama_depan_label'),
(31, 'Last Name', '', 'en_nama_belakang_label'),
(32, 'Email', '', 'en_email_label'),
(33, 'Phone Number', '', 'en_phone_label'),
(34, 'Message', '', 'en_message_label'),
(35, 'Send', '', 'en_button_text');

-- --------------------------------------------------------

--
-- Table structure for table `settings_footersettings`
--

CREATE TABLE `settings_footersettings` (
  `id` int(11) NOT NULL,
  `text_value` longtext DEFAULT NULL,
  `img_value` varchar(100) DEFAULT NULL,
  `name` varchar(254) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_footersettings`
--

INSERT INTO `settings_footersettings` (`id`, `text_value`, `img_value`, `name`) VALUES
(1, 'Jangan sampai ketinggalan perkembangan dan acara PT PAR. <br /> Anda dapat berhenti langganan berita kapan saja', '', 'id_subscribe_description'),
(2, 'Masukan email anda', '', 'id_email_placeholder'),
(3, 'Langganan berita', '', 'id_langganan_berita'),
(4, 'Info', '', 'id_info'),
(5, 'Quick Links', '', 'id_quick_links'),
(6, 'Tentang Kami', '', 'id_tentang_kami'),
(7, 'Bisnis Kami', '', 'id_bisnis_kami'),
(8, 'Ada Pertanyaan? Hubungi Kami.', '', 'id_ada_pertanyaan'),
(9, 'Kami akan segera menjawab pertanyaan anda seputar <br /> layanan kami melalui call center atau online chat', '', 'id_ada_pertanyaan_desc'),
(10, 'Beranda', '', 'id_beranda'),
(11, 'Berita', '', 'id_berita'),
(12, 'Karir', '', 'id_karir'),
(13, 'Tender', '', 'id_tender'),
(14, 'Berita', '', 'id_news'),
(15, 'Sitemap', '', 'id_sitemap'),
(16, 'Kebijakan Privasi', '', 'id_kebijakan_privasi'),
(17, 'Profile Perusahaan', '', 'id_profile_perusahaan'),
(18, 'Management', '', 'id_manajement'),
(19, 'Penghargaan', '', 'id_penghargaan'),
(20, 'Kontak', '', 'id_kontak'),
(21, 'Patra Jasa', '', 'id_patra_jasa'),
(22, 'Any Question? Contact Us', '', 'en_ada_pertanyaan'),
(23, 'We will immediately answer your questions about <br /> our services through the call center or online chat', '', 'en_ada_pertanyaan_desc'),
(24, 'Our Business', '', 'en_bisnis_kami'),
(25, 'About Us', '', 'en_tentang_kami'),
(26, 'Quick Links', '', 'en_quick_links'),
(27, 'Information', '', 'en_info'),
(28, 'News Subscription', '', 'en_langganan_berita'),
(29, 'Input Your Email', '', 'en_email_placeholder'),
(30, 'Don\'t miss the developments and events of PT PAR. <br /> You can unsubscribe at any time', '', 'en_subscribe_description'),
(31, 'Sitemap', '', 'en_sitemap'),
(32, 'Privacy Policy', '', 'en_kebijakan_privasi'),
(33, 'Company Profile', '', 'en_profile_perusahaan'),
(34, 'Management', '', 'en_manajement'),
(35, 'Awards', '', 'en_penghargaan'),
(36, 'Contact', '', 'en_kontak'),
(37, 'Home', '', 'en_beranda'),
(38, 'Patra Jasa', '', 'en_patra_jasa'),
(39, 'News', '', 'en_berita'),
(40, 'Career', '', 'en_karir'),
(41, 'Tender', '', 'en_tender'),
(42, 'News', '', 'en_news');

-- --------------------------------------------------------

--
-- Table structure for table `settings_gallerypagesettings`
--

CREATE TABLE `settings_gallerypagesettings` (
  `id` int(11) NOT NULL,
  `text_value` longtext DEFAULT NULL,
  `img_value` varchar(100) DEFAULT NULL,
  `name` varchar(254) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_gallerypagesettings`
--

INSERT INTO `settings_gallerypagesettings` (`id`, `text_value`, `img_value`, `name`) VALUES
(1, '', 'static_files/settings/2022/8/banner-galery.png', 'all_banner_image'),
(2, 'Galeri', '', 'id_title'),
(3, 'Galeri', '', 'id_banner_text'),
(4, 'Dapatkan informasi dan penawaran menarik setiap harinya ke <br /> email anda dengan berlangganan newsletter kami', '', 'id_banner_sub_text'),
(5, 'Album Foto & Video', '', 'id_album_title'),
(6, 'Gallery', '', 'en_title'),
(7, 'Album Foto & Video', '', 'en_banner_text'),
(8, 'Get interesting information and offers every day to your email <br /> by subscribing to our newsletter', '', 'en_banner_sub_text'),
(9, 'Album Foto & Video', '', 'en_album_title');

-- --------------------------------------------------------

--
-- Table structure for table `settings_homesettings`
--

CREATE TABLE `settings_homesettings` (
  `id` int(11) NOT NULL,
  `text_value` longtext DEFAULT NULL,
  `img_value` varchar(100) DEFAULT NULL,
  `name` varchar(254) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_homesettings`
--

INSERT INTO `settings_homesettings` (`id`, `text_value`, `img_value`, `name`) VALUES
(1, '', 'static_files/settings/2022/9/about.png', 'all_img_about_1'),
(2, '3000', '', 'all_total_kendaraan'),
(3, 'PT Prima Armada Raya (PAR) merupakan salah satu anak <br /> Perusahaan PT Patra Jasa yang berpengalaman dalam usaha <br /> penyediaan jasa penyewaan kendaraan dan jasa layanan  <br /> pengemudi baik di lingkungan PT Pertamina (Persero) <br /> maupun sektor lainnya', 'static_files/settings/2022/8/rectangle-8_Ndsopv0.png', 'id_about_description'),
(4, 'Perjalanan 8 Tahun PT PAR dalam <br /> Layanan Transportasi di Indonesia', '', 'id_map_title'),
(5, 'Selengkapnya', '', 'id_btn_selengkapnya'),
(6, 'Pertumbuhan pesat sebagai hasil dari profesionalitasi <br /> PT PAR sebagai penyedia layanan transportasi', '', 'id_map_description'),
(7, 'Pengemudi', '', 'id_map_pengemudi'),
(20, 'About Us', '', 'en_about_title'),
(10, 'Perusahaan Jasa Transportasi <br /> dengan Pertumbuhan Tercepat', '', 'id_about_text'),
(11, 'Bisnis Kami', '', 'id_our_business_title'),
(12, 'Layanan transportasi profesional yang ditawarkan PT PAR', '', 'id_our_business_sub_title'),
(13, 'Berita & Acara', '', 'id_news_title'),
(14, 'Pusat informasi berita dan acara terbaru perusahaan', '', 'id_news_sub_title'),
(15, 'Klien Kami1', '', 'id_client_title'),
(16, 'List klien kami', '', 'id_client_sub_title'),
(19, 'Tentang Kami', '', 'id_about_title'),
(17, '3000', '', 'all_total_pengemudi'),
(18, 'Kendaraan', '', 'id_map_kendaraan'),
(21, 'Transportation Service Company <br/> with the Fastest Growth', '', 'en_about_text'),
(22, 'PT Prima Armada Raya (PAR) is a subsidiary of PT Patra Jasa which is experienced in the business of providing vehicle rental services and driver services both within PT Pertamina (Persero) <br / > and other sectors', '', 'en_about_description'),
(23, 'PT PAR\'s 8 Years Journey in <br /> Transportation Services in Indonesia', '', 'en_map_title'),
(24, 'Rapid growth as a result of PT PAR\'s professionalism as a transportation service provider', '', 'en_map_description'),
(25, 'Vehicle', '', 'en_map_kendaraan'),
(26, 'Driver', '', 'en_map_pengemudi'),
(27, 'Our Business', '', 'en_our_business_title'),
(28, 'Professional transportation services offered by PT PAR', '', 'en_our_business_sub_title'),
(29, 'News', '', 'en_news_title'),
(30, 'Information center for the latest company news and events', '', 'en_news_sub_title'),
(31, 'Our Client', '', 'en_client_title'),
(32, 'List of our client', '', 'en_client_sub_title'),
(33, 'More Detail', '', 'en_btn_selengkapnya');

-- --------------------------------------------------------

--
-- Table structure for table `settings_investorpagesettings`
--

CREATE TABLE `settings_investorpagesettings` (
  `id` int(11) NOT NULL,
  `text_value` longtext DEFAULT NULL,
  `img_value` varchar(100) DEFAULT NULL,
  `name` varchar(254) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_investorpagesettings`
--

INSERT INTO `settings_investorpagesettings` (`id`, `text_value`, `img_value`, `name`) VALUES
(1, '', 'static_files/settings/2022/9/banner-hub-investor.png', 'all_banner_image'),
(2, '', 'static_files/settings/2022/9/komitmen-img.png', 'all_komitmen_image'),
(3, 'Hubungan Investor', '', 'id_title'),
(4, 'Informasi laporan Prima Armada Raya tentang performa <br /> perusahaan dan keuangan', '', 'id_banner_text'),
(5, 'Komitmen<br />Prima Armada Raya', '', 'id_komitmen_text'),
(6, 'Komitmen kami untuk memberikan informasi keuangan yang <br /> lengkap dan update berita tentang Prima Armada Raya. <br /> Bagian ini akan memberikan wawasan tentang harga saham, <br /> kinerja keuangan terkini, instrumen permodalan dan manfaat <br /> informasi keuangan perbankan lainnya untuk keterlibatan <br /> pemangku kepentingan', '', 'id_komitmen_description'),
(7, 'Laporan Tahunan', '', 'en_yearly_report_title'),
(8, 'Lihat perkembangan bisnis kami pada laporan tahunan', '', 'id_yearly_report_sub_title'),
(9, 'Laporan Tahunan', '', 'id_yearly_report_title'),
(10, 'Investor Relations', '', 'en_title'),
(11, 'Prima Armada Raya report information about the company\'s <br /> performance and finance', '', 'en_banner_sub_text'),
(12, 'We are committed to providing complete <br /> financial information and news updates about Prima Armada Raya. <br /> This section will provide insight on stock prices, <br /> recent financial performance, capital instruments and benefits <br /> other banking financial information for stakeholder engagement <br />', '', 'en_komitmen_description'),
(13, 'Prima Armada Raya <br/> Commitment', '', 'en_komitmen_text'),
(14, 'See our business progress in the annual report', '', 'en_yearly_report_sub_title'),
(15, 'Prima Armada Raya report information about the company\'s <br /> performance and finance', '', 'id_banner_sub_text'),
(16, 'Prima Armada Raya report information about the company\'s <br /> performance and finance', '', 'en_banner_text');

-- --------------------------------------------------------

--
-- Table structure for table `settings_newspagesettings`
--

CREATE TABLE `settings_newspagesettings` (
  `id` int(11) NOT NULL,
  `text_value` longtext DEFAULT NULL,
  `img_value` varchar(100) DEFAULT NULL,
  `name` varchar(254) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_newspagesettings`
--

INSERT INTO `settings_newspagesettings` (`id`, `text_value`, `img_value`, `name`) VALUES
(1, '', 'static_files/settings/2022/8/banner-artikel.png', 'all_banner_image'),
(2, 'Artikel & Berita', '', 'id_title'),
(3, 'Dapatkan informasi dan penawaran menarik setiap harinya ke <br /> email anda dengan berlangganan newsletter kami', '', 'id_banner_text'),
(4, 'Article & News', '', 'en_banner_text'),
(5, 'Get interesting information and offers every day to your email <br /> by subscribing to our newsletter', '', 'en_banner_sub_text'),
(6, 'Latest Article', '', 'en_latest_artikel_title'),
(7, 'Latest News', '', 'en_latest_news_title');

-- --------------------------------------------------------

--
-- Table structure for table `settings_ourbusinesssettings`
--

CREATE TABLE `settings_ourbusinesssettings` (
  `id` int(11) NOT NULL,
  `text_value` longtext DEFAULT NULL,
  `img_value` varchar(100) DEFAULT NULL,
  `name` varchar(254) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_ourbusinesssettings`
--

INSERT INTO `settings_ourbusinesssettings` (`id`, `text_value`, `img_value`, `name`) VALUES
(1, '', 'static_files/settings/2022/8/banner-bisnis-kami_k9Xquvi.png', 'all_banner_image'),
(2, 'Bisnis Kami', '', 'id_title'),
(3, 'Solusi Utama <br /> Layanan Transportasi', 'static_files/settings/2022/8/banner-bisnis-kami_hro8lQO.png', 'id_banner_text'),
(4, 'Our Business', '', 'en_title'),
(5, 'Ultimate Solution <br /> Transportation Service', '', 'en_banner_text');

-- --------------------------------------------------------

--
-- Table structure for table `settings_searchpagesettings`
--

CREATE TABLE `settings_searchpagesettings` (
  `id` int(11) NOT NULL,
  `text_value` longtext DEFAULT NULL,
  `img_value` varchar(100) DEFAULT NULL,
  `name` varchar(254) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_searchpagesettings`
--

INSERT INTO `settings_searchpagesettings` (`id`, `text_value`, `img_value`, `name`) VALUES
(1, 'Hasil Pencarian', '', 'id_title'),
(2, 'Search Results', '', 'en_title'),
(3, 'Tidak Ditemukan Data <br/> Hasil Pencarian', '', 'id_not_found_search'),
(4, 'No Data Found', '', 'en_not_found_search');

-- --------------------------------------------------------

--
-- Table structure for table `settings_tenderpagesettings`
--

CREATE TABLE `settings_tenderpagesettings` (
  `id` int(11) NOT NULL,
  `text_value` longtext DEFAULT NULL,
  `img_value` varchar(100) DEFAULT NULL,
  `name` varchar(254) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_tenderpagesettings`
--

INSERT INTO `settings_tenderpagesettings` (`id`, `text_value`, `img_value`, `name`) VALUES
(1, '', 'static_files/settings/2022/9/banner-tender.png', 'all_banner_image'),
(2, 'Tender', '', 'id_title'),
(3, 'Pusat Informasi Pengadaan Prima Armada Raya', '', 'id_banner_text'),
(4, 'Pengumuman', '', 'id_pengumuman_text'),
(5, 'Penutupan', '', 'id_penutupan_text'),
(6, 'Procurement', '', 'en_title'),
(7, 'Prima Armada Raya Procurement <br /> Information Center', '', 'en_banner_text'),
(8, 'Announcement', '', 'en_pengumuman_text'),
(9, 'Closed', '', 'en_penutupan_text'),
(10, 'Belum Ada Infomrmasi <br /> Tender Saat ini', '', 'id_no_tender'),
(11, 'No Procurement Information', '', 'en_no_tender');

-- --------------------------------------------------------

--
-- Table structure for table `settings_tjslpagesettings`
--

CREATE TABLE `settings_tjslpagesettings` (
  `id` int(11) NOT NULL,
  `text_value` longtext DEFAULT NULL,
  `img_value` varchar(100) DEFAULT NULL,
  `name` varchar(254) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_tjslpagesettings`
--

INSERT INTO `settings_tjslpagesettings` (`id`, `text_value`, `img_value`, `name`) VALUES
(1, '', 'static_files/settings/2022/9/banner-tjsl.png', 'all_banner_image'),
(2, 'TJSL', '', 'id_banner_text'),
(3, 'Program Tanggung Jawab Sosial Lingkungan (TJSL) dilakukan untuk menciptakan <br /> hubungan yang harmonis antara Perusahaan dan masyarakat', '', 'id_banner_sub_text'),
(4, 'Album Foto & Video', '', 'id_album_title');

-- --------------------------------------------------------

--
-- Table structure for table `taggit_tag`
--

CREATE TABLE `taggit_tag` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `taggit_taggeditem`
--

CREATE TABLE `taggit_taggeditem` (
  `id` int(11) NOT NULL,
  `object_id` int(11) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_user`
--

CREATE TABLE `user_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(50) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `mobile_number` varchar(30) NOT NULL,
  `dealer_address` varchar(100) DEFAULT NULL,
  `dealer_name` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_user`
--

INSERT INTO `user_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `name`, `is_active`, `is_staff`, `mobile_number`, `dealer_address`, `dealer_name`) VALUES
(1, 'pbkdf2_sha256$390000$LSObHH3G2ZFYLVyqzz71tU$kw9H+rEKD6o3GPZDGs2asL/kOzgqnljHIH/hittmUNg=', '2022-09-03 23:50:45.487822', 1, 'admin', NULL, 1, 1, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_user_groups`
--

CREATE TABLE `user_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_user_user_permissions`
--

CREATE TABLE `user_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissions_group_id_b120cbf9` (`group_id`),
  ADD KEY `auth_group_permissions_permission_id_84c5c92e` (`permission_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  ADD KEY `auth_permission_content_type_id_2f476e4b` (`content_type_id`);

--
-- Indexes for table `content_album`
--
ALTER TABLE `content_album`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `content_award`
--
ALTER TABLE `content_award`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `content_banner`
--
ALTER TABLE `content_banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `content_carier`
--
ALTER TABLE `content_carier`
  ADD PRIMARY KEY (`id`),
  ADD KEY `content_carier_category_id_71e2152d` (`category_id`);

--
-- Indexes for table `content_cariercategory`
--
ALTER TABLE `content_cariercategory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `content_certicate`
--
ALTER TABLE `content_certicate`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `content_client`
--
ALTER TABLE `content_client`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `content_contact`
--
ALTER TABLE `content_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `content_directorprofile`
--
ALTER TABLE `content_directorprofile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `content_gallery`
--
ALTER TABLE `content_gallery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `content_gallery_album_id_0e42b0c5` (`album_id`);

--
-- Indexes for table `content_history`
--
ALTER TABLE `content_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `content_menu`
--
ALTER TABLE `content_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `content_news`
--
ALTER TABLE `content_news`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `content_newstag`
--
ALTER TABLE `content_newstag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `content_news_categories`
--
ALTER TABLE `content_news_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `content_news_categories_news_id_newstag_id_74396613_uniq` (`news_id`,`newstag_id`),
  ADD KEY `content_news_categories_news_id_16bb9237` (`news_id`),
  ADD KEY `content_news_categories_newstag_id_86285de7` (`newstag_id`);

--
-- Indexes for table `content_ourbusiness`
--
ALTER TABLE `content_ourbusiness`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `content_portal`
--
ALTER TABLE `content_portal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `content_tatanilai`
--
ALTER TABLE `content_tatanilai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `content_tender`
--
ALTER TABLE `content_tender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `content_yearlyreport`
--
ALTER TABLE `content_yearlyreport`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6` (`user_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indexes for table `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Indexes for table `django_site`
--
ALTER TABLE `django_site`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_site_domain_a2e37b91_uniq` (`domain`);

--
-- Indexes for table `django_summernote_attachment`
--
ALTER TABLE `django_summernote_attachment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings_aboutussettings`
--
ALTER TABLE `settings_aboutussettings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `settings_carierdetailpagesettings`
--
ALTER TABLE `settings_carierdetailpagesettings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `settings_carierpagesettings`
--
ALTER TABLE `settings_carierpagesettings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `settings_commonsettings`
--
ALTER TABLE `settings_commonsettings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `settings_contactusesettings`
--
ALTER TABLE `settings_contactusesettings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `settings_footersettings`
--
ALTER TABLE `settings_footersettings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `settings_gallerypagesettings`
--
ALTER TABLE `settings_gallerypagesettings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `settings_homesettings`
--
ALTER TABLE `settings_homesettings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `settings_investorpagesettings`
--
ALTER TABLE `settings_investorpagesettings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `settings_newspagesettings`
--
ALTER TABLE `settings_newspagesettings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `settings_ourbusinesssettings`
--
ALTER TABLE `settings_ourbusinesssettings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `settings_searchpagesettings`
--
ALTER TABLE `settings_searchpagesettings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `settings_tenderpagesettings`
--
ALTER TABLE `settings_tenderpagesettings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `settings_tjslpagesettings`
--
ALTER TABLE `settings_tjslpagesettings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `taggit_tag`
--
ALTER TABLE `taggit_tag`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `taggit_taggeditem`
--
ALTER TABLE `taggit_taggeditem`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `taggit_taggeditem_content_type_id_object_id_tag_id_4bb97a8e_uniq` (`content_type_id`,`object_id`,`tag_id`),
  ADD KEY `taggit_taggeditem_object_id_e2d7d1df` (`object_id`),
  ADD KEY `taggit_taggeditem_content_type_id_9957a03c` (`content_type_id`),
  ADD KEY `taggit_taggeditem_tag_id_f4f5b767` (`tag_id`),
  ADD KEY `taggit_taggeditem_content_type_id_object_id_196cc965_idx` (`content_type_id`,`object_id`);

--
-- Indexes for table `user_user`
--
ALTER TABLE `user_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `user_user_groups`
--
ALTER TABLE `user_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_user_groups_user_id_group_id_bb60391f_uniq` (`user_id`,`group_id`),
  ADD KEY `user_user_groups_user_id_13f9a20d` (`user_id`),
  ADD KEY `user_user_groups_group_id_c57f13c0` (`group_id`);

--
-- Indexes for table `user_user_user_permissions`
--
ALTER TABLE `user_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_user_user_permissions_user_id_permission_id_64f4d5b8_uniq` (`user_id`,`permission_id`),
  ADD KEY `user_user_user_permissions_user_id_31782f58` (`user_id`),
  ADD KEY `user_user_user_permissions_permission_id_ce49d4de` (`permission_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=173;

--
-- AUTO_INCREMENT for table `content_album`
--
ALTER TABLE `content_album`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `content_award`
--
ALTER TABLE `content_award`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `content_banner`
--
ALTER TABLE `content_banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `content_carier`
--
ALTER TABLE `content_carier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `content_cariercategory`
--
ALTER TABLE `content_cariercategory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `content_certicate`
--
ALTER TABLE `content_certicate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `content_client`
--
ALTER TABLE `content_client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `content_contact`
--
ALTER TABLE `content_contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `content_directorprofile`
--
ALTER TABLE `content_directorprofile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `content_gallery`
--
ALTER TABLE `content_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `content_history`
--
ALTER TABLE `content_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `content_menu`
--
ALTER TABLE `content_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `content_news`
--
ALTER TABLE `content_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `content_newstag`
--
ALTER TABLE `content_newstag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `content_news_categories`
--
ALTER TABLE `content_news_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `content_ourbusiness`
--
ALTER TABLE `content_ourbusiness`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `content_portal`
--
ALTER TABLE `content_portal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `content_tatanilai`
--
ALTER TABLE `content_tatanilai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `content_tender`
--
ALTER TABLE `content_tender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `content_yearlyreport`
--
ALTER TABLE `content_yearlyreport`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=475;

--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `django_site`
--
ALTER TABLE `django_site`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `django_summernote_attachment`
--
ALTER TABLE `django_summernote_attachment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `settings_aboutussettings`
--
ALTER TABLE `settings_aboutussettings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `settings_carierdetailpagesettings`
--
ALTER TABLE `settings_carierdetailpagesettings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `settings_carierpagesettings`
--
ALTER TABLE `settings_carierpagesettings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `settings_commonsettings`
--
ALTER TABLE `settings_commonsettings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `settings_contactusesettings`
--
ALTER TABLE `settings_contactusesettings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `settings_footersettings`
--
ALTER TABLE `settings_footersettings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `settings_gallerypagesettings`
--
ALTER TABLE `settings_gallerypagesettings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `settings_homesettings`
--
ALTER TABLE `settings_homesettings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `settings_investorpagesettings`
--
ALTER TABLE `settings_investorpagesettings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `settings_newspagesettings`
--
ALTER TABLE `settings_newspagesettings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `settings_ourbusinesssettings`
--
ALTER TABLE `settings_ourbusinesssettings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `settings_searchpagesettings`
--
ALTER TABLE `settings_searchpagesettings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `settings_tenderpagesettings`
--
ALTER TABLE `settings_tenderpagesettings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `settings_tjslpagesettings`
--
ALTER TABLE `settings_tjslpagesettings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `taggit_tag`
--
ALTER TABLE `taggit_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `taggit_taggeditem`
--
ALTER TABLE `taggit_taggeditem`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_user`
--
ALTER TABLE `user_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_user_groups`
--
ALTER TABLE `user_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_user_user_permissions`
--
ALTER TABLE `user_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
